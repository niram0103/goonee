(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "/W/p":
/*!*****************************************************************************!*\
  !*** ./src/app/components/site/userauthentication/services/user.service.ts ***!
  \*****************************************************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../environments/environment */ "AytR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "tk/3");





class UserService {
    constructor(http) {
        this.http = http;
    }
    createUser(user) {
        //console.log(user);
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/user/register', user)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_1__["catchError"])((err) => {
            console.log('error caught in service');
            console.error(err);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_0__["throwError"])(err); //Rethrow it back to component
        }));
    }
    userLogin(credentials) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/user/login', credentials);
    }
    //generate change password link
    forgetPassword(credentials) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/sendPasswordResetLink', credentials);
    }
    updatePassword(data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/resetPassword', data);
    }
    updatePasswordWithPhone(data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/update_password_with_phone', data);
    }
    getDetail(token) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/getDetail?resetToken=' + token.resetToken);
    }
    sendotpToPhone(phonenumber) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/send_otp', phonenumber);
    }
    verifyOTP(data) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/verify_otp', data);
    }
    //check for unique email address
    isUniqueEmail(email) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/user_email', email);
    }
    //check for unique phone number
    isUniquePhone(phone) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/unique_phone_number', phone);
    }
    socialLogin(user) {
        return this.http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].apiUrl + '/user/slogin', user);
    }
}
UserService.ɵfac = function UserService_Factory(t) { return new (t || UserService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"])); };
UserService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({ token: UserService, factory: UserService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/paritoshikpaul/huptech/gooneelive/frontend/src/main.ts */"zUnb");


/***/ }),

/***/ "3oJF":
/*!**********************************************************************************************!*\
  !*** ./src/app/components/site/pages/main-category-detail/main-category-detail.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: MainCategoryDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainCategoryDetailComponent", function() { return MainCategoryDetailComponent; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../../environments/environment */ "AytR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_category_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../shared/services/category.service */ "fH6q");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _goone_slider_goone_slider_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../goone-slider/goone-slider.component */ "lmtY");
/* harmony import */ var _shared_pipes_StripHtmltags__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../shared/pipes/StripHtmltags */ "Bh+D");







function MainCategoryDetailComponent_div_23_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "a", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "img", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const category_r2 = ctx.$implicit;
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("routerLink", category_r2.category_alias);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate2"]("src", "", ctx_r1.baseUrl, "category_imgs/", category_r2.category_image, "", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("alt", category_r2.category);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](category_r2.category);
} }
function MainCategoryDetailComponent_div_23_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "h2", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Sub Categories");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](5, MainCategoryDetailComponent_div_23_div_5_Template, 5, 5, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r0.subCategories);
} }
class MainCategoryDetailComponent {
    constructor(catService, route) {
        this.catService = catService;
        this.route = route;
        this.title = "Top Experts";
        this.title1 = "Rising talents";
        this.title2 = "For you";
        this.slides = [
            { img: "assets/images/goonee-talents-1.jpg" },
            { img: "assets/images/goonee-talents-2.jpg" },
            { img: "assets/images/goonee-talents-3.jpg" },
            { img: "assets/images/goonee-talents-4.jpg" },
            { img: "assets/images/goonee-talents-5.jpg" }
        ];
        this.slides1 = [
            { img: "assets/images/goonee-talents-1.jpg" },
            { img: "assets/images/goonee-talents-2.jpg" },
            { img: "assets/images/goonee-talents-3.jpg" },
            { img: "assets/images/goonee-talents-4.jpg" },
            { img: "assets/images/goonee-talents-5.jpg" }
        ];
        this.slides2 = [
            { img: "assets/images/goonee-talents-1.jpg" },
            { img: "assets/images/goonee-talents-2.jpg" },
            { img: "assets/images/goonee-talents-3.jpg" },
            { img: "assets/images/goonee-talents-4.jpg" },
            { img: "assets/images/goonee-talents-5.jpg" }
        ];
        this.slideConfig = { "slidesToShow": 4, "slidesToScroll": 4,
            "nextArrow": "<div class='slickArrows next'><span class=\"slicknext btn\"><i class=\"fas fa-arrow-left\"></i></span></div>",
            "prevArrow": "<div class='slickArrows prev'><span class=\"slickprev btn\"><i class=\"fas fa-arrow-right\"></i></span></div>"
        };
        this.slideConfig1 = { "slidesToShow": 4, "slidesToScroll": 4,
            "nextArrow": "<div class='slickArrows next'><span class=\"slicknext btn\"><i class=\"fas fa-arrow-left\"></i></span></div>",
            "prevArrow": "<div class='slickArrows prev'><span class=\"slickprev btn\"><i class=\"fas fa-arrow-right\"></i></span></div>"
        };
        this.slideConfig2 = { "slidesToShow": 4, "slidesToScroll": 4,
            "nextArrow": "<div class='slickArrows next'><span class=\"slicknext btn\"><i class=\"fas fa-arrow-left\"></i></span></div>",
            "prevArrow": "<div class='slickArrows prev'><span class=\"slickprev btn\"><i class=\"fas fa-arrow-right\"></i></span></div>"
        };
    }
    ngOnInit() {
        this.getSubcategories();
    }
    getSubcategories() {
        this.baseUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].serverUrl;
        let cat = this.route.snapshot.paramMap.get('data');
        let catId;
        this.catService.getCategories().subscribe((data) => {
            this.category = data.data.filter(alias => alias.category_alias == cat);
            catId = this.category[0].id;
            this.data = {
                pid: catId,
                cat_alias: cat
            };
            this.catService.getSubCategories(this.data).subscribe((data) => {
                this.categories = data.data;
                console.log(this.categories);
                this.subCategories = this.categories.sub_categories;
            });
        });
    }
}
MainCategoryDetailComponent.ɵfac = function MainCategoryDetailComponent_Factory(t) { return new (t || MainCategoryDetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_shared_services_category_service__WEBPACK_IMPORTED_MODULE_2__["CategoryService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"])); };
MainCategoryDetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: MainCategoryDetailComponent, selectors: [["app-main-category-detail"]], decls: 268, vars: 17, consts: [[1, "hero-banner", "catban"], [1, "hero-caption"], [1, "container"], [1, "col-lg-5"], ["aria-label", "breadcrumb"], [1, "breadcrumb"], [1, "breadcrumb-item"], ["href", "index.html"], ["width", "20", "height", "17", "viewBox", "0 0 20 17", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["fill-rule", "evenodd", "clip-rule", "evenodd", "d", "M19.8639 8.38897C19.6825 8.53657 19.3877 8.53657 19.2063 8.38897L14.9497 4.93544C14.9192 4.91573 14.893 4.89448 14.8688 4.86936L13.045 3.38989C13.0145 3.37018 12.9883 3.34893 12.964 3.32381L9.99976 0.918552L0.793681 8.38897C0.612255 8.53657 0.317496 8.53657 0.13607 8.38897C-0.0453566 8.24175 -0.0453566 8.00258 0.13607 7.85537L9.61453 0.164323C9.62929 0.146163 9.63977 0.125298 9.65929 0.108683C9.7531 0.0329514 9.87691 -0.00220978 9.99976 0.000108542C10.1226 -0.00220978 10.2464 0.0325651 10.3402 0.108683C10.3607 0.124912 10.3698 0.145777 10.3859 0.164323L12.8569 2.17006V1.15811C12.8569 0.944826 13.0697 0.771724 13.3331 0.771724H15.2378C15.3697 0.771724 15.4887 0.815 15.5745 0.884936C15.6611 0.954872 15.714 1.05147 15.714 1.15811V4.48839L19.8639 7.85537C20.0454 8.00258 20.0454 8.24175 19.8639 8.38897ZM14.7616 1.5445H13.8092V2.94284L14.7616 3.71561V1.5445ZM3.33317 7.7267C3.59603 7.7267 3.80936 7.8998 3.80936 8.11309V15.4545C3.80936 15.8814 4.23602 16.2272 4.76173 16.2272H7.61884V11.2042C7.61884 10.9905 7.83169 10.8178 8.09502 10.8178H11.9045C12.1678 10.8178 12.3807 10.9905 12.3807 11.2042V16.2272H15.2378C15.764 16.2272 16.1902 15.8814 16.1902 15.4545V8.11309C16.1902 7.8998 16.403 7.7267 16.6663 7.7267C16.9297 7.7267 17.1425 7.8998 17.1425 8.11309V15.4545C17.1425 16.308 16.2897 17 15.2378 17H4.76173C3.70984 17 2.85699 16.308 2.85699 15.4545V8.11309C2.85699 7.89941 3.07032 7.7267 3.33317 7.7267ZM11.4283 16.2268V11.5902H8.57121V16.2268H11.4283Z"], ["routerLink", "/category"], ["aria-current", "page", 1, "breadcrumb-item", "active"], [1, "col-lg-6", "offset-lg-6", "p-0"], ["alt", "goonee_category_banner", 1, "w-100", "d-none", "d-lg-block", 3, "src"], ["alt", "goonee_category_banner", 1, "w-100", "d-block", "d-lg-none", 3, "src"], ["class", "goonee-cats py-5", 4, "ngIf"], [3, "title", "slideArray", "slidesConfig"], [1, "py-5"], [1, "fw-bold"], [1, "trending-topics", "mt-4"], ["href", "#", 1, "btn", "bg-light"], [1, "bg-light", "py-5", "goonee-cards", "goonee-recent-active", "px-3", "px-md-0"], [1, "d-none", "d-sm-block"], [1, "row", "mt-4"], [1, "col-lg-4", "col-md-6", "my-3"], [1, "card-view", "px-4", "py-4", "d-flex", "align-items-center", "justify-content-between"], [1, "user-circle"], ["src", "assets/images/ra-user-1.jpg", "alt", "user"], [1, "online"], [1, "user-nd-text", "mx-2"], ["href", "#", 1, "btn", "btn-primary"], ["src", "assets/images/ra-user-2.jpg", "alt", "user"], ["src", "assets/images/ra-user-3.jpg", "alt", "user"], ["src", "assets/images/ra-user-4.jpg", "alt", "user"], ["src", "assets/images/ra-user-5.jpg", "alt", "user"], ["src", "assets/images/ra-user-6.jpg", "alt", "user"], ["id", "carouselrecentactive", "data-bs-ride", "carousel", 1, "carousel", "slide", "d-block", "d-sm-none"], [1, "carousel-inner"], [1, "carousel-item", "active"], [1, "card-view", "px-3", "py-3", "my-3", "d-flex", "align-items-center", "justify-content-between"], [1, "carousel-item"], [1, "carousel-indicators"], ["type", "button", "data-bs-target", "#carouselrecentactive", "data-bs-slide-to", "0", "aria-current", "true", "aria-label", "Slide 1", 1, "active"], ["type", "button", "data-bs-target", "#carouselrecentactive", "data-bs-slide-to", "1", "aria-label", "Slide 2"], ["type", "button", "data-bs-target", "#carouselrecentactive", "data-bs-slide-to", "2", "aria-label", "Slide 3"], [1, "goonee-cats", "py-5"], [1, "container", "px-4", "px-sm-0"], [1, "row", "gx-lg-4", "gx-3", "py-4", "py-md-5"], ["class", "col-md-2 col-sm-4 col-6 mt-3 mb-4", 4, "ngFor", "ngForOf"], [1, "col-md-2", "col-sm-4", "col-6", "mt-3", "mb-4"], [1, "goonee-cat-card", 3, "routerLink"], [3, "src", "alt"]], template: function MainCategoryDetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "nav", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "ol", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "li", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "svg", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "path", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "li", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "a", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, "Categories");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "li", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](19, "striphtml");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](21, "img", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](22, "img", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](23, MainCategoryDetailComponent_div_23_Template, 6, 1, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](24, "app-goone-slider", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](27, "h2", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](28, "Trending Topics");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](31, "Life Motivation");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33, "Dating Advice");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](35, "Motivation");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](37, "Fitness");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](39, "Counseling");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](41, "Baking");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](43, "Interview Training");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](44, "app-goone-slider", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](45, "app-goone-slider", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "h2", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](49, "Recently Active");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](51, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](53, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](55, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](56, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](57, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](58, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](59, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](60, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](61, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](62, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](63, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](65, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](66, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](67, "img", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](68, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](69, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](71, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](72, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](73, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](74, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](75, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](76, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](77, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](78, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](79, "img", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](80, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](81, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](82, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](83, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](84, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](85, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](86, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](87, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](88, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](89, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](90, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](91, "img", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](92, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](93, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](94, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](95, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](96, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](97, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](98, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](99, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](100, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](101, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](102, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](103, "img", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](104, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](105, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](106, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](107, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](108, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](109, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](110, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](111, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](112, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](113, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](114, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](115, "img", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](116, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](117, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](118, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](119, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](120, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](121, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](122, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](123, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](124, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](125, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](126, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](127, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](128, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](129, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](130, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](131, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](132, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](133, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](134, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](135, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](136, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](137, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](138, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](139, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](140, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](141, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](142, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](143, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](144, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](145, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](146, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](147, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](148, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](149, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](150, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](151, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](152, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](153, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](154, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](155, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](156, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](157, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](158, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](159, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](160, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](161, "div", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](162, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](163, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](164, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](165, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](166, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](167, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](168, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](169, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](170, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](171, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](172, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](173, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](174, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](175, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](176, "img", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](177, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](178, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](179, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](180, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](181, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](182, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](183, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](184, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](185, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](186, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](187, "img", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](188, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](189, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](190, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](191, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](192, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](193, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](194, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](195, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](196, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](197, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](198, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](199, "img", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](200, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](201, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](202, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](203, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](204, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](205, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](206, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](207, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](208, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](209, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](210, "img", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](211, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](212, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](213, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](214, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](215, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](216, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](217, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](218, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](219, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](220, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](221, "img", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](222, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](223, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](224, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](225, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](226, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](227, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](228, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](229, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](230, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](231, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](232, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](233, "img", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](234, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](235, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](236, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](237, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](238, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](239, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](240, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](241, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](242, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](243, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](244, "img", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](245, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](246, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](247, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](248, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](249, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](250, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](251, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](252, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](253, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](254, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](255, "img", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](256, "span", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](257, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](258, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](259, "Brooklyn Simmons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](260, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](261, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](262, "a", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](263, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](264, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](265, "button", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](266, "button", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](267, "button", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.categories.category);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](ctx.categories.category);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind1"](19, 15, ctx.categories.category_description));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("src", ctx.categories.image_url, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("src", ctx.categories.image_url, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.subCategories);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("title", ctx.title)("slideArray", ctx.slides)("slidesConfig", ctx.slideConfig);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("title", ctx.title1)("slideArray", ctx.slides1)("slidesConfig", ctx.slideConfig1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("title", ctx.title2)("slideArray", ctx.slides2)("slidesConfig", ctx.slideConfig2);
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], _goone_slider_goone_slider_component__WEBPACK_IMPORTED_MODULE_5__["GooneSliderComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"]], pipes: [_shared_pipes_StripHtmltags__WEBPACK_IMPORTED_MODULE_6__["StripHtmltags"]], styles: [".goonee-talents-sec[_ngcontent-%COMP%] {\n  padding-left: 8%;\n  overflow: hidden;\n}\n\n@media (max-width: 575.98px) {\n  .goonee-talents-sec[_ngcontent-%COMP%] {\n    padding-left: 1.5rem;\n  }\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slick-list[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slick-list[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slick-list[_ngcontent-%COMP%] {\n  padding-right: 5rem;\n  padding-top: 2rem;\n}\n\n@media (max-width: 991.98px) {\n  .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slick-list[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slick-list[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slick-list[_ngcontent-%COMP%] {\n    padding-right: 3rem;\n  }\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%] {\n  margin: 1rem 2rem 1rem 0;\n  background-color: #fff;\n  border-radius: 0.6rem;\n  border-top-left-radius: 0;\n  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.2);\n  transition: all 0.5s ease-in-out;\n}\n\n@media (max-width: 991.98px) {\n  .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%] {\n    margin: 1rem 1rem 1rem 0;\n  }\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   img[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   img[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  width: 100%;\n  border-radius: 0.6rem;\n  border-top-left-radius: 0;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   h5[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .h5[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   h5[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .h5[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   h5[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .h5[_ngcontent-%COMP%] {\n  font-weight: 400;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .review-s[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .review-s[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .review-s[_ngcontent-%COMP%] {\n  margin-bottom: 0.7rem;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .review-s[_ngcontent-%COMP%]   i[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .review-s[_ngcontent-%COMP%]   i[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .review-s[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n  line-height: inherit;\n  color: #6C60E1;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .review-s[_ngcontent-%COMP%]   span[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .review-s[_ngcontent-%COMP%]   span[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .review-s[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  margin-left: 0.5rem;\n  color: #495057;\n  font-weight: 300;\n  transition: all 0.5s ease-in-out;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   p[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   p[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  color: #495057;\n  font-weight: 300;\n  margin-bottom: 0.7rem;\n  transition: all 0.5s ease-in-out;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .h2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .h2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]   .h2[_ngcontent-%COMP%] {\n  font-weight: 700;\n  font-size: 1.4rem;\n  margin-bottom: 1rem;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover, .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover, .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover {\n  background-color: #000;\n  color: #fff;\n  margin-top: -0.3rem;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover   .btn[_ngcontent-%COMP%] {\n  border-color: #fff;\n  background-color: #000;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover   p[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover   p[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover   p[_ngcontent-%COMP%] {\n  color: #fff;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover   .review-s[_ngcontent-%COMP%]   span[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover   .review-s[_ngcontent-%COMP%]   span[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .goonee-talents-card[_ngcontent-%COMP%]:hover   .review-s[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  color: #fff;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%] {\n  position: absolute;\n  right: 7%;\n  top: -45px;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slicknext[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slicknext1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slicknext2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slicknext[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slicknext1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slicknext2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slicknext[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slicknext1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slicknext2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slicknext[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slicknext1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slicknext2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slicknext[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slicknext1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slicknext2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slicknext[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slicknext1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slicknext2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slicknext[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slicknext1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slicknext2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slicknext[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slicknext1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slicknext2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slicknext[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slicknext1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slicknext2[_ngcontent-%COMP%] {\n  background-color: #fff;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slickprev[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slickprev1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slickprev2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slickprev[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slickprev1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slickprev2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slickprev[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slickprev1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slickprev2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slickprev[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slickprev1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slickprev2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slickprev[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slickprev1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slickprev2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slickprev[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slickprev1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slickprev2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slickprev[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slickprev1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .slickprev2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slickprev[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slickprev1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .slickprev2[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slickprev[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slickprev1[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .slickprev2[_ngcontent-%COMP%] {\n  background-color: #000;\n  color: #fff;\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n  padding: 0.6rem 1rem;\n  margin: 0 0.5rem;\n  box-shadow: 0 0 4px rgba(0, 0, 0, 0.1);\n}\n\n.goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]:hover, .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]:hover, .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]:hover, .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]:hover, .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]:hover, .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]:hover, .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]:hover, .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]:hover, .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]:hover {\n  background-color: #6C60E1;\n  color: #fff;\n}\n\n@media (max-width: 575.98px) {\n  .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-talents-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .trending-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows1[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%], .goonee-talents-sec[_ngcontent-%COMP%]   .goonee-rising-slider[_ngcontent-%COMP%]   .slickArrows2[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n    padding: 0.3rem 0.8rem;\n    margin: 0 0.3rem;\n  }\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%] {\n  margin: 1.2rem 0;\n  border: none;\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-header[_ngcontent-%COMP%]   .accordion-button[_ngcontent-%COMP%] {\n  background-color: rgba(108, 96, 225, 0.1);\n  border: none;\n  box-shadow: none !important;\n  border-radius: 0.6rem;\n  border-top-left-radius: 0;\n  font-weight: 700;\n  overflow: hidden;\n  padding-right: 70px;\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-header[_ngcontent-%COMP%]   .accordion-button[_ngcontent-%COMP%]::after {\n  background-color: #6C60E1;\n  background-image: url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23fff'%3e%3cpath fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/%3e%3c/svg%3e\");\n  position: absolute;\n  right: 0;\n  width: 70px;\n  height: 100%;\n  background-position: center;\n  border-bottom-left-radius: 0.6rem;\n  background-size: 1.6rem;\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-header[_ngcontent-%COMP%]   .accordion-button[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  height: 35px;\n  margin-right: 1rem;\n  fill: #6C60E1;\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-header[_ngcontent-%COMP%]   .accordion-button[aria-expanded=true][_ngcontent-%COMP%] {\n  background-color: #6C60E1;\n  color: #fff;\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-header[_ngcontent-%COMP%]   .accordion-button[aria-expanded=true][_ngcontent-%COMP%]::after {\n  background-color: #000;\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-header[_ngcontent-%COMP%]   .accordion-button[aria-expanded=true][_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  fill: #fff;\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-collapse[_ngcontent-%COMP%] {\n  padding-right: 70px;\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-collapse[_ngcontent-%COMP%]   .accordion-body[_ngcontent-%COMP%] {\n  margin-top: -0.6rem;\n  padding-top: 2rem;\n  border-bottom-left-radius: 0.6rem;\n  border-bottom-right-radius: 0.6rem;\n  background-color: rgba(108, 96, 225, 0.1);\n  color: #495057;\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-collapse[_ngcontent-%COMP%]   .accordion-body[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%], .accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-collapse[_ngcontent-%COMP%]   .accordion-body[_ngcontent-%COMP%]   .h6[_ngcontent-%COMP%] {\n  font-weight: 700;\n  color: #000;\n}\n\n.accordion[_ngcontent-%COMP%]   .accordion-item[_ngcontent-%COMP%]   .accordion-collapse[_ngcontent-%COMP%]   .accordion-body[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  font-weight: 300;\n  font-size: 1rem;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]   .user-circle[_ngcontent-%COMP%] {\n  position: relative;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]   .user-circle[_ngcontent-%COMP%]   img[_ngcontent-%COMP%] {\n  border-radius: 60px;\n  height: 60px;\n  width: 60px;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]   .user-circle[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  height: 15px;\n  width: 15px;\n  border-radius: 15px;\n  border: 2px solid #fff;\n  position: absolute;\n  bottom: 0;\n  right: 0px;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]   .user-circle[_ngcontent-%COMP%]   span.online[_ngcontent-%COMP%] {\n  background-color: #63BE6B;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]   .user-circle[_ngcontent-%COMP%]   span.offline[_ngcontent-%COMP%] {\n  background-color: #ccc;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]   .user-nd-text[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%], .goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]   .user-nd-text[_ngcontent-%COMP%]   .h6[_ngcontent-%COMP%] {\n  margin-bottom: 0.2rem;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]   .user-nd-text[_ngcontent-%COMP%]   small[_ngcontent-%COMP%], .goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]   .user-nd-text[_ngcontent-%COMP%]   .small[_ngcontent-%COMP%] {\n  color: #495057;\n  font-weight: 300;\n  transition: all 0.5s ease-in-out;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]:hover {\n  margin-top: -0.55rem;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]:hover   .user-circle[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  border-color: #000;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]:hover   .user-nd-text[_ngcontent-%COMP%]   small[_ngcontent-%COMP%], .goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]:hover   .user-nd-text[_ngcontent-%COMP%]   .small[_ngcontent-%COMP%] {\n  color: #fff;\n}\n\n.goonee-recent-active[_ngcontent-%COMP%]   .card-view[_ngcontent-%COMP%]:hover   .btn[_ngcontent-%COMP%] {\n  border-color: #fff;\n  background-color: #000;\n}\n\n.trending-topics[_ngcontent-%COMP%]   .slick-list[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  color: #6C60E1;\n  margin-right: 1rem;\n}\n\n.trending-topics[_ngcontent-%COMP%]   .slick-list[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\n  background-color: #000 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uL21haW4tY2F0ZWdvcnktZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsZ0JBQUE7RUFDQSxnQkFBQTtBQUFGOztBQUNBO0VBQ0U7SUFDRSxvQkFBQTtFQUVGO0FBQ0Y7O0FBRkE7OztFQUdFLG1CQUFBO0VBQ0EsaUJBQUE7QUFJRjs7QUFIQTtFQUNFOzs7SUFHRSxtQkFBQTtFQU1GO0FBQ0Y7O0FBTkE7OztFQUdFLHdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0VBQ0EsMENBQUE7RUFDQSxnQ0FBQTtBQVFGOztBQVBBO0VBQ0U7OztJQUdFLHdCQUFBO0VBVUY7QUFDRjs7QUFWQTs7O0VBR0UsV0FBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7QUFZRjs7QUFYQTs7Ozs7RUFLRSxnQkFBQTtBQWNGOztBQWJBOzs7RUFHRSxxQkFBQTtBQWdCRjs7QUFmQTs7O0VBR0Usb0JBQUE7RUFDQSxjQUFBO0FBa0JGOztBQWpCQTs7O0VBR0UsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQ0FBQTtBQW9CRjs7QUFuQkE7OztFQUdFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0NBQUE7QUFzQkY7O0FBckJBOzs7OztFQUtFLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQXdCRjs7QUF2QkE7OztFQUdFLHNCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FBMEJGOztBQXpCQTs7O0VBR0Usa0JBQUE7RUFDQSxzQkFBQTtBQTRCRjs7QUEzQkE7OztFQUdFLFdBQUE7QUE4QkY7O0FBN0JBOzs7RUFHRSxXQUFBO0FBZ0NGOztBQS9CQTs7Ozs7Ozs7O0VBU0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtBQWtDRjs7QUFqQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQTJCRSxzQkFBQTtBQW9DRjs7QUFuQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQTJCRSxzQkFBQTtFQUNBLFdBQUE7QUFzQ0Y7O0FBckNBOzs7Ozs7Ozs7RUFTRSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0Esc0NBQUE7QUF3Q0Y7O0FBdkNBOzs7Ozs7Ozs7RUFTRSx5QkFBQTtFQUNBLFdBQUE7QUEwQ0Y7O0FBekNBO0VBQ0U7Ozs7Ozs7OztJQVNFLHNCQUFBO0lBQ0EsZ0JBQUE7RUE0Q0Y7QUFDRjs7QUEzQ0E7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUE2Q0Y7O0FBNUNBO0VBQ0UseUNBQUE7RUFDQSxZQUFBO0VBQ0EsMkJBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBK0NGOztBQTlDQTtFQUNFLHlCQUFBO0VBQ0EsOFJBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLDJCQUFBO0VBQ0EsaUNBQUE7RUFDQSx1QkFBQTtBQWlERjs7QUFoREE7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0FBbURGOztBQWxEQTtFQUNFLHlCQUFBO0VBQ0EsV0FBQTtBQXFERjs7QUFwREE7RUFDRSxzQkFBQTtBQXVERjs7QUF0REE7RUFDRSxVQUFBO0FBeURGOztBQXhEQTtFQUNFLG1CQUFBO0FBMkRGOztBQTFEQTtFQUNFLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQ0FBQTtFQUNBLGtDQUFBO0VBQ0EseUNBQUE7RUFDQSxjQUFBO0FBNkRGOztBQTVEQTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBQStERjs7QUE5REE7RUFDRSxnQkFBQTtFQUNBLGVBQUE7QUFpRUY7O0FBL0RBO0VBQ0Usa0JBQUE7QUFrRUY7O0FBakVBO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQW9FRjs7QUFuRUE7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FBc0VGOztBQXJFQTtFQUNFLHlCQUFBO0FBd0VGOztBQXZFQTtFQUNFLHNCQUFBO0FBMEVGOztBQXhFQTtFQUNFLHFCQUFBO0FBMkVGOztBQXpFQTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGdDQUFBO0FBNEVGOztBQTFFQTtFQUNFLG9CQUFBO0FBNkVGOztBQTVFQTtFQUNFLGtCQUFBO0FBK0VGOztBQTlFQTtFQUNFLFdBQUE7QUFpRkY7O0FBaEZBO0VBQ0Usa0JBQUE7RUFDQSxzQkFBQTtBQW1GRjs7QUFqRkE7RUFDRSxjQUFBO0VBQ0Esa0JBQUE7QUFvRkY7O0FBbkZBO0VBQ0UsaUNBQUE7QUFzRkYiLCJmaWxlIjoibWFpbi1jYXRlZ29yeS1kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5nb29uZWUtdGFsZW50cy1zZWMge1xuICBwYWRkaW5nLWxlZnQ6IDglO1xuICBvdmVyZmxvdzogaGlkZGVuOyB9XG5AbWVkaWEgKG1heC13aWR0aDogNTc1Ljk4cHgpIHtcbiAgLmdvb25lZS10YWxlbnRzLXNlYyB7XG4gICAgcGFkZGluZy1sZWZ0OiAxLjVyZW07IH0gfVxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGljay1saXN0LFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGljay1saXN0LFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLnNsaWNrLWxpc3Qge1xuICBwYWRkaW5nLXJpZ2h0OiA1cmVtO1xuICBwYWRkaW5nLXRvcDogMnJlbTsgfVxuQG1lZGlhIChtYXgtd2lkdGg6IDk5MS45OHB4KSB7XG4gIC5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2stbGlzdCxcbiAgLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGljay1saXN0LFxuICAuZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2stbGlzdCB7XG4gICAgcGFkZGluZy1yaWdodDogM3JlbTsgfSB9XG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZCB7XG4gIG1hcmdpbjogMXJlbSAycmVtIDFyZW0gMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm9yZGVyLXJhZGl1czogMC42cmVtO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwO1xuICBib3gtc2hhZG93OiAwcHggMnB4IDRweCByZ2JhKDAsIDAsIDAsIDAuMik7XG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UtaW4tb3V0OyB9XG5AbWVkaWEgKG1heC13aWR0aDogOTkxLjk4cHgpIHtcbiAgLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkLFxuICAuZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQsXG4gIC5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkIHtcbiAgICBtYXJnaW46IDFyZW0gMXJlbSAxcmVtIDA7IH0gfVxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkIGltZyxcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZCBpbWcsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZCBpbWcge1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyLXJhZGl1czogMC42cmVtO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwOyB9XG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgaDUsIC5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZCAuaDUsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgaDUsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgLmg1LFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgaDUsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZCAuaDUge1xuICBmb250LXdlaWdodDogNDAwOyB9XG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgLnJldmlldy1zLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkIC5yZXZpZXctcyxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkIC5yZXZpZXctcyB7XG4gIG1hcmdpbi1ib3R0b206IDAuN3JlbTsgfVxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkIC5yZXZpZXctcyBpLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkIC5yZXZpZXctcyBpLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgLnJldmlldy1zIGkge1xuICBsaW5lLWhlaWdodDogaW5oZXJpdDtcbiAgY29sb3I6ICM2QzYwRTE7IH1cbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZCAucmV2aWV3LXMgc3Bhbixcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZCAucmV2aWV3LXMgc3Bhbixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkIC5yZXZpZXctcyBzcGFuIHtcbiAgbWFyZ2luLWxlZnQ6IDAuNXJlbTtcbiAgY29sb3I6ICM0OTUwNTc7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UtaW4tb3V0OyB9XG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgcCxcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZCBwLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgcCB7XG4gIGNvbG9yOiAjNDk1MDU3O1xuICBmb250LXdlaWdodDogMzAwO1xuICBtYXJnaW4tYm90dG9tOiAwLjdyZW07XG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UtaW4tb3V0OyB9XG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgaDIsIC5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZCAuaDIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgaDIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgLmgyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQgaDIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZCAuaDIge1xuICBmb250LXdlaWdodDogNzAwO1xuICBmb250LXNpemU6IDEuNHJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTsgfVxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkOmhvdmVyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkOmhvdmVyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQ6aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xuICBjb2xvcjogI2ZmZjtcbiAgbWFyZ2luLXRvcDogLTAuM3JlbTsgfVxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkOmhvdmVyIC5idG4sXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLmdvb25lZS10YWxlbnRzLWNhcmQ6aG92ZXIgLmJ0bixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkOmhvdmVyIC5idG4ge1xuICBib3JkZXItY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7IH1cbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZDpob3ZlciBwLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkOmhvdmVyIHAsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZDpob3ZlciBwIHtcbiAgY29sb3I6ICNmZmY7IH1cbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZDpob3ZlciAucmV2aWV3LXMgc3Bhbixcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuZ29vbmVlLXRhbGVudHMtY2FyZDpob3ZlciAucmV2aWV3LXMgc3Bhbixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5nb29uZWUtdGFsZW50cy1jYXJkOmhvdmVyIC5yZXZpZXctcyBzcGFuIHtcbiAgY29sb3I6ICNmZmY7IH1cbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2tBcnJvd3MsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLnNsaWNrQXJyb3dzMSxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2tBcnJvd3MyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGlja0Fycm93cyxcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MxLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGlja0Fycm93czIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MxLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLnNsaWNrQXJyb3dzMiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDclO1xuICB0b3A6IC00NXB4OyB9XG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5zbGlja25leHQsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5zbGlja25leHQxLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93cyAuc2xpY2tuZXh0Mixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5zbGlja25leHQsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLnNsaWNrQXJyb3dzMSAuc2xpY2tuZXh0MSxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5zbGlja25leHQyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93czIgLnNsaWNrbmV4dCxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2tBcnJvd3MyIC5zbGlja25leHQxLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93czIgLnNsaWNrbmV4dDIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5zbGlja25leHQsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5zbGlja25leHQxLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGlja0Fycm93cyAuc2xpY2tuZXh0Mixcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5zbGlja25leHQsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzMSAuc2xpY2tuZXh0MSxcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5zbGlja25leHQyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGlja0Fycm93czIgLnNsaWNrbmV4dCxcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MyIC5zbGlja25leHQxLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGlja0Fycm93czIgLnNsaWNrbmV4dDIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MgLnNsaWNrbmV4dCxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93cyAuc2xpY2tuZXh0MSxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93cyAuc2xpY2tuZXh0Mixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93czEgLnNsaWNrbmV4dCxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93czEgLnNsaWNrbmV4dDEsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5zbGlja25leHQyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLnNsaWNrQXJyb3dzMiAuc2xpY2tuZXh0LFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLnNsaWNrQXJyb3dzMiAuc2xpY2tuZXh0MSxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93czIgLnNsaWNrbmV4dDIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmOyB9XG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5zbGlja3ByZXYsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5zbGlja3ByZXYxLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93cyAuc2xpY2twcmV2Mixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5zbGlja3ByZXYsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLnNsaWNrQXJyb3dzMSAuc2xpY2twcmV2MSxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5zbGlja3ByZXYyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93czIgLnNsaWNrcHJldixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2tBcnJvd3MyIC5zbGlja3ByZXYxLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93czIgLnNsaWNrcHJldjIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5zbGlja3ByZXYsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5zbGlja3ByZXYxLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGlja0Fycm93cyAuc2xpY2twcmV2Mixcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5zbGlja3ByZXYsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzMSAuc2xpY2twcmV2MSxcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5zbGlja3ByZXYyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGlja0Fycm93czIgLnNsaWNrcHJldixcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MyIC5zbGlja3ByZXYxLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGlja0Fycm93czIgLnNsaWNrcHJldjIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MgLnNsaWNrcHJldixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93cyAuc2xpY2twcmV2MSxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93cyAuc2xpY2twcmV2Mixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93czEgLnNsaWNrcHJldixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93czEgLnNsaWNrcHJldjEsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5zbGlja3ByZXYyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLnNsaWNrQXJyb3dzMiAuc2xpY2twcmV2LFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLnNsaWNrQXJyb3dzMiAuc2xpY2twcmV2MSxcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93czIgLnNsaWNrcHJldjIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xuICBjb2xvcjogI2ZmZjsgfVxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93cyAuYnRuLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93czEgLmJ0bixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2tBcnJvd3MyIC5idG4sXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5idG4sXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzMSAuYnRuLFxuLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGlja0Fycm93czIgLmJ0bixcbi5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS1yaXNpbmctc2xpZGVyIC5zbGlja0Fycm93cyAuYnRuLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLnNsaWNrQXJyb3dzMSAuYnRuLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLnNsaWNrQXJyb3dzMiAuYnRuIHtcbiAgcGFkZGluZzogMC42cmVtIDFyZW07XG4gIG1hcmdpbjogMCAwLjVyZW07XG4gIGJveC1zaGFkb3c6IDAgMCA0cHggcmdiYSgwLCAwLCAwLCAwLjEpOyB9XG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5idG46aG92ZXIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtdGFsZW50cy1zbGlkZXIgLnNsaWNrQXJyb3dzMSAuYnRuOmhvdmVyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93czIgLmJ0bjpob3Zlcixcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MgLmJ0bjpob3Zlcixcbi5nb29uZWUtdGFsZW50cy1zZWMgLnRyZW5kaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5idG46aG92ZXIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzMiAuYnRuOmhvdmVyLFxuLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLnNsaWNrQXJyb3dzIC5idG46aG92ZXIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MxIC5idG46aG92ZXIsXG4uZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MyIC5idG46aG92ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNkM2MEUxO1xuICBjb2xvcjogI2ZmZjsgfVxuQG1lZGlhIChtYXgtd2lkdGg6IDU3NS45OHB4KSB7XG4gIC5nb29uZWUtdGFsZW50cy1zZWMgLmdvb25lZS10YWxlbnRzLXNsaWRlciAuc2xpY2tBcnJvd3MgLmJ0bixcbiAgLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93czEgLmJ0bixcbiAgLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXRhbGVudHMtc2xpZGVyIC5zbGlja0Fycm93czIgLmJ0bixcbiAgLmdvb25lZS10YWxlbnRzLXNlYyAudHJlbmRpbmctc2xpZGVyIC5zbGlja0Fycm93cyAuYnRuLFxuICAuZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzMSAuYnRuLFxuICAuZ29vbmVlLXRhbGVudHMtc2VjIC50cmVuZGluZy1zbGlkZXIgLnNsaWNrQXJyb3dzMiAuYnRuLFxuICAuZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MgLmJ0bixcbiAgLmdvb25lZS10YWxlbnRzLXNlYyAuZ29vbmVlLXJpc2luZy1zbGlkZXIgLnNsaWNrQXJyb3dzMSAuYnRuLFxuICAuZ29vbmVlLXRhbGVudHMtc2VjIC5nb29uZWUtcmlzaW5nLXNsaWRlciAuc2xpY2tBcnJvd3MyIC5idG4ge1xuICAgIHBhZGRpbmc6IDAuM3JlbSAwLjhyZW07XG4gICAgbWFyZ2luOiAwIDAuM3JlbTsgfSB9XG5cbi5hY2NvcmRpb24gLmFjY29yZGlvbi1pdGVtIHtcbiAgbWFyZ2luOiAxLjJyZW0gMDtcbiAgYm9yZGVyOiBub25lOyB9XG4uYWNjb3JkaW9uIC5hY2NvcmRpb24taXRlbSAuYWNjb3JkaW9uLWhlYWRlciAuYWNjb3JkaW9uLWJ1dHRvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMTA4LCA5NiwgMjI1LCAwLjEpO1xuICBib3JkZXI6IG5vbmU7XG4gIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogMC42cmVtO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwO1xuICBmb250LXdlaWdodDogNzAwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBwYWRkaW5nLXJpZ2h0OiA3MHB4OyB9XG4uYWNjb3JkaW9uIC5hY2NvcmRpb24taXRlbSAuYWNjb3JkaW9uLWhlYWRlciAuYWNjb3JkaW9uLWJ1dHRvbjo6YWZ0ZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNkM2MEUxO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJkYXRhOmltYWdlL3N2Zyt4bWwsJTNjc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgdmlld0JveD0nMCAwIDE2IDE2JyBmaWxsPSclMjNmZmYnJTNlJTNjcGF0aCBmaWxsLXJ1bGU9J2V2ZW5vZGQnIGQ9J00xLjY0NiA0LjY0NmEuNS41IDAgMCAxIC43MDggMEw4IDEwLjI5M2w1LjY0Ni01LjY0N2EuNS41IDAgMCAxIC43MDguNzA4bC02IDZhLjUuNSAwIDAgMS0uNzA4IDBsLTYtNmEuNS41IDAgMCAxIDAtLjcwOHonLyUzZSUzYy9zdmclM2VcIik7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDA7XG4gIHdpZHRoOiA3MHB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMC42cmVtO1xuICBiYWNrZ3JvdW5kLXNpemU6IDEuNnJlbTsgfVxuLmFjY29yZGlvbiAuYWNjb3JkaW9uLWl0ZW0gLmFjY29yZGlvbi1oZWFkZXIgLmFjY29yZGlvbi1idXR0b24gc3ZnIHtcbiAgaGVpZ2h0OiAzNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDFyZW07XG4gIGZpbGw6ICM2QzYwRTE7IH1cbi5hY2NvcmRpb24gLmFjY29yZGlvbi1pdGVtIC5hY2NvcmRpb24taGVhZGVyIC5hY2NvcmRpb24tYnV0dG9uW2FyaWEtZXhwYW5kZWQ9XCJ0cnVlXCJdIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzZDNjBFMTtcbiAgY29sb3I6ICNmZmY7IH1cbi5hY2NvcmRpb24gLmFjY29yZGlvbi1pdGVtIC5hY2NvcmRpb24taGVhZGVyIC5hY2NvcmRpb24tYnV0dG9uW2FyaWEtZXhwYW5kZWQ9XCJ0cnVlXCJdOjphZnRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7IH1cbi5hY2NvcmRpb24gLmFjY29yZGlvbi1pdGVtIC5hY2NvcmRpb24taGVhZGVyIC5hY2NvcmRpb24tYnV0dG9uW2FyaWEtZXhwYW5kZWQ9XCJ0cnVlXCJdIHN2ZyB7XG4gIGZpbGw6ICNmZmY7IH1cbi5hY2NvcmRpb24gLmFjY29yZGlvbi1pdGVtIC5hY2NvcmRpb24tY29sbGFwc2Uge1xuICBwYWRkaW5nLXJpZ2h0OiA3MHB4OyB9XG4uYWNjb3JkaW9uIC5hY2NvcmRpb24taXRlbSAuYWNjb3JkaW9uLWNvbGxhcHNlIC5hY2NvcmRpb24tYm9keSB7XG4gIG1hcmdpbi10b3A6IC0wLjZyZW07XG4gIHBhZGRpbmctdG9wOiAycmVtO1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAwLjZyZW07XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwLjZyZW07XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMTA4LCA5NiwgMjI1LCAwLjEpO1xuICBjb2xvcjogIzQ5NTA1NzsgfVxuLmFjY29yZGlvbiAuYWNjb3JkaW9uLWl0ZW0gLmFjY29yZGlvbi1jb2xsYXBzZSAuYWNjb3JkaW9uLWJvZHkgaDYsIC5hY2NvcmRpb24gLmFjY29yZGlvbi1pdGVtIC5hY2NvcmRpb24tY29sbGFwc2UgLmFjY29yZGlvbi1ib2R5IC5oNiB7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGNvbG9yOiAjMDAwOyB9XG4uYWNjb3JkaW9uIC5hY2NvcmRpb24taXRlbSAuYWNjb3JkaW9uLWNvbGxhcHNlIC5hY2NvcmRpb24tYm9keSBwIHtcbiAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgZm9udC1zaXplOiAxcmVtOyB9XG5cbi5nb29uZWUtcmVjZW50LWFjdGl2ZSAuY2FyZC12aWV3IC51c2VyLWNpcmNsZSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTsgfVxuLmdvb25lZS1yZWNlbnQtYWN0aXZlIC5jYXJkLXZpZXcgLnVzZXItY2lyY2xlIGltZyB7XG4gIGJvcmRlci1yYWRpdXM6IDYwcHg7XG4gIGhlaWdodDogNjBweDtcbiAgd2lkdGg6IDYwcHg7IH1cbi5nb29uZWUtcmVjZW50LWFjdGl2ZSAuY2FyZC12aWV3IC51c2VyLWNpcmNsZSBzcGFuIHtcbiAgaGVpZ2h0OiAxNXB4O1xuICB3aWR0aDogMTVweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgYm9yZGVyOiAycHggc29saWQgI2ZmZjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAwcHg7IH1cbi5nb29uZWUtcmVjZW50LWFjdGl2ZSAuY2FyZC12aWV3IC51c2VyLWNpcmNsZSBzcGFuLm9ubGluZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM2M0JFNkI7IH1cbi5nb29uZWUtcmVjZW50LWFjdGl2ZSAuY2FyZC12aWV3IC51c2VyLWNpcmNsZSBzcGFuLm9mZmxpbmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2NjOyB9XG5cbi5nb29uZWUtcmVjZW50LWFjdGl2ZSAuY2FyZC12aWV3IC51c2VyLW5kLXRleHQgaDYsIC5nb29uZWUtcmVjZW50LWFjdGl2ZSAuY2FyZC12aWV3IC51c2VyLW5kLXRleHQgLmg2IHtcbiAgbWFyZ2luLWJvdHRvbTogMC4ycmVtOyB9XG5cbi5nb29uZWUtcmVjZW50LWFjdGl2ZSAuY2FyZC12aWV3IC51c2VyLW5kLXRleHQgc21hbGwsIC5nb29uZWUtcmVjZW50LWFjdGl2ZSAuY2FyZC12aWV3IC51c2VyLW5kLXRleHQgLnNtYWxsIHtcbiAgY29sb3I6ICM0OTUwNTc7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIHRyYW5zaXRpb246IGFsbCAwLjVzIGVhc2UtaW4tb3V0OyB9XG5cbi5nb29uZWUtcmVjZW50LWFjdGl2ZSAuY2FyZC12aWV3OmhvdmVyIHtcbiAgbWFyZ2luLXRvcDogLTAuNTVyZW07IH1cbi5nb29uZWUtcmVjZW50LWFjdGl2ZSAuY2FyZC12aWV3OmhvdmVyIC51c2VyLWNpcmNsZSBzcGFuIHtcbiAgYm9yZGVyLWNvbG9yOiAjMDAwOyB9XG4uZ29vbmVlLXJlY2VudC1hY3RpdmUgLmNhcmQtdmlldzpob3ZlciAudXNlci1uZC10ZXh0IHNtYWxsLCAuZ29vbmVlLXJlY2VudC1hY3RpdmUgLmNhcmQtdmlldzpob3ZlciAudXNlci1uZC10ZXh0IC5zbWFsbCB7XG4gIGNvbG9yOiAjZmZmOyB9XG4uZ29vbmVlLXJlY2VudC1hY3RpdmUgLmNhcmQtdmlldzpob3ZlciAuYnRuIHtcbiAgYm9yZGVyLWNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwOyB9XG5cbi50cmVuZGluZy10b3BpY3MgLnNsaWNrLWxpc3QgYSB7XG4gIGNvbG9yOiAjNkM2MEUxO1xuICBtYXJnaW4tcmlnaHQ6IDFyZW07IH1cbi50cmVuZGluZy10b3BpY3MgLnNsaWNrLWxpc3QgYTpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAgIWltcG9ydGFudDsgfVxuXG4iXX0= */"] });


/***/ }),

/***/ "7hes":
/*!*********************************************************************************!*\
  !*** ./src/app/components/site/userauthentication/userauthentication.module.ts ***!
  \*********************************************************************************/
/*! exports provided: UserauthenticationModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserauthenticationModule", function() { return UserauthenticationModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "tMgt");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signup/signup.component */ "t2kx");
/* harmony import */ var ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-intl-tel-input */ "t34c");
/* harmony import */ var _forgetpassword_forgetpassword_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./forgetpassword/forgetpassword.component */ "u8BJ");
/* harmony import */ var _changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./changepassword/changepassword.component */ "s4QN");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ "fXoL");









class UserauthenticationModule {
}
UserauthenticationModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdefineNgModule"]({ type: UserauthenticationModule });
UserauthenticationModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵdefineInjector"]({ factory: function UserauthenticationModule_Factory(t) { return new (t || UserauthenticationModule)(); }, imports: [[
            _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
            ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_5__["NgxIntlTelInputModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_8__["ɵɵsetNgModuleScope"](UserauthenticationModule, { declarations: [_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"], _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__["SignupComponent"], _forgetpassword_forgetpassword_component__WEBPACK_IMPORTED_MODULE_6__["ForgetpasswordComponent"], _changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_7__["ChangepasswordComponent"]], imports: [_angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
        _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
        ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_5__["NgxIntlTelInputModule"]] }); })();


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    apiUrl: 'https://dev.goonelive.com/api/v1',
    fbAppId: '628383398522695,',
    serverUrl: 'https://dev.goonelive.com/'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "Bh+D":
/*!***********************************************!*\
  !*** ./src/app/shared/pipes/StripHtmltags.ts ***!
  \***********************************************/
/*! exports provided: StripHtmltags */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StripHtmltags", function() { return StripHtmltags; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class StripHtmltags {
    transform(value) {
        return value.replace(/<.*?>/g, ''); // replace tags
    }
}
StripHtmltags.ɵfac = function StripHtmltags_Factory(t) { return new (t || StripHtmltags)(); };
StripHtmltags.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({ name: "striphtml", type: StripHtmltags, pure: true });


/***/ }),

/***/ "FBdK":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/site/userauthentication/signup/unique-email-validator.service.ts ***!
  \*********************************************************************************************/
/*! exports provided: CustomValidationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomValidationService", function() { return CustomValidationService; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../environments/environment */ "AytR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");




class CustomValidationService {
    constructor(http) {
        this.http = http;
    }
    validateEmailNotTaken(control) {
        return this.checkEmailNotTaken(control.value).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(res => {
            return res ? null : { emailTaken: true };
        }));
    }
    checkEmailNotTaken(email) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl + '/check_email').pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])((Users) => Users.filter(user => user.email === email)), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])(users => !users.length));
    }
}
CustomValidationService.ɵfac = function CustomValidationService_Factory(t) { return new (t || CustomValidationService)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"])); };
CustomValidationService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: CustomValidationService, factory: CustomValidationService.ɵfac, providedIn: "root" });


/***/ }),

/***/ "H3jT":
/*!**************************************************************!*\
  !*** ./src/app/components/layout/footer/footer.component.ts ***!
  \**************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");


class FooterComponent {
    constructor() { }
    ngOnInit() {
    }
}
FooterComponent.ɵfac = function FooterComponent_Factory(t) { return new (t || FooterComponent)(); };
FooterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FooterComponent, selectors: [["app-footer"]], decls: 61, vars: 0, consts: [[1, "bg-dark"], [1, "container", "pt-lg-5", "pb-lg-5", "pt-4", "pb-4", "text-white"], [1, "row", "px-3", "px-sm-0"], [1, "col-md-3", "mt-3", "mb-3"], [1, "footer-links"], ["href", "#"], ["routerLink", "/about-us"], [1, "d-flex", "flex-wrap"], ["src", "assets/images/goonee-ios-store-icon.svg", "alt", "goonee-ios-store", 1, "mw-100", "mb-2", "me-2"], ["src", "assets/images/goonee-android-store-icon.svg", "alt", "goonee-android-store", 1, "mw-100", "mb-2", "me-2"], [1, "follow-us", "d-flex", "mt-3"], [1, "mb-0"], [1, "ms-3"], [1, "fab", "fa-facebook-f"], [1, "fab", "fa-instagram"], [1, "fab", "fa-twitter"], [1, "fab", "fa-linkedin-in"], [1, "footer-copyright", "bg-white", "pt-3", "pb-3", "text-center"], [1, "container"], [1, "fw-bold"]], template: function FooterComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "footer", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "About Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Contact Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "FAQ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Why Goonee?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Community");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Blog");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Community Guidelines");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "User Safety");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Non - Descrimination Policy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Legal");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Privacy Policy");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Service Terms");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Payment Terms");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Independent Tutor Agreement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "h6", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Follow Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](48, "i", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](50, "i", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "i", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](54, "i", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "b", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Goonee Live");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, " \u00A9 2021 All Rights Reserved | Designed & Developed by huptech web pvt.ltd ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmb290ZXIuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "PfYf":
/*!**********************************************************************!*\
  !*** ./src/app/components/site/pages/category/category.component.ts ***!
  \**********************************************************************/
/*! exports provided: CategoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryComponent", function() { return CategoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_category_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/services/category.service */ "fH6q");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _goone_slider_goone_slider_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../goone-slider/goone-slider.component */ "lmtY");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");





function CategoryComponent_div_37_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const category_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("routerLink", category_r1.category_alias);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", category_r1.image_url, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("alt", category_r1.category);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](category_r1.category);
} }
class CategoryComponent {
    constructor(_catService) {
        this._catService = _catService;
        this.title = "New Talents";
        this.categories = [];
        this.slides = [
            { img: "assets/images/goonee-talents-1.jpg" },
            { img: "assets/images/goonee-talents-2.jpg" },
            { img: "assets/images/goonee-talents-3.jpg" },
            { img: "assets/images/goonee-talents-4.jpg" },
            { img: "assets/images/goonee-talents-5.jpg" }
        ];
        this.slideConfig = { "slidesToShow": 4, "slidesToScroll": 4,
            "nextArrow": "<div class='slickArrows next'><span class=\"slicknext btn\"><i class=\"fas fa-arrow-left\"></i></span></div>",
            "prevArrow": "<div class='slickArrows prev'><span class=\"slickprev btn\"><i class=\"fas fa-arrow-right\"></i></span></div>"
        };
    }
    ngOnInit() {
        this._catService.getCategories()
            .subscribe((data) => {
            this.categories = data.data;
        });
    }
}
CategoryComponent.ɵfac = function CategoryComponent_Factory(t) { return new (t || CategoryComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shared_services_category_service__WEBPACK_IMPORTED_MODULE_1__["CategoryService"])); };
CategoryComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: CategoryComponent, selectors: [["app-category"]], decls: 42, vars: 4, consts: [[1, "hero-banner"], [1, "hero-caption"], [1, "container"], [1, "col-lg-5"], [1, "mt-4"], ["href", "#", 1, "btn", "btn-primary", "me-3"], [1, "col-lg-6", "offset-lg-6", "p-0"], ["src", "assets/images/goonee-category-banner.jpg", "alt", "goonee_category_banner", 1, "w-100", "d-none", "d-lg-block"], ["src", "assets/images/goonee-category-banner-mobile.jpg", "alt", "goonee_category_banner", 1, "w-100", "d-block", "d-lg-none"], [1, "goonee-cats", "py-5"], [1, "container", "px-4", "px-sm-0"], [1, "d-flex", "flex-wrap", "justify-content-between"], [1, "fw-bold"], [1, "cate-filter", "d-flex", "flex-wrap"], [1, "border", "d-flex", "order-sm-0", "order-1"], ["type", "search", "name", "s", "placeholder", "Search category", 1, "form-control", "border-0"], ["type", "submit", 1, "btn", "btn-dark"], ["width", "28", "height", "29", "viewBox", "0 0 28 29", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["d", "M24.0084 22.7677L20.0987 18.8695C21.3601 17.2624 22.0446 15.2779 22.042 13.2349C22.042 11.4155 21.5025 9.63689 20.4917 8.12407C19.4808 6.61126 18.0441 5.43217 16.3632 4.7359C14.6822 4.03963 12.8326 3.85745 11.0481 4.21241C9.26359 4.56737 7.62444 5.44351 6.3379 6.73005C5.05136 8.01659 4.17521 9.65574 3.82026 11.4402C3.4653 13.2247 3.64748 15.0744 4.34375 16.7553C5.04002 18.4363 6.21911 19.873 7.73192 20.8838C9.24474 21.8946 11.0233 22.4342 12.8428 22.4342C14.8858 22.4367 16.8703 21.7523 18.4773 20.4908L22.3755 24.4005C22.4824 24.5083 22.6096 24.5938 22.7497 24.6522C22.8898 24.7106 23.0401 24.7407 23.1919 24.7407C23.3437 24.7407 23.494 24.7106 23.6342 24.6522C23.7743 24.5938 23.9015 24.5083 24.0084 24.4005C24.1161 24.2936 24.2017 24.1664 24.2601 24.0263C24.3185 23.8862 24.3485 23.7359 24.3485 23.5841C24.3485 23.4323 24.3185 23.282 24.2601 23.1419C24.2017 23.0017 24.1161 22.8745 24.0084 22.7677ZM5.94331 13.2349C5.94331 11.8703 6.34796 10.5364 7.10608 9.40178C7.8642 8.26718 8.94175 7.38286 10.2025 6.86065C11.4632 6.33845 12.8504 6.20182 14.1888 6.46804C15.5271 6.73425 16.7565 7.39136 17.7214 8.35627C18.6863 9.32117 19.3434 10.5505 19.6096 11.8889C19.8759 13.2273 19.7392 14.6145 19.217 15.8752C18.6948 17.1359 17.8105 18.2135 16.6759 18.9716C15.5413 19.7297 14.2073 20.1344 12.8428 20.1344C11.0129 20.1344 9.25801 19.4075 7.96412 18.1136C6.67022 16.8197 5.94331 15.0648 5.94331 13.2349Z", "fill", "white"], [1, "ms-4", "dropdown", "order-sm-1", "order-0"], ["type", "button", "id", "catfiltr", "data-bs-toggle", "dropdown", "aria-expanded", "false", 1, "border", "px-3", "bg-white", "dropdown-toggle"], ["aria-labelledby", "catfiltr", 1, "dropdown-menu"], ["href", "#", 1, "dropdown-item"], [1, "row", "gx-lg-4", "gx-3", "py-4", "py-md-5"], ["class", "col-md-2 col-sm-4 col-6 mt-3 mb-4", 4, "ngFor", "ngForOf"], [1, "text-center"], ["href", "#", 1, "btn", "btn-outline-dark", "px-md-5", "fw-bold"], [3, "title", "slideArray", "slidesConfig"], [1, "col-md-2", "col-sm-4", "col-6", "mt-3", "mb-4"], [1, "goonee-cat-card", 3, "routerLink"], [3, "src", "alt"]], template: function CategoryComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Infinite Possibilities");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna lacus, quam nisi sed arcu. Imperdiet cursus nulla at.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Get Started");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "img", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "h2", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Categories");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "svg", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "path", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, " By: Top Categories");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "ul", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Top Categories");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "A to Z");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](37, CategoryComponent_div_37_Template, 5, 4, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "a", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Explore More");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "app-goone-slider", 27);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.categories);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("title", ctx.title)("slideArray", ctx.slides)("slidesConfig", ctx.slideConfig);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], _goone_slider_goone_slider_component__WEBPACK_IMPORTED_MODULE_3__["GooneSliderComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjYXRlZ29yeS5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "Qfl8":
/*!**************************************************************!*\
  !*** ./src/app/components/site/pages/home/home.component.ts ***!
  \**************************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_loading_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/services/loading.service */ "gw8Y");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-slick-carousel */ "eSVu");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");





function HomeComponent_a_157_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 77);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 78);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const slide_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", slide_r2.img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
class HomeComponent {
    constructor(_loading) {
        this._loading = _loading;
        this.slideArray = [
            { img: "assets/images/goonee-slide-1.jpg" },
            { img: "assets/images/goonee-slide-2.jpg" },
            { img: "assets/images/goonee-slide-3.jpg" },
            { img: "assets/images/goonee-slide-1.jpg" },
            { img: "assets/images/goonee-slide-2.jpg" },
            { img: "assets/images/goonee-slide-3.jpg" },
        ];
        this.slidesConfig = {
            "slidesToShow": 1,
            "slidesToScroll": 4,
            "vertical": true,
            "verticalSwiping": true,
            "dots": true,
            "arrows": true,
            "infinite": true,
            "nextArrow": "<div class='slickArrows'><span class=\"slicknext slick-arrow\" style=\"display: inline;\">Swipe Down <i class=\"fas fa-arrow-left\"></i></span></div>",
            "prevArrow": "<div class='slickArrows1'><span class=\"slickprev slick-arrow\" style=\"display: inline;\"><i class=\"fas fa-arrow-right\"></i> Swipe Up</span></div>"
        };
    }
    ngOnInit() {
        this._loading.setLoading(true, 'Lazy Load');
    }
    ngAfterContentInit() {
        setTimeout(() => {
            this._loading.setLoading(false, 'Lazy Load');
        }, 500);
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shared_services_loading_service__WEBPACK_IMPORTED_MODULE_1__["LoadingService"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 190, vars: 2, consts: [[1, "hero-banner"], [1, "hero-caption"], [1, "container"], [1, "col-lg-5"], [1, "d-flex", "flex-wrap", "herobtns", "justify-content-lg-start", "justify-content-center", "mt-4"], ["routerLink", "/learn", 1, "btn", "btn-primary", "me-3"], ["routerLink", "/be-a-tutor", 1, "btn", "btn-secondary", "text-white"], [1, "col-lg-6", "offset-lg-6", "p-0"], ["src", "assets/images/goonee-home-banner.jpg", "alt", "goonee_home_banner", 1, "w-100", "d-none", "d-lg-block"], ["src", "assets/images/goonee-home-banner-mobile.png", "alt", "goonee_home_banner", 1, "w-100", "d-block", "d-lg-none"], [1, "goonee-cards", "py-5"], [1, "container", "my-md-4"], [1, "col-lg-10", "offset-lg-1", "d-none", "d-sm-block"], [1, "row", "gx-md-5", "gx-3", "px-5"], [1, "col"], ["href", "#", 1, "card-view", "px-4", "py-5", "d-block"], ["src", "assets/images/goonee-icon-1.png", "alt", "goonee-Webinars"], ["src", "assets/images/goonee-icon-2.png", "alt", "goonee-1to1-Class"], ["src", "assets/images/goonee-icon-3.png", "alt", "goonee-Courses"], ["id", "carouselGooneeOne", "data-bs-ride", "carousel", 1, "carousel", "slide", "d-block", "d-sm-none"], [1, "carousel-inner"], [1, "carousel-item", "active", "p-3"], [1, "carousel-item", "p-3"], [1, "carousel-indicators"], ["type", "button", "data-bs-target", "#carouselGooneeOne", "data-bs-slide-to", "0", "aria-current", "true", "aria-label", "Slide 1", 1, "active"], ["type", "button", "data-bs-target", "#carouselGooneeOne", "data-bs-slide-to", "1", "aria-label", "Slide 2"], ["type", "button", "data-bs-target", "#carouselGooneeOne", "data-bs-slide-to", "2", "aria-label", "Slide 3"], [1, "goonee-cats", "bg-dark", "text-white", "py-5"], [1, "container", "py-3", "px-4", "px-sm-0"], [1, "text-center", "mb-4", "fw-bold"], [1, "row", "IP-row-one", "gx-lg-4", "gx-3", "py-4", "py-md-5"], [1, "col-md-2", "col-sm-4", "col-6"], ["href", "#", 1, "goonee-cat-card", "mb-4"], ["src", "assets/images/goonee-cat1.png", "alt", "The Arts"], ["src", "assets/images/goonee-cat2.png", "alt", "Music"], ["src", "assets/images/goonee-cat3.png", "alt", "Auto"], ["src", "assets/images/goonee-cat4.png", "alt", "Business"], ["src", "assets/images/goonee-cat5.png", "alt", "Life Style"], ["src", "assets/images/goonee-cat6.png", "alt", "Fitness"], [1, "btn-row", "d-flex", "align-items-center", "justify-content-between"], ["href", "#", 1, "btn", "btn-secondary", "text-white", "px-lg-5", "px-2", "py-md-3", "py-2", "d-flex", "align-items-center"], ["viewBox", "0 0 22 47", "xmlns", "http://www.w3.org/2000/svg", 1, "me-3", "d-none", "d-sm-block"], ["d", "M6.80666 23.6021C6.80207 27.0696 7.48229 30.5039 8.80827 33.7079C10.1343 36.9119 12.0799 39.8225 14.5336 42.2726L10.4192 46.387C4.37932 40.3384 0.988195 32.1391 0.991091 23.5912C0.993986 15.0433 4.39067 6.84633 10.4346 0.801758L14.5502 4.91737C12.0916 7.36767 10.1418 10.2799 8.81281 13.4866C7.48387 16.6932 6.80208 20.131 6.80666 23.6021Z"], ["d", "M16.2159 23.6023C16.213 25.8333 16.652 28.0428 17.5075 30.1033C18.363 32.1638 19.6181 34.0345 21.2004 35.6074L17.0937 39.714C14.9715 37.5998 13.2879 35.087 12.1396 32.3202C10.9913 29.5534 10.4009 26.587 10.4023 23.5914C10.4038 20.5957 10.997 17.6299 12.148 14.8641C13.2989 12.0984 14.9849 9.58734 17.1091 7.4751L21.2158 11.5818C19.629 13.1552 18.37 15.0278 17.5117 17.0912C16.6535 19.1545 16.2131 21.3676 16.2159 23.6023Z"], [1, "mx-2"], ["src", "assets/images/goonee-white-logo.svg", "alt", "Goonee"], ["href", "#", 1, "btn", "btn-primary", "px-lg-5", "px-2", "py-md-3", "py-2", "d-flex", "align-items-center"], ["viewBox", "0 0 21 47", "xmlns", "http://www.w3.org/2000/svg", 1, "ms-3", "d-none", "d-sm-block"], ["d", "M14.8155 23.6021C14.8201 27.0696 14.1399 30.5039 12.8139 33.7079C11.4879 36.9119 9.54227 39.8225 7.08862 42.2726L11.203 46.387C17.2429 40.3384 20.634 32.1391 20.6311 23.5912C20.6282 15.0433 17.2315 6.84633 11.1876 0.801758L7.07195 4.91737C9.53056 7.36767 11.4804 10.2799 12.8094 13.4866C14.1383 16.6932 14.8201 20.131 14.8155 23.6021Z"], ["d", "M5.40626 23.6023C5.40916 25.8333 4.97019 28.0428 4.11468 30.1033C3.25917 32.1638 2.00407 34.0345 0.421804 35.6074L4.52848 39.714C6.65067 37.5998 8.33427 35.087 9.48257 32.3202C10.6309 29.5534 11.2213 26.587 11.2198 23.5914C11.2184 20.5957 10.6252 17.6299 9.47421 14.8641C8.32326 12.0984 6.63725 9.58734 4.51304 7.4751L0.406364 11.5818C1.99322 13.1552 3.25221 15.0278 4.11045 17.0912C4.96869 19.1545 5.40912 21.3676 5.40626 23.6023Z"], [1, "row", "IP-row-two", "gx-lg-4", "gx-3", "pt-md-5", "pt-4", "pb-4"], ["src", "assets/images/goonee-cat7.png", "alt", "Outdoors"], ["src", "assets/images/goonee-cat8.png", "alt", "Remedial"], ["src", "assets/images/goonee-cat9.png", "alt", "Technology"], ["src", "assets/images/goonee-cat10.png", "alt", "Languages"], ["src", "assets/images/goonee-cat11.png", "alt", "Academics"], ["src", "assets/images/goonee-cat12.png", "alt", "Marketing"], [1, "text-center"], ["routerLink", "/category", 1, "explore-btn", "fw-bold", "text-white", "btn", "px-5", "py-2"], [1, "py-5", "cources-carousel"], [1, "row", "gx-md-5", "gx-3", "align-items-center", "px-3", "px-sm-0"], [1, "col-md-6", "px-0", "px-sm-5"], [1, "goonee-cource-wrap"], [1, "goonee-cource-slider", 3, "config"], ["slickModal", "slick-carousel"], ["href", "#", "class", "slide-item", "ngxSlickItem", "", 4, "ngFor", "ngForOf"], [1, "col-md-6", "text-center", "text-md-start"], [1, "fw-bold"], ["href", "#", 1, "fw-bold", "btn", "btn-primary", "px-4", "py-2"], [1, "goonee-questions", "py-5"], [1, "container", "px-md-5", "px-4"], [1, "row", "gx-3", "gx-lg-5", "pt-md-5"], [1, "col-md-6", "mt-3", "mb-3"], [1, "haq-card", "bg-white", "overflow-hidden"], ["src", "assets/images/goonee-haq1.jpg", "alt", "Frequently asked questions"], [1, "p-lg-5", "p-4"], ["src", "assets/images/goonee-haq2.png", "alt", "Why Goonee"], ["href", "#", "ngxSlickItem", "", 1, "slide-item"], ["alt", "Goonee slide", 3, "src"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Best Online Platform for Learning And Teaching.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna lacus, quam nisi sed arcu. Imperdiet cursus nulla at.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "a", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Start Learning");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Start Teaching");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](14, "img", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Webinars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "img", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "1 to 1 Class");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "img", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Courses");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Webinars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "img", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "1 to 1 Class");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](59, "img", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Courses");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "button", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](66, "button", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "button", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "h2", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Infinite Possibilities");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "img", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "The Arts");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](80, "img", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Music");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](85, "img", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Auto");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](90, "img", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "Business");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](95, "img", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Life Style");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "img", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "Fitness");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "a", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "svg", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](106, "path", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "path", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](108, " Start Learning");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "span", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](110, "img", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "a", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](112, "Start Teaching ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "svg", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](114, "path", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "path", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](119, "img", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, "Outdoors");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](124, "img", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "Remedial");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "img", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](130, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](131, "Technology");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](134, "img", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](135, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](136, "Languages");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](139, "img", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](141, "Academics");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](144, "img", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Marketing");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "div", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "a", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](149, "Explore More");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "div", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "div", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "div", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "ngx-slick-carousel", 63, 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](157, HomeComponent_a_157_Template, 2, 1, "a", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "div", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "h2", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](160, "Lorem Ipsum Dolar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](162, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Urna lacus, quam nisi sed arcu. Imperdiet cursus nulla at.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "a", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, "Start Learning");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "div", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "div", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "h2", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](168, "Have A Question?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "div", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "div", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](171, "div", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](172, "img", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "div", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](174, "h4", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](175, "Frequently asked questions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](177, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu tristique ac mauris platea. Urna sit justo, rhoncus scelerisque. Tellus erat.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "a", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, "Get Help");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](180, "div", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "div", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](182, "img", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "div", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "h4", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](185, "Why Goonee?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](187, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Eu tristique ac mauris platea. Urna sit justo, rhoncus scelerisque. Tellus erat.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "a", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](189, "Learn More");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](155);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("config", ctx.slidesConfig);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.slideArray);
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_3__["SlickCarouselComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_3__["SlickItemDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJob21lLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _components_layout_header_header_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/layout/header/header.component */ "gK8K");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _components_layout_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/layout/footer/footer.component */ "H3jT");




class AppComponent {
    constructor() {
        this.title = 'Goonee';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 3, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-header");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "app-footer");
    } }, directives: [_components_layout_header_header_component__WEBPACK_IMPORTED_MODULE_1__["HeaderComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"], _components_layout_footer_footer_component__WEBPACK_IMPORTED_MODULE_3__["FooterComponent"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "YZvJ":
/*!****************************************************************!*\
  !*** ./src/app/components/site/pages/about/about.component.ts ***!
  \****************************************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_loading_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/services/loading.service */ "gw8Y");


class AboutComponent {
    constructor(_loading) {
        this._loading = _loading;
    }
    ngOnInit() {
        this._loading.setLoading(true, 'Lazy Load');
    }
    ngAfterContentInit() {
        setTimeout(() => {
            this._loading.setLoading(false, 'Lazy Load');
        }, 500);
    }
}
AboutComponent.ɵfac = function AboutComponent_Factory(t) { return new (t || AboutComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shared_services_loading_service__WEBPACK_IMPORTED_MODULE_1__["LoadingService"])); };
AboutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AboutComponent, selectors: [["app-about"]], decls: 233, vars: 0, consts: [[1, "hero-banner"], [1, "hero-caption"], [1, "container"], [1, "col-lg-5"], ["aria-label", "breadcrumb"], [1, "breadcrumb"], [1, "breadcrumb-item"], ["href", "#"], ["width", "20", "height", "17", "viewBox", "0 0 20 17", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["fill-rule", "evenodd", "clip-rule", "evenodd", "d", "M19.8639 8.38897C19.6825 8.53657 19.3877 8.53657 19.2063 8.38897L14.9497 4.93544C14.9192 4.91573 14.893 4.89448 14.8688 4.86936L13.045 3.38989C13.0145 3.37018 12.9883 3.34893 12.964 3.32381L9.99976 0.918552L0.793681 8.38897C0.612255 8.53657 0.317496 8.53657 0.13607 8.38897C-0.0453566 8.24175 -0.0453566 8.00258 0.13607 7.85537L9.61453 0.164323C9.62929 0.146163 9.63977 0.125298 9.65929 0.108683C9.7531 0.0329514 9.87691 -0.00220978 9.99976 0.000108542C10.1226 -0.00220978 10.2464 0.0325651 10.3402 0.108683C10.3607 0.124912 10.3698 0.145777 10.3859 0.164323L12.8569 2.17006V1.15811C12.8569 0.944826 13.0697 0.771724 13.3331 0.771724H15.2378C15.3697 0.771724 15.4887 0.815 15.5745 0.884936C15.6611 0.954872 15.714 1.05147 15.714 1.15811V4.48839L19.8639 7.85537C20.0454 8.00258 20.0454 8.24175 19.8639 8.38897ZM14.7616 1.5445H13.8092V2.94284L14.7616 3.71561V1.5445ZM3.33317 7.7267C3.59603 7.7267 3.80936 7.8998 3.80936 8.11309V15.4545C3.80936 15.8814 4.23602 16.2272 4.76173 16.2272H7.61884V11.2042C7.61884 10.9905 7.83169 10.8178 8.09502 10.8178H11.9045C12.1678 10.8178 12.3807 10.9905 12.3807 11.2042V16.2272H15.2378C15.764 16.2272 16.1902 15.8814 16.1902 15.4545V8.11309C16.1902 7.8998 16.403 7.7267 16.6663 7.7267C16.9297 7.7267 17.1425 7.8998 17.1425 8.11309V15.4545C17.1425 16.308 16.2897 17 15.2378 17H4.76173C3.70984 17 2.85699 16.308 2.85699 15.4545V8.11309C2.85699 7.89941 3.07032 7.7267 3.33317 7.7267ZM11.4283 16.2268V11.5902H8.57121V16.2268H11.4283Z"], ["aria-current", "page", 1, "breadcrumb-item", "active"], [1, "d-flex", "flex-wrap", "herobtns", "justify-content-lg-start", "justify-content-center", "mt-4"], ["href", "#", 1, "btn", "btn-primary", "me-3"], ["href", "#", 1, "btn", "btn-secondary", "text-white"], [1, "col-lg-6", "offset-lg-6", "p-0"], ["src", "assets/images/goonee-about-banner.jpg", "alt", "goonee-about-banner", 1, "w-100", "d-none", "d-lg-block"], ["src", "assets/images/goonee-about-banner-mobile.jpg", "alt", "goonee-about-banner", 1, "w-100", "d-block", "d-lg-none"], [1, "py-5", "cources-carousel"], [1, "row", "gx-md-5", "gx-3", "align-items-center", "px-3", "px-sm-0"], [1, "col-md-5", "text-center"], ["src", "assets/images/goonee-statistics-glance.png", "alt", "goonee-statistics-glance", 1, "mw-100"], [1, "col-md-1"], [1, "col-md-6", "text-center", "text-md-start", "mt-3", "mb-3"], [1, "fw-bold", "mb-4"], [1, "fw-light"], [1, "py-5", "bg-dark", "text-white"], [1, "col-md-6", "text-center", "text-md-start", "order-md-0", "order-1", "mt-3", "mb-3"], ["href", "#", 1, "btn", "btn-primary", "hover-outline-white", "mt-2", "me-sm-3", "mt-3"], [1, "col-md-5", "text-center", "order-md-1", "order-0"], ["src", "assets/images/goonee-manage-sessions.png", "alt", "goonee-statistics-glance", 1, "mw-100"], ["src", "assets/images/goonee-built-trust.png", "alt", "goonee-statistics-glance", 1, "mw-100"], [1, "py-5", "bg-light"], ["href", "#", 1, "btn", "btn-primary", "mt-2", "me-sm-3", "mt-3"], ["src", "assets/images/goonee-learn-use-app.png", "alt", "goonee-statistics-glance", 1, "mw-100"], ["src", "assets/images/goonee-tutor-from-home.png", "alt", "goonee-statistics-glance", 1, "mw-100"], [1, "d-flex", "flex-wrap", "justify-content-lg-start", "justify-content-center", "mt-4", "px-5", "px-md-0"], ["href", "#", 1, "btn", "btn-primary", "hover-outline-white", "mt-2", "me-2", "me-sm-3"], ["href", "#", 1, "btn", "btn-secondary", "hover-outline-white", "mt-2", "text-white"], ["src", "assets/images/goonee-your-safety.png", "alt", "goonee-statistics-glance", 1, "mw-100"], [1, "py-5"], [1, "container", "px-lg-5"], [1, "fw-bold", "mb-5", "text-center"], [1, "about-get-start", "d-none", "d-md-block", "mt-4", "pb-5"], [1, "row"], [1, "col"], [1, "fw-bold", "text-primary"], [1, "fw-bold", "text-dark"], ["src", "assets/images/goonee-geting-stared.svg", "alt", "goonee-getting-started", 1, "w-100"], ["id", "carouselGooneeTwo", "data-bs-ride", "carousel", 1, "aboutCarousel", "carousel", "slide", "d-block", "d-md-none"], [1, "carousel-inner"], [1, "carousel-item", "active", "about-icons-view"], [1, "about-icon-block"], ["src", "assets/images/goonee-about-icon1.svg", "alt", "Open your Goonee App"], [1, "p-4"], [1, "carousel-item", "about-icons-view"], ["src", "assets/images/goonee-about-icon2.svg", "alt", "Build up your profile"], ["src", "assets/images/goonee-about-icon3.svg", "alt", "Invite your friends"], ["src", "assets/images/goonee-about-icon4.svg", "alt", "Share your tutor username"], ["src", "assets/images/goonee-about-icon5.svg", "alt", "Time to Tutor"], [1, "carousel-indicators"], ["type", "button", "data-bs-target", "#carouselGooneeTwo", "data-bs-slide-to", "0", "aria-current", "true", "aria-label", "Slide 1", 1, "active"], ["type", "button", "data-bs-target", "#carouselGooneeTwo", "data-bs-slide-to", "1", "aria-label", "Slide 2"], ["type", "button", "data-bs-target", "#carouselGooneeTwo", "data-bs-slide-to", "2", "aria-label", "Slide 3"], ["type", "button", "data-bs-target", "#carouselGooneeTwo", "data-bs-slide-to", "3", "aria-label", "Slide 4"], ["type", "button", "data-bs-target", "#carouselGooneeTwo", "data-bs-slide-to", "4", "aria-label", "Slide 5"], [1, "container", "py-0", "py-md-5"], ["src", "assets/images/goonee-become-tutor.png", "alt", "goonee-statistics-glance", 1, "mw-100"], ["href", "#", 1, "btn", "btn-primary", "mt-2", "me-sm-3"], ["href", "#", 1, "btn", "btn-secondary", "mt-2", "text-white"]], template: function AboutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "nav", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "ol", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "li", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "svg", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "path", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "li", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "About Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "About Goonee");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolorum impedit autem labore quam! Doloribus aperiam architecto harum vel in doloremque eaque labore, dolorem nihil odio nam mollitia vitae. Odio, eaque?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Start Learning");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Start Teaching");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "img", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "h1", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Track your statistics with a glance");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam gravida diam non ante ullamcorper integer lectus montes, integer. Diam aliquet sed varius varius vitae tortor. Porta aliquam.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "h1", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Schedule & manage your sessions with ease");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam gravida diam non ante ullamcorper integer lectus montes, integer. Diam aliquet sed varius varius vitae tortor. Porta aliquam.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "a", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Get Started");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "img", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "img", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](53, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "h1", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Built for trust");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam gravida diam non ante ullamcorper integer lectus montes, integer. Diam aliquet sed varius varius vitae tortor. Porta aliquam.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "h1", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "Learn how to use the app");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam gravida diam non ante ullamcorper integer lectus montes, integer. Diam aliquet sed varius varius vitae tortor. Porta aliquam.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "a", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68, "Get Started");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](71, "img", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "img", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "h1", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Tutor from home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam gravida diam non ante ullamcorper integer lectus montes, integer. Diam aliquet sed varius varius vitae tortor. Porta aliquam.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "h1", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](88, "Your safety is our safety");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam gravida diam non ante ullamcorper integer lectus montes, integer. Diam aliquet sed varius varius vitae tortor. Porta aliquam.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam gravida diam non ante ullamcorper integer lectus.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "a", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](95, "Start Learning");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "a", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Start Teaching");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](98, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "img", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "h1", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Getting Started");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](107, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, "02");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "h6", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](112, "Build up your profile");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus sit cursus a, volutpat. Id volutpat lacus non.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "04");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "h6", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "Share your tutor username");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus sit cursus a, volutpat. Id volutpat lacus non.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](123, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](124, "img", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128, "01");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "h6", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, "Open your Goonee App");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](132, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus sit cursus a, volutpat. Id volutpat lacus non.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](133, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](135, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](136, "03");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "h6", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](138, "Invite your friends, family and current clientele");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus sit cursus a, volutpat. Id volutpat lacus non.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](141, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](144, "05");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "h6", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Time to Tutor");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](148, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus sit cursus a, volutpat. Id volutpat lacus non.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "div", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](154, "img", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](155, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](157, "01");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "h6", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](160, "Open your Goonee App");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](162, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus sit cursus a, volutpat. Id volutpat lacus non.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "div", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "div", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](166, "img", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](167, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](168, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](169, "02");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](171, "h6", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](172, "Build up your profile");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](174, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus sit cursus a, volutpat. Id volutpat lacus non.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "div", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "div", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](178, "img", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](179, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](180, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](181, "03");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "h6", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](184, "Invite your friends, family and current clientele");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](186, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus sit cursus a, volutpat. Id volutpat lacus non.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](187, "div", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "div", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](189, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](190, "img", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](191, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](192, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](193, "04");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](194, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](195, "h6", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](196, "Share your tutor username");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](197, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](198, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus sit cursus a, volutpat. Id volutpat lacus non.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](199, "div", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](200, "div", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](201, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](202, "img", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](203, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](204, "h1", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](205, "05");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](206, "div", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](207, "h6", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](208, "Time to Tutor");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](209, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](210, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus sit cursus a, volutpat. Id volutpat lacus non.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](211, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](212, "button", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](213, "button", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](214, "button", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](215, "button", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](216, "button", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](217, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](218, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](219, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](220, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](221, "img", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](222, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](223, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](224, "h1", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](225, "Become a Goonee Tutor");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](226, "p", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](227, "Download the Goonee iOS Platform in the Apple App Store today for free. Goonee will be launching on Android this Summer.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](228, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](229, "a", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](230, "Download For iOS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](231, "a", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](232, "Download For Androind");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhYm91dC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _components_layout_header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/layout/header/header.component */ "gK8K");
/* harmony import */ var _components_layout_footer_footer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/layout/footer/footer.component */ "H3jT");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _components_site_userauthentication_userauthentication_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/site/userauthentication/userauthentication.module */ "7hes");
/* harmony import */ var _components_site_pages_home_home_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/site/pages/home/home.component */ "Qfl8");
/* harmony import */ var _components_site_pages_about_about_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/site/pages/about/about.component */ "YZvJ");
/* harmony import */ var _shared_services_auth_interceptor__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./shared/services/auth.interceptor */ "yZLc");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! angularx-social-login */ "ahC7");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../environments/environment */ "AytR");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _components_site_pages_category_category_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/site/pages/category/category.component */ "PfYf");
/* harmony import */ var _components_site_pages_learn_learn_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/site/pages/learn/learn.component */ "sn8Y");
/* harmony import */ var _components_site_pages_be_a_tutor_be_a_tutor_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/site/pages/be-a-tutor/be-a-tutor.component */ "ggxV");
/* harmony import */ var _components_site_pages_main_category_detail_main_category_detail_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/site/pages/main-category-detail/main-category-detail.component */ "3oJF");
/* harmony import */ var _angular_service_worker__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/service-worker */ "Jho9");
/* harmony import */ var _shared_pipes_StripHtmltags__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./shared/pipes/StripHtmltags */ "Bh+D");
/* harmony import */ var ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-slick-carousel */ "eSVu");
/* harmony import */ var _components_site_goone_slider_goone_slider_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/site/goone-slider/goone-slider.component */ "lmtY");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/core */ "fXoL");




























class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_23__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_23__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [{
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"],
            useClass: _shared_services_auth_interceptor__WEBPACK_IMPORTED_MODULE_10__["AuthInterceptor"],
            multi: true
        }, {
            provide: 'SocialAuthServiceConfig',
            useValue: {
                autoLogin: false,
                providers: [
                    {
                        id: angularx_social_login__WEBPACK_IMPORTED_MODULE_12__["FacebookLoginProvider"].PROVIDER_ID,
                        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_12__["FacebookLoginProvider"](_environments_environment__WEBPACK_IMPORTED_MODULE_13__["environment"].fbAppId),
                    },
                ],
            },
        }, {
            provide: _angular_common__WEBPACK_IMPORTED_MODULE_1__["LocationStrategy"],
            useClass: _angular_common__WEBPACK_IMPORTED_MODULE_1__["HashLocationStrategy"]
        }], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
            _components_site_userauthentication_userauthentication_module__WEBPACK_IMPORTED_MODULE_7__["UserauthenticationModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"],
            ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_21__["SlickCarouselModule"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_14__["ToastrModule"].forRoot({
                timeOut: 3000,
                progressBar: true,
                progressAnimation: 'increasing',
                positionClass: "toast-top-right",
                closeButton: true,
                preventDuplicates: true,
            }),
            angularx_social_login__WEBPACK_IMPORTED_MODULE_12__["SocialLoginModule"],
            _angular_service_worker__WEBPACK_IMPORTED_MODULE_19__["ServiceWorkerModule"].register('ngsw-worker.js', {
                enabled: _environments_environment__WEBPACK_IMPORTED_MODULE_13__["environment"].production,
                // Register the ServiceWorker as soon as the app is stable
                // or after 30 seconds (whichever comes first).
                registrationStrategy: 'registerWhenStable:30000'
            })
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_23__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
        _components_layout_header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"],
        _components_layout_footer_footer_component__WEBPACK_IMPORTED_MODULE_5__["FooterComponent"],
        _components_site_pages_home_home_component__WEBPACK_IMPORTED_MODULE_8__["HomeComponent"],
        _components_site_pages_about_about_component__WEBPACK_IMPORTED_MODULE_9__["AboutComponent"],
        _components_site_pages_category_category_component__WEBPACK_IMPORTED_MODULE_15__["CategoryComponent"],
        _components_site_pages_learn_learn_component__WEBPACK_IMPORTED_MODULE_16__["LearnComponent"],
        _components_site_pages_be_a_tutor_be_a_tutor_component__WEBPACK_IMPORTED_MODULE_17__["BeATutorComponent"],
        _components_site_pages_main_category_detail_main_category_detail_component__WEBPACK_IMPORTED_MODULE_18__["MainCategoryDetailComponent"],
        _shared_pipes_StripHtmltags__WEBPACK_IMPORTED_MODULE_20__["StripHtmltags"],
        _components_site_goone_slider_goone_slider_component__WEBPACK_IMPORTED_MODULE_22__["GooneSliderComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
        _components_site_userauthentication_userauthentication_module__WEBPACK_IMPORTED_MODULE_7__["UserauthenticationModule"],
        _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"],
        ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_21__["SlickCarouselModule"], ngx_toastr__WEBPACK_IMPORTED_MODULE_14__["ToastrModule"], angularx_social_login__WEBPACK_IMPORTED_MODULE_12__["SocialLoginModule"], _angular_service_worker__WEBPACK_IMPORTED_MODULE_19__["ServiceWorkerModule"]] }); })();


/***/ }),

/***/ "bVZ7":
/*!*******************************************************!*\
  !*** ./src/app/shared/services/auth-state.service.ts ***!
  \*******************************************************/
/*! exports provided: AuthStateService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthStateService", function() { return AuthStateService; });
/* harmony import */ var rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/internal/BehaviorSubject */ "7RJT");
/* harmony import */ var rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./token.service */ "hrsj");



class AuthStateService {
    constructor(token) {
        this.token = token;
        this.userState = new rxjs_internal_BehaviorSubject__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"](this.token.isLoggedIn());
        this.userAuthState = this.userState.asObservable();
    }
    setAuthState(value) {
        this.userState.next(value);
    }
}
AuthStateService.ɵfac = function AuthStateService_Factory(t) { return new (t || AuthStateService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"])); };
AuthStateService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: AuthStateService, factory: AuthStateService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "fH6q":
/*!*****************************************************!*\
  !*** ./src/app/shared/services/category.service.ts ***!
  \*****************************************************/
/*! exports provided: CategoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryService", function() { return CategoryService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../environments/environment */ "AytR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");



class CategoryService {
    constructor(http) {
        this.http = http;
    }
    getCategories() {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiUrl + '/category/all');
    }
    getSubCategories(data) {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiUrl + '/category/' + data.cat_alias + '?pid=' + data.pid + '');
    }
}
CategoryService.ɵfac = function CategoryService_Factory(t) { return new (t || CategoryService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
CategoryService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: CategoryService, factory: CategoryService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "gK8K":
/*!**************************************************************!*\
  !*** ./src/app/components/layout/header/header.component.ts ***!
  \**************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_auth_state_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../shared/services/auth-state.service */ "bVZ7");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _shared_services_token_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../shared/services/token.service */ "hrsj");
/* harmony import */ var _shared_services_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/services/loading.service */ "gw8Y");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "ofXK");








function HeaderComponent_div_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "svg", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "path", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "path", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "path", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function HeaderComponent_a_60_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "a", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Log in");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
function HeaderComponent_a_61_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "a", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function HeaderComponent_a_61_Template_a_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4); const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"](); return ctx_r3.logOut(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Log Out");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
} }
class HeaderComponent {
    constructor(auth, router, token, _loading, toastr) {
        this.auth = auth;
        this.router = router;
        this.token = token;
        this._loading = _loading;
        this.toastr = toastr;
        this.loading = false;
    }
    ngOnInit() {
        this.listenToLoading();
        this.auth.userAuthState.subscribe(val => {
            this.isSignedIn = val;
        });
    }
    // Signout
    logOut() {
        this.auth.setAuthState(false);
        this.token.removeToken();
        this.toastr.success("Logged Out Successfully.");
        this.router.navigate(['login']);
    }
    listenToLoading() {
        this._loading.loadingSub
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["delay"])(0))
            .subscribe((loading) => {
            this.loading = loading;
        });
    }
}
HeaderComponent.ɵfac = function HeaderComponent_Factory(t) { return new (t || HeaderComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_shared_services_auth_state_service__WEBPACK_IMPORTED_MODULE_2__["AuthStateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_shared_services_token_service__WEBPACK_IMPORTED_MODULE_4__["TokenService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_shared_services_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"])); };
HeaderComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({ type: HeaderComponent, selectors: [["app-header"]], decls: 99, vars: 3, consts: [["class", "pageload d-flex align-items-center position-fixed justify-content-center h-100 w-100", 4, "ngIf"], [1, "main-header"], [1, "head-top", "text-white"], [1, "container"], [1, "d-flex", "flex-wrap", "align-self-center", "justify-content-between"], [1, "follow-us", "d-flex"], [1, "mb-0", "me-3"], ["href", "#"], [1, "fab", "fa-facebook-f"], [1, "fab", "fa-instagram"], [1, "fab", "fa-twitter"], [1, "fab", "fa-linkedin-in"], [1, "top-link"], ["routerLink", "/about-us"], [1, "navbar", "navbar-expand-md", "navbar-light"], ["routerLink", "/", 1, "navbar-brand"], ["src", "assets/images/goonee-logo.svg", "alt", "Goonee"], [1, "navbar-nav", "ms-auto", "d-sm-flex", "d-none"], [1, "nav-item"], ["aria-current", "page", "routerLink", "/learn", 1, "nav-link", "active"], ["routerLink", "/be-a-tutor", 1, "nav-link"], ["data-bs-toggle", "collapse", "href", "#sideNavbar", "aria-expanded", "false", "aria-controls", "sideNavbar", 1, "navToggle"], ["width", "35", "height", "24", "viewBox", "0 0 35 24", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["d", "M2.47382 13.6634C3.52218 13.6634 4.37203 12.8135 4.37203 11.7652C4.37203 10.7168 3.52218 9.86694 2.47382 9.86694C1.42547 9.86694 0.575615 10.7168 0.575615 11.7652C0.575615 12.8135 1.42547 13.6634 2.47382 13.6634Z", "fill", "black"], ["d", "M32.9591 9.86694H9.95277C8.96732 9.86694 8.16845 10.6658 8.16845 11.6513V11.879C8.16845 12.8645 8.96732 13.6634 9.95277 13.6634H32.9591C33.9445 13.6634 34.7434 12.8645 34.7434 11.879V11.6513C34.7434 10.6658 33.9445 9.86694 32.9591 9.86694Z", "fill", "black"], ["d", "M32.9591 19.358H2.35993C1.37448 19.358 0.575615 20.1569 0.575615 21.1424V21.3701C0.575615 22.3556 1.37448 23.1545 2.35993 23.1545H32.9591C33.9445 23.1545 34.7434 22.3556 34.7434 21.3701V21.1424C34.7434 20.1569 33.9445 19.358 32.9591 19.358Z", "fill", "black"], ["d", "M32.9592 0.375977H2.36002C1.37457 0.375977 0.575706 1.17484 0.575706 2.16029V2.38808C0.575706 3.37353 1.37457 4.1724 2.36002 4.1724H32.9592C33.9446 4.1724 34.7435 3.37353 34.7435 2.38808V2.16029C34.7435 1.17484 33.9446 0.375977 32.9592 0.375977Z", "fill", "black"], ["width", "31", "height", "30", "viewBox", "0 0 31 30", "fill", "none", "xmlns", "http://www.w3.org/2000/svg", 1, "close"], ["d", "M15.6551 29.9662C13.703 29.9699 11.7695 29.5877 9.96562 28.8417C8.16174 28.0957 6.52306 27.0005 5.14375 25.6192C-0.653062 19.8224 -0.653062 10.391 5.14375 4.59416C6.52101 3.20918 8.15929 2.11111 9.96377 1.36351C11.7682 0.615919 13.7031 0.233646 15.6563 0.238821C19.6275 0.238821 23.3608 1.78496 26.1676 4.59416C28.9756 7.40218 30.523 11.1355 30.523 15.1067C30.523 19.0779 28.9768 22.8112 26.1676 25.6192C24.7881 27.0005 23.1492 28.0956 21.3451 28.8416C19.541 29.5876 17.6073 29.9698 15.6551 29.9662ZM15.6563 2.61749C14.0155 2.61283 12.3901 2.93379 10.8743 3.56177C9.35843 4.18975 7.98226 5.11227 6.82547 6.27588C4.46702 8.63433 3.16827 11.7706 3.16827 15.1067C3.16827 18.4428 4.46702 21.5779 6.82547 23.9375C11.6946 28.8066 19.618 28.8078 24.4859 23.9375C26.8444 21.5791 28.1443 18.4428 28.1443 15.1067C28.1443 11.7706 26.8455 8.63552 24.4859 6.27588C23.3291 5.11262 21.9531 4.19033 20.4375 3.56237C18.9219 2.93441 17.2968 2.61325 15.6563 2.61749Z", "fill", "black"], ["d", "M10.6096 21.3412C10.3741 21.3417 10.1439 21.2723 9.94795 21.1417C9.75204 21.0111 9.59935 20.8252 9.50925 20.6077C9.41916 20.3902 9.39572 20.1508 9.44192 19.9199C9.48812 19.6891 9.60187 19.4771 9.76873 19.311L19.8602 9.21951C19.9707 9.10909 20.1018 9.0215 20.246 8.96173C20.3903 8.90197 20.5449 8.87122 20.7011 8.87122C20.8573 8.87122 21.0119 8.90197 21.1562 8.96173C21.3004 9.0215 21.4315 9.10909 21.542 9.21951C21.6524 9.32994 21.74 9.46103 21.7997 9.6053C21.8595 9.74958 21.8903 9.90421 21.8903 10.0604C21.8903 10.2165 21.8595 10.3712 21.7997 10.5154C21.74 10.6597 21.6524 10.7908 21.542 10.9012L11.4505 20.9927C11.3403 21.1036 11.2093 21.1914 11.0649 21.2512C10.9206 21.311 10.7658 21.3416 10.6096 21.3412Z", "fill", "black"], ["d", "M20.7012 21.3412C20.545 21.3414 20.3903 21.3107 20.246 21.2509C20.1017 21.1911 19.9706 21.1033 19.8603 20.9927L9.76883 10.9012C9.6584 10.7908 9.57081 10.6597 9.51105 10.5154C9.45129 10.3712 9.42053 10.2165 9.42053 10.0604C9.42053 9.90421 9.45129 9.74958 9.51105 9.6053C9.57081 9.46103 9.6584 9.32994 9.76883 9.21951C9.87925 9.10909 10.0103 9.0215 10.1546 8.96173C10.2989 8.90197 10.4535 8.87122 10.6097 8.87122C10.7658 8.87122 10.9205 8.90197 11.0648 8.96173C11.209 9.0215 11.3401 9.10909 11.4505 9.21951L21.542 19.311C21.7089 19.4771 21.8227 19.6891 21.8689 19.9199C21.9151 20.1508 21.8916 20.3902 21.8015 20.6077C21.7114 20.8252 21.5587 21.0111 21.3628 21.1417C21.1669 21.2723 20.9366 21.3417 20.7012 21.3412Z", "fill", "black"], ["id", "sideNavbar", 1, "side-navbar", "collapse"], [1, "p-3", "pt-0", "ps-md-4", "pe-md-4", "side-nav-links"], [1, "mm-mob-links", "mb-0", "d-sm-none", "d-block"], ["routerLink", "/learn"], ["routerLink", "/be-a-tutor"], ["routerLink", "/login", 4, "ngIf"], ["type", "button", 3, "click", 4, "ngIf"], [1, "p-3", "pt-0", "ps-md-4", "pe-md-4", "side-app-icons"], ["src", "assets/images/goonee-ios-store-icon.svg", "alt", "goonee-ios-store", 1, "mw-100"], ["src", "assets/images/goonee-android-store-icon.svg", "alt", "goonee-android-store", 1, "mw-100"], [1, "follow-us", "bg-primary", "d-flex", "p-3", "ps-md-4", "pe-md-4"], [1, "mb-0", "me-2"], [1, "pageload", "d-flex", "align-items-center", "position-fixed", "justify-content-center", "h-100", "w-100"], ["viewBox", "0 0 40 47", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["d", "M6.59414 23.5076C6.58955 26.9751 7.26976 30.4094 8.59575 33.6134C9.92174 36.8174 11.8674 39.728 14.321 42.1781L10.2066 46.2925C4.1668 40.2439 0.775671 32.0446 0.778566 23.4967C0.781462 14.9488 4.17814 6.75185 10.2221 0.707275L14.3377 4.82289C11.8791 7.27319 9.92923 10.1854 8.60029 13.3921C7.27135 16.5987 6.58956 20.0365 6.59414 23.5076Z", 1, "aniPath-f"], ["d", "M16.0033 23.5078C16.0004 25.7388 16.4393 27.9483 17.2948 30.0088C18.1504 32.0693 19.4055 33.94 20.9877 35.5129L16.8811 39.6195C14.7589 37.5053 13.0753 34.9926 11.927 32.2257C10.7787 29.4589 10.1883 26.4925 10.1897 23.4969C10.1911 20.5012 10.7844 17.5354 11.9353 14.7697C13.0863 12.004 14.7723 9.49286 16.8965 7.38062L21.0032 11.4873C19.4163 13.0608 18.1573 14.9333 17.2991 16.9967C16.4408 19.06 16.0004 21.2731 16.0033 23.5078Z", 1, "aniPath-s"], ["d", "M29.8524 33.6831C35.1938 33.6831 39.2215 30.341 39.2215 23.0563H29.3088V26.2839H34.8789C34.8789 28.5692 32.3366 30.1116 29.7658 30.1116C26.1667 30.1116 23.5673 27.0266 23.5673 23.5132C23.5673 19.9999 26.2238 17.0004 29.6514 17.0004C30.4789 17.0035 31.2969 17.1777 32.0539 17.5119C32.8109 17.8461 33.4907 18.3333 34.0505 18.9427L36.9358 16.4575C35.2219 14.5438 32.6224 13.3156 29.6518 13.3156C23.996 13.3156 19.5972 17.9143 19.5972 23.513C19.5978 29.0843 23.9111 33.6831 29.8524 33.6831Z"], ["routerLink", "/login"], ["type", "button", 3, "click"]], template: function HeaderComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, HeaderComponent_div_0_Template, 5, 0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "header", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "h6", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, "Follow us on:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](10, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "i", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](14, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](16, "i", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18, "Lorem ipsum, dolor sit amet consectetur adipisicing elit.");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, " About Goonee");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, "Contact Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "nav", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](27, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "ul", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](29, "li", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "a", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](31, "Learn");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "li", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](33, "a", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](34, "Be A Tutor");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "a", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "svg", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](37, "path", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](38, "path", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](39, "path", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](40, "path", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](41, "svg", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](42, "path", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](43, "path", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](44, "path", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](45, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](47, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "a", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](49, "Learn");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "a", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](51, "Be A Tutor");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](53, "About us");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](55, "Contact us");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](56, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](57, "FAQ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](58, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](59, "Why Goonee?");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](60, HeaderComponent_a_60_Template, 2, 0, "a", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](61, HeaderComponent_a_61_Template, 2, 0, "a", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](62, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](63, "Community");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](65, "Blog");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](66, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](67, "Community Guidelines");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](68, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](69, "User Safety");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](71, "Non - Descrimination Policy");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](72, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](73, "Legal");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](74, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](75, "Privacy Policy");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](76, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](77, "Service Terms");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](78, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](79, "Payment Terms");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](80, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](81, "Independent Tutor Agreement");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](82, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](83, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](84, "img", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](85, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](86, "img", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](87, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](88, "h6", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](89, "Follow Us");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](90, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](91, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](92, "i", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](93, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](94, "i", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](95, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](96, "i", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](97, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](98, "i", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.loading);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](60);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx.isSignedIn);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.isSignedIn);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterLinkWithHref"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJoZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "ggxV":
/*!**************************************************************************!*\
  !*** ./src/app/components/site/pages/be-a-tutor/be-a-tutor.component.ts ***!
  \**************************************************************************/
/*! exports provided: BeATutorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BeATutorComponent", function() { return BeATutorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_loading_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/services/loading.service */ "gw8Y");


class BeATutorComponent {
    constructor(_loading) {
        this._loading = _loading;
    }
    ngOnInit() {
        this._loading.setLoading(true, 'Lazy Load');
    }
    ngAfterContentInit() {
        setTimeout(() => {
            this._loading.setLoading(false, 'Lazy Load');
        }, 500);
    }
}
BeATutorComponent.ɵfac = function BeATutorComponent_Factory(t) { return new (t || BeATutorComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shared_services_loading_service__WEBPACK_IMPORTED_MODULE_1__["LoadingService"])); };
BeATutorComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BeATutorComponent, selectors: [["app-be-a-tutor"]], decls: 204, vars: 0, consts: [[1, "hero-banner"], [1, "hero-caption"], [1, "container"], [1, "col-lg-5"], ["aria-label", "breadcrumb"], [1, "breadcrumb"], [1, "breadcrumb-item"], ["href", "index.html"], ["width", "20", "height", "17", "viewBox", "0 0 20 17", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["fill-rule", "evenodd", "clip-rule", "evenodd", "d", "M19.8639 8.38897C19.6825 8.53657 19.3877 8.53657 19.2063 8.38897L14.9497 4.93544C14.9192 4.91573 14.893 4.89448 14.8688 4.86936L13.045 3.38989C13.0145 3.37018 12.9883 3.34893 12.964 3.32381L9.99976 0.918552L0.793681 8.38897C0.612255 8.53657 0.317496 8.53657 0.13607 8.38897C-0.0453566 8.24175 -0.0453566 8.00258 0.13607 7.85537L9.61453 0.164323C9.62929 0.146163 9.63977 0.125298 9.65929 0.108683C9.7531 0.0329514 9.87691 -0.00220978 9.99976 0.000108542C10.1226 -0.00220978 10.2464 0.0325651 10.3402 0.108683C10.3607 0.124912 10.3698 0.145777 10.3859 0.164323L12.8569 2.17006V1.15811C12.8569 0.944826 13.0697 0.771724 13.3331 0.771724H15.2378C15.3697 0.771724 15.4887 0.815 15.5745 0.884936C15.6611 0.954872 15.714 1.05147 15.714 1.15811V4.48839L19.8639 7.85537C20.0454 8.00258 20.0454 8.24175 19.8639 8.38897ZM14.7616 1.5445H13.8092V2.94284L14.7616 3.71561V1.5445ZM3.33317 7.7267C3.59603 7.7267 3.80936 7.8998 3.80936 8.11309V15.4545C3.80936 15.8814 4.23602 16.2272 4.76173 16.2272H7.61884V11.2042C7.61884 10.9905 7.83169 10.8178 8.09502 10.8178H11.9045C12.1678 10.8178 12.3807 10.9905 12.3807 11.2042V16.2272H15.2378C15.764 16.2272 16.1902 15.8814 16.1902 15.4545V8.11309C16.1902 7.8998 16.403 7.7267 16.6663 7.7267C16.9297 7.7267 17.1425 7.8998 17.1425 8.11309V15.4545C17.1425 16.308 16.2897 17 15.2378 17H4.76173C3.70984 17 2.85699 16.308 2.85699 15.4545V8.11309C2.85699 7.89941 3.07032 7.7267 3.33317 7.7267ZM11.4283 16.2268V11.5902H8.57121V16.2268H11.4283Z"], ["aria-current", "page", 1, "breadcrumb-item", "active"], [1, "d-flex", "flex-wrap", "herobtns", "justify-content-lg-start", "justify-content-center", "mt-4"], ["href", "#", 1, "btn", "btn-primary", "me-3"], ["href", "#", 1, "btn", "btn-secondary", "text-white"], [1, "col-lg-6", "offset-lg-6", "p-0"], ["src", "assets/images/goonee-betuter-banner.jpg", "alt", "goonee be a tutor", 1, "w-100", "d-none", "d-lg-block"], ["src", "assets/images/goonee-betuter-banner-mobile.jpg", "alt", "goonee be a tutor", 1, "w-100", "d-block", "d-lg-none"], [1, "goonee-cards", "py-5"], [1, "container", "my-md-4"], [1, "fw-bold", "mb-5", "text-md-start", "text-center"], [1, "d-none", "d-sm-block"], [1, "row", "gx-md-5", "gx-3"], [1, "col"], ["href", "#", 1, "card-view", "px-4", "py-5", "d-block"], ["src", "assets/images/goonee-bat-icon-1.png", "alt", "goonee control"], ["src", "assets/images/goonee-bat-icon-2.png", "alt", "goonee Simplicity"], ["src", "assets/images/goonee-bat-icon-3.png", "alt", "goonee Earn money teaching"], ["id", "carouselGooneetwo", "data-bs-ride", "carousel", 1, "carousel", "slide", "d-block", "d-sm-none"], [1, "carousel-inner"], [1, "carousel-item", "active", "p-3"], [1, "carousel-item", "p-3"], [1, "carousel-indicators"], ["type", "button", "data-bs-target", "#carouselGooneetwo", "data-bs-slide-to", "0", "aria-current", "true", "aria-label", "Slide 1", 1, "active"], ["type", "button", "data-bs-target", "#carouselGooneetwo", "data-bs-slide-to", "1", "aria-label", "Slide 2"], ["type", "button", "data-bs-target", "#carouselGooneetwo", "data-bs-slide-to", "2", "aria-label", "Slide 3"], [1, "py-5", "bg-dark", "text-white", "text-center"], [1, "mb-3", "mb-md-5"], [1, "fw-bold", "mb-3"], [1, "fw-light"], [1, "row", "gx-md-5", "gx-3", "px-3", "px-sm-0"], [1, "col-md-4", "my-3", "px-lg-5"], ["src", "assets/images/goonee-build_profile.png", "alt", "goonee Create", 1, "mw-100", "px-md-5", "px-3"], [1, "fw-bold", "mt-md-5", "mt-3", "mb-3"], ["src", "assets/images/goonee-tutor-connect.png", "alt", "goonee Connect", 1, "mw-100", "px-md-5", "px-3"], ["src", "assets/images/goonee-get-paid.png", "alt", "goonee Tutor", 1, "mw-100", "px-md-5", "px-3"], ["id", "carouselGooneeHIW", "data-bs-ride", "carousel", 1, "carousel", "slide", "d-block", "d-sm-none"], [1, "my-2"], ["src", "assets/images/goonee-build_profile.png", "alt", "goonee Create", 1, "mw-100", "px-5"], [1, "fw-light", "mb-1"], ["src", "assets/images/goonee-tutor-connect.png", "alt", "goonee Connect", 1, "mw-100", "px-5"], ["src", "assets/images/goonee-get-paid.png", "alt", "goonee Tutor", 1, "mw-100", "px-5"], ["type", "button", "data-bs-target", "#carouselGooneeHIW", "data-bs-slide-to", "0", "aria-current", "true", "aria-label", "Slide 1", 1, "active"], ["type", "button", "data-bs-target", "#carouselGooneeHIW", "data-bs-slide-to", "1", "aria-label", "Slide 2"], ["type", "button", "data-bs-target", "#carouselGooneeHIW", "data-bs-slide-to", "2", "aria-label", "Slide 3"], [1, "py-5"], [1, "container", "py-md-4", "py-2"], [1, "fw-bold", "mb-md-5", "mb-4", "text-center"], [1, "col-lg-8", "offset-lg-2"], ["id", "accordionTH", 1, "accordion"], [1, "accordion-item"], ["id", "headingOne", 1, "accordion-header"], ["type", "button", "data-bs-toggle", "collapse", "data-bs-target", "#collapseOne", "aria-expanded", "true", "aria-controls", "collapseOne", 1, "accordion-button"], ["width", "35", "height", "35", "viewBox", "0 0 35 35", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["d", "M29.8919 5.33321C26.681 2.12234 22.412 0.354004 17.8711 0.354004C13.3302 0.354004 9.06118 2.1224 5.8503 5.33321C2.63943 8.54402 0.871094 12.8131 0.871094 17.354C0.871094 21.8949 2.63943 26.1639 5.8503 29.3748C9.06111 32.5857 13.3302 34.354 17.8711 34.354C22.412 34.354 26.681 32.5857 29.8919 29.3748C33.1028 26.1639 34.8711 21.8949 34.8711 17.354C34.8711 12.8131 33.1028 8.54402 29.8919 5.33321ZM17.8711 31.6072C10.0119 31.6072 3.61792 25.2132 3.61792 17.354C3.61792 9.49482 10.0119 3.10083 17.8711 3.10083C25.7303 3.10083 32.1243 9.49482 32.1243 17.354C32.1243 25.2132 25.7303 31.6072 17.8711 31.6072ZM23.5303 21.0708L19.2446 16.7851V7.97804C19.2446 7.21955 18.6297 6.60462 17.8712 6.60462C17.1127 6.60462 16.4977 7.21955 16.4977 7.97804V17.354C16.4977 17.7183 16.6424 18.0676 16.9 18.3252L21.5879 23.0131C21.8561 23.2814 22.2076 23.4154 22.5591 23.4154C22.9106 23.4154 23.2621 23.2814 23.5304 23.0131C24.0666 22.4767 24.0666 21.6071 23.5303 21.0708Z"], ["id", "collapseOne", "aria-labelledby", "headingOne", "data-bs-parent", "#accordionTH", 1, "accordion-collapse", "collapse", "show"], [1, "accordion-body"], ["id", "headingTwo", 1, "accordion-header"], ["type", "button", "data-bs-toggle", "collapse", "data-bs-target", "#collapseTwo", "aria-expanded", "false", "aria-controls", "collapseTwo", 1, "accordion-button", "collapsed"], ["width", "34", "height", "35", "viewBox", "0 0 34 35", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["d", "M21.766 9.82416C23.1903 10.8972 24.7418 12.3297 26.2011 14.0826C27.7357 15.9143 28.9651 17.8864 29.8597 19.9421C30.974 22.5124 31.5394 25.1982 31.5394 27.9208C31.5394 31.6661 28.4864 34.7144 24.7417 34.7144H8.99892C5.25418 34.7144 2.20117 31.6661 2.20117 27.9208C2.20117 25.1982 2.76653 22.5124 3.88092 19.9421C4.76945 17.9002 6.00156 15.9266 7.53913 14.083C8.96721 12.373 10.4816 10.9672 11.8902 9.89156L8.25803 4.41227C7.95259 3.94991 8.01086 3.33032 8.39611 2.9358C9.75664 1.54106 11.1669 0.88054 12.8132 0.88054C13.9747 0.88054 15.1421 1.17156 17.0821 1.87969C17.1646 1.91171 17.2478 1.92612 17.3557 1.92612C17.6887 1.92612 18.0057 1.82127 18.9176 1.44695C20.1689 0.920886 20.8571 0.714355 21.7387 0.714355C22.9118 0.714355 23.9659 1.18682 24.9395 2.1369C25.322 2.49922 25.4081 3.07259 25.1586 3.54102L21.766 9.82416ZM14.1251 8.99588H19.5394L22.616 3.28731C22.3073 3.11683 22.0317 3.05623 21.7318 3.05623C21.2425 3.05623 20.8221 3.1894 19.8092 3.61161C18.6715 4.08936 18.0864 4.26799 17.3488 4.26799C16.9756 4.26799 16.611 4.20536 16.2681 4.08087C14.6237 3.47191 13.6598 3.22241 12.8132 3.22241C12.0623 3.22241 11.4302 3.44213 10.7752 3.94362L14.1251 8.99588ZM9.30131 15.6322C6.14509 19.4384 4.54789 23.5706 4.54789 27.9208C4.54789 30.366 6.54365 32.3586 8.99892 32.3586H24.7417C27.1986 32.3586 29.1917 30.3647 29.1788 27.9208C29.1788 23.5596 27.5856 19.4349 24.4253 15.6321C22.8735 13.759 21.2092 12.2893 19.8717 11.3516H13.8572C12.4136 12.3869 10.8445 13.7696 9.30131 15.6322ZM19.4432 17.4505C20.088 17.4505 20.6166 17.9783 20.6166 18.6284C20.6166 19.2784 20.088 19.8062 19.4432 19.8062H16.0866C15.5801 19.8062 15.1725 20.2132 15.1725 20.7126C15.1725 21.212 15.5801 21.619 16.0866 21.619H17.9938C19.7897 21.619 21.2546 23.0816 21.2546 24.8811C21.2546 26.6046 19.9159 28.0202 18.217 28.1358V28.9803C18.217 29.6303 17.6884 30.1581 17.0437 30.1581C16.3989 30.1581 15.8703 29.6303 15.8703 28.9803V28.1431H14.5748C13.93 28.1431 13.4014 27.6153 13.4014 26.9653C13.4014 26.3152 13.93 25.7874 14.5748 25.7874H17.9938C18.5003 25.7874 18.9079 25.3804 18.9079 24.8811C18.9079 24.3817 18.5003 23.9747 17.9938 23.9747H16.0866C14.2907 23.9747 12.8258 22.5121 12.8258 20.7126C12.8258 18.9883 14.1708 17.5733 15.8634 17.4581V16.6411C15.8634 15.991 16.392 15.4632 17.0367 15.4632C17.6815 15.4632 18.2101 15.991 18.2101 16.6411V17.4505H19.4432Z"], ["id", "collapseTwo", "aria-labelledby", "headingTwo", "data-bs-parent", "#accordionTH", 1, "accordion-collapse", "collapse"], ["id", "headingThree", 1, "accordion-header"], ["type", "button", "data-bs-toggle", "collapse", "data-bs-target", "#collapseThree", "aria-expanded", "false", "aria-controls", "collapseThree", 1, "accordion-button", "collapsed"], ["d", "M29.7202 22.0376C29.8727 21.6818 30.2215 21.4509 30.6031 21.4494H30.731C33.0553 21.4494 34.9462 19.5587 34.9462 17.2347C34.9462 14.9108 33.0553 13.0201 30.731 13.0201H30.495C30.1674 13.0188 29.8674 12.8533 29.6904 12.5869C29.6708 12.4901 29.6409 12.3955 29.6008 12.3046C29.4404 11.9414 29.515 11.5235 29.7908 11.2383L29.8661 11.163C29.8683 11.161 29.8703 11.1589 29.8723 11.1569C30.668 10.3604 31.1059 9.30175 31.1052 8.17596C31.1046 7.0521 30.6669 5.99564 29.8729 5.20049C29.8708 5.19836 29.8687 5.1963 29.8667 5.19418C29.0706 4.3991 28.0128 3.96141 26.8877 3.96141C26.887 3.96141 26.8861 3.96141 26.8853 3.96141C25.7594 3.96201 24.7011 4.40102 23.906 5.19697L23.8273 5.27573C23.5421 5.55138 23.1241 5.62602 22.7608 5.46572C22.7562 5.46379 22.7517 5.4618 22.7472 5.45987C22.3914 5.3074 22.1604 4.9587 22.159 4.57713V4.44923C22.159 2.12534 20.268 0.234619 17.9437 0.234619C15.6195 0.234619 13.7285 2.12534 13.7285 4.44923V4.68531C13.7272 5.01282 13.5617 5.31278 13.2952 5.48969C13.1985 5.50928 13.1038 5.53929 13.013 5.57934C12.6498 5.73971 12.2317 5.66507 11.9465 5.38935L11.8651 5.30787C11.0689 4.51278 10.0113 4.0751 8.8862 4.0751C8.88541 4.0751 8.88448 4.0751 8.88375 4.0751C7.75781 4.0757 6.69948 4.51471 5.90157 5.31345C5.10584 6.10986 4.66797 7.16851 4.66863 8.2943C4.66923 9.42008 5.1083 10.4782 5.90436 11.2732L5.98306 11.3519C6.25881 11.6371 6.33346 12.055 6.17314 12.4183C6.16138 12.4448 6.15062 12.4716 6.14059 12.4987C6.00351 12.8713 5.66074 13.1194 5.26498 13.1337H5.15652C2.83238 13.1337 0.941406 15.0244 0.941406 17.3483C0.941406 19.6723 2.83238 21.563 5.15672 21.5628H5.39276C5.77996 21.5644 6.12857 21.7952 6.28106 22.151C6.28299 22.1556 6.28498 22.1601 6.28691 22.1645C6.4473 22.5278 6.37265 22.9457 6.09683 23.2309L6.01813 23.3096C6.01633 23.3114 6.01454 23.3133 6.01268 23.3152C5.21868 24.1112 4.7818 25.1688 4.7824 26.2933C4.783 27.4191 5.22207 28.4773 6.01866 29.2728C6.02058 29.2748 6.02257 29.2768 6.02457 29.2787C7.66928 30.9176 10.3406 30.9148 11.9815 29.2723L12.0603 29.1935C12.3454 28.9179 12.7634 28.8431 13.1267 29.0035C13.1531 29.0151 13.18 29.026 13.2071 29.036C13.5798 29.1731 13.8278 29.5158 13.8422 29.9116V30.0199C13.8422 32.3439 15.7332 34.2346 18.0574 34.2346C20.3817 34.2346 22.2726 32.3439 22.2727 30.0201V29.7841C22.2743 29.397 22.5051 29.0483 22.8609 28.8959C22.8655 28.8939 22.8699 28.892 22.8744 28.89C23.2378 28.7297 23.6557 28.8042 23.941 29.08L24.0224 29.1614C24.8185 29.9565 25.8762 30.3943 27.0013 30.3942C27.0021 30.3942 27.003 30.3942 27.0038 30.3942C28.1297 30.3935 29.188 29.9546 29.9859 29.1559C31.6285 27.5117 31.627 24.8378 29.9832 23.196L29.9045 23.1173C29.6288 22.8321 29.5541 22.4143 29.7144 22.0509C29.7164 22.0465 29.7184 22.0421 29.7202 22.0376ZM27.9445 25.0424C27.9481 25.046 27.9516 25.0496 27.9552 25.0532L28.0409 25.1389C28.6135 25.711 28.6141 26.6421 28.0398 27.2169C27.7627 27.4944 27.3941 27.6472 27.002 27.6474C27.0016 27.6474 27.0014 27.6474 27.0011 27.6474C26.6093 27.6474 26.2409 27.495 25.962 27.2164L25.8768 27.1312C25.8732 27.1276 25.8696 27.1241 25.8659 27.1205C24.7747 26.0534 23.1683 25.7608 21.7713 26.3743C20.4125 26.9599 19.5312 28.2932 19.5252 29.7786V30.0201C19.5252 30.8294 18.8667 31.4879 18.0572 31.4879C17.2478 31.4879 16.5893 30.8294 16.5893 30.0201V29.8922C16.5893 29.8815 16.5892 29.8709 16.5889 29.8601C16.5533 28.3385 15.6157 27.0138 14.1961 26.4734C12.807 25.879 11.2172 26.1755 10.1348 27.2341C10.1312 27.2376 10.1276 27.2412 10.1241 27.2447L10.0382 27.3305C9.46627 27.9031 8.53493 27.9037 7.96223 27.3317C7.96057 27.3301 7.95885 27.3284 7.95725 27.3268C7.68156 27.05 7.52974 26.6827 7.52947 26.292C7.52921 25.8999 7.68176 25.5313 7.95885 25.2539C7.96044 25.2524 7.96197 25.2507 7.96356 25.2492L8.04578 25.1669C8.04937 25.1633 8.05296 25.1597 8.05648 25.1561C9.1237 24.0652 9.41626 22.459 8.80292 21.0622C8.21727 19.7034 6.88372 18.8222 5.39801 18.8162H5.15645C4.34705 18.8162 3.68854 18.1577 3.68854 17.3484C3.68854 16.5391 4.34712 15.8807 5.15645 15.8806H5.28437C5.29506 15.8806 5.30582 15.8805 5.31651 15.8802C6.83842 15.8446 8.16327 14.9071 8.70376 13.4877C9.29804 12.0988 9.0017 10.5094 7.94291 9.42712C7.93939 9.42354 7.9358 9.41995 7.93221 9.41636L7.84647 9.33057C7.56906 9.05345 7.41617 8.68496 7.41597 8.2929C7.4157 7.9009 7.56826 7.53222 7.8476 7.25258C8.12475 6.9752 8.49329 6.82233 8.88541 6.82213C8.88574 6.82213 8.88594 6.82213 8.88627 6.82213C9.27812 6.82213 9.64652 6.9746 9.9254 7.25318L10.0107 7.33838C10.0142 7.34196 10.0179 7.34555 10.0214 7.34907C11.0454 8.35047 12.5231 8.66976 13.8559 8.19834C13.9818 8.1822 14.1052 8.14867 14.2225 8.0984C15.5854 7.51435 16.47 6.17899 16.4759 4.69095V4.44943C16.4759 3.64014 17.1344 2.98172 17.9438 2.98172C18.7532 2.98172 19.4118 3.6402 19.4118 4.4495V4.58284C19.4176 6.06284 20.2991 7.39621 21.658 7.98185C23.0551 8.59531 24.6614 8.3026 25.7525 7.23551C25.7561 7.23199 25.7597 7.22841 25.7633 7.22482L25.8491 7.13909C26.1262 6.86171 26.4948 6.70884 26.8869 6.70864C26.8872 6.70864 26.8875 6.70864 26.8877 6.70864C27.2783 6.70864 27.6458 6.86018 27.9227 7.1355C27.9242 7.1371 27.9258 7.13862 27.9273 7.14022C28.2048 7.41733 28.3577 7.78582 28.3579 8.17788C28.3582 8.56889 28.2065 8.93651 27.9309 9.21362C27.9296 9.21495 27.9282 9.21628 27.9268 9.21768L27.8416 9.30294C27.838 9.30646 27.8345 9.31005 27.8309 9.3137C26.8294 10.3375 26.51 11.8151 26.9815 13.1476C26.9977 13.2736 27.0313 13.397 27.0815 13.5143C27.6657 14.877 29.0012 15.7614 30.4894 15.7673H30.7309C31.5404 15.7673 32.1989 16.4258 32.1989 17.2351C32.1989 18.0443 31.5403 18.7028 30.7309 18.7028H30.5976C29.1174 18.7087 27.7839 19.59 27.1982 20.9487C26.5847 22.3452 26.8773 23.9515 27.9445 25.0424ZM17.9437 11.5995C14.8361 11.5995 12.3077 14.1274 12.3077 17.2347C12.3077 20.342 14.836 22.87 17.9437 22.87C21.0514 22.87 23.5797 20.342 23.5797 17.2347C23.5797 14.1274 21.0514 11.5995 17.9437 11.5995ZM17.9437 20.123C16.3509 20.123 15.055 18.8273 15.055 17.2347C15.055 15.642 16.3509 14.3463 17.9437 14.3463C19.5366 14.3463 20.8325 15.642 20.8325 17.2347C20.8325 18.8273 19.5366 20.123 17.9437 20.123Z"], ["id", "collapseThree", "aria-labelledby", "headingThree", "data-bs-parent", "#accordionTH", 1, "accordion-collapse", "collapse"], ["id", "collapseFour", 1, "accordion-header"], ["type", "button", "data-bs-toggle", "collapse", "data-bs-target", "#collapseFour", "aria-expanded", "false", "aria-controls", "collapseThree", 1, "accordion-button", "collapsed"], ["width", "32", "height", "31", "viewBox", "0 0 32 31", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["fill-rule", "evenodd", "clip-rule", "evenodd", "d", "M26.0291 27.7846H6.17849C5.11072 27.7846 4.32122 26.7304 4.72504 25.761C6.5944 21.2781 10.9865 18.71 16.103 18.71C21.2211 18.71 25.6132 21.2781 27.4826 25.761C27.8864 26.7304 27.0969 27.7846 26.0291 27.7846ZM9.9278 9.63546C9.9278 6.29904 12.6986 3.58575 16.103 3.58575C19.509 3.58575 22.2783 6.29904 22.2783 9.63546C22.2783 12.9719 19.509 15.6852 16.103 15.6852C12.6986 15.6852 9.9278 12.9719 9.9278 9.63546ZM31.1608 27.2341C30.0386 22.1538 26.5267 18.4045 21.9062 16.703C24.3549 14.7716 25.7826 11.6484 25.2578 8.22882C24.6498 4.26172 21.2801 1.0872 17.2147 0.6244C11.6005 -0.0153573 6.83942 4.26483 6.83942 9.63546C6.83942 12.494 8.19153 15.0408 10.3014 16.703C5.67787 18.4045 2.16903 22.1538 1.0453 27.2341C0.636944 29.0807 2.15694 30.8095 4.08528 30.8095H28.1208C30.0507 30.8095 31.5691 29.0807 31.1608 27.2341Z", "fill", "#6C60E1"], ["id", "collapseFour", "aria-labelledby", "collapseFour", "data-bs-parent", "#accordionTH", 1, "accordion-collapse", "collapse"], [1, "py-5", "bg-light"], [1, "container", "py-0", "py-md-5"], [1, "row", "gx-md-5", "gx-3", "align-items-center", "px-3", "px-sm-0"], [1, "col-md-5", "text-center"], ["src", "assets/images/goonee-become-tutor.png", "alt", "goonee-statistics-glance", 1, "mw-100"], [1, "col-md-1"], [1, "col-md-6", "text-center", "text-md-start", "mt-3", "mb-3"], [1, "fw-bold", "mb-4"], [1, "d-flex", "flex-wrap", "justify-content-lg-start", "justify-content-center", "mt-4", "px-3", "px-md-0"], ["href", "#", 1, "btn", "btn-primary", "mt-2", "me-sm-3"], ["href", "#", 1, "btn", "btn-secondary", "mt-2", "text-white"]], template: function BeATutorComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "nav", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "ol", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "li", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "svg", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "path", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "li", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Be A Tutor");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Be A Tutor");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolorum impedit autem labore quam! Doloribus aperiam architecto harum vel in doloremque eaque labore, dolorem nihil odio nam mollitia vitae. Odio, eaque?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Download For iOS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Download For Android");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h1", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Overview");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "You\u2019re in control");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "You\u2019re in charge of your unique tutoring business. You set your own subjects, rate, schedule, policies, and preferences.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "img", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Simplicity");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "QuickTutor makes it simple to earn money and reach new people all over the world who are looking for your help.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "img", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Earn money teaching");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Never worry about getting paid for your work. We have safe and secure automatic billing and instant payouts.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "You\u2019re in control");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "You\u2019re in charge of your unique tutoring business. You set your own subjects, rate, schedule, policies, and preferences.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "img", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "Simplicity");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "QuickTutor makes it simple to earn money and reach new people all over the world who are looking for your help.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "img", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Earn money teaching");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Never worry about getting paid for your work. We have safe and secure automatic billing and instant payouts.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "button", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "button", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "button", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "h1", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "How It works");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "p", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Available on the App Store & Playstore");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "img", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "h5", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "Create");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "p", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "Create your profile, decide your hourly rate, and add up to twenty different topics you\u2019d like to tutor!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](94, "img", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "h5", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "p", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "Manage payments and scheduling with your current clientele and find new clients to connect with and tutor!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "img", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "h5", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "Tutor");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "p", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Scheduling up to thirty days in advance. Start sessions early at any time. Do what you do best and get paid!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "img", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "h5", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Create");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Create your profile, decide your hourly rate, and add up to twenty different topics you\u2019d like to tutor!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "img", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "h5", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "Manage payments and scheduling with your current clientele and find new clients to connect with and tutor!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "div", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](123, "img", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "h5", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Tutor");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "p", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "Scheduling up to thirty days in advance. Start sessions early at any time. Do what you do best and get paid!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "button", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](130, "button", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](131, "button", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "div", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "div", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "h1", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "Tutor Handbook");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "div", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "h2", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "button", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "svg", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](142, "path", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](143, " Create your schedule ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "div", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](146, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](147, "You can tutor whenever you want \u2013 day or night, 365 days a year. When you tutor is always up to you so that it won\u2019t interfere with the most important things in life. Sessions can be scheduled as early as fifteen minutes in the future or up to thirty days in advance.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "h2", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "button", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "svg", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](152, "path", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](153, " Earn what you want ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "div", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](157, " With QuickTutor you can charge whatever you\u2019d like and each session can be any price. For your first fifteen hours of tutoring, we will take 10% of your fare. After that, we\u2019ll just take 7.5% of your fare.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "h2", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "button", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "svg", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](162, "path", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](163, " A biz management tool ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "div", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](167, "Tap and tutor. All you have to do is build up your profile and wait. Learners will send you connection requests, and once you accept them, you can coordinate session details and accept session requests from learners. You can also send photos back and forth in case they have a quick question before a session.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](168, "div", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "h2", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "button", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](171, "svg", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](172, "path", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](173, " Easy profile setup ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](174, "div", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](177, "Profile Pictures");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, "Upload one-to-four pictures of yourself. Note: all user profile pictures are subject to moderation. No nudity.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](180, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](181, "Biography");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](183, "Make sure your bio clearly describes you, what you need, and what you are looking for in a tutor.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](184, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](185, "Preferences & Policies");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](187, "You set your own subjects, rate, schedule, policies, and preferences. Your profile is the face of your business.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "div", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](189, "div", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](190, "div", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](191, "div", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](192, "img", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](193, "div", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](194, "div", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](195, "h1", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](196, "Are you Ready to Become a Goonee Tutor?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](197, "p", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](198, "Download the Goonee iOS Platform in the Apple App Store today for free. Goonee will be launching on Android this Summer.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](199, "div", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](200, "a", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](201, "Download For iOS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](202, "a", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](203, "Download For Androind");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJiZS1hLXR1dG9yLmNvbXBvbmVudC5zY3NzIn0= */"] });


/***/ }),

/***/ "gw8Y":
/*!****************************************************!*\
  !*** ./src/app/shared/services/loading.service.ts ***!
  \****************************************************/
/*! exports provided: LoadingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingService", function() { return LoadingService; });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");


class LoadingService {
    constructor() {
        this.loadingSub = new rxjs__WEBPACK_IMPORTED_MODULE_0__["BehaviorSubject"](false);
        /**
         * Contains in-progress loading requests
         */
        this.loadingMap = new Map();
    }
    setLoading(loading, url) {
        if (!url) {
            throw new Error('The request URL must be provided to the LoadingService.setLoading function');
        }
        if (loading === true) {
            this.loadingMap.set(url, loading);
            this.loadingSub.next(true);
        }
        else if (loading === false && this.loadingMap.has(url)) {
            this.loadingMap.delete(url);
        }
        if (this.loadingMap.size === 0) {
            this.loadingSub.next(false);
        }
    }
}
LoadingService.ɵfac = function LoadingService_Factory(t) { return new (t || LoadingService)(); };
LoadingService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: LoadingService, factory: LoadingService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "hrsj":
/*!**************************************************!*\
  !*** ./src/app/shared/services/token.service.ts ***!
  \**************************************************/
/*! exports provided: TokenService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenService", function() { return TokenService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../environments/environment */ "AytR");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");


class TokenService {
    constructor() {
        this.issuer = {
            login: _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiUrl + '/user/login',
            register: _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].apiUrl + '/user/register'
        };
    }
    handleData(token) {
        localStorage.setItem('token', token);
    }
    getToken() {
        return localStorage.getItem('token');
    }
    // Verify the token
    isValidToken() {
        const token = this.getToken();
        if (token) {
            const payload = this.payload(token);
            if (payload) {
                return Object.values(this.issuer).indexOf(payload.iss) > -1 ? true : false;
            }
        }
        else {
            return false;
        }
    }
    payload(token) {
        const jwtPayload = token.split('.')[1];
        return JSON.parse(atob(jwtPayload));
    }
    // User state based on valid token
    isLoggedIn() {
        return this.isValidToken();
    }
    // Remove token
    removeToken() {
        localStorage.removeItem('token');
    }
}
TokenService.ɵfac = function TokenService_Factory(t) { return new (t || TokenService)(); };
TokenService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: TokenService, factory: TokenService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "iLbm":
/*!********************************************************************!*\
  !*** ./src/app/components/site/userauthentication/signup/users.ts ***!
  \********************************************************************/
/*! exports provided: Users */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Users", function() { return Users; });
class Users {
}


/***/ }),

/***/ "lmtY":
/*!************************************************************************!*\
  !*** ./src/app/components/site/goone-slider/goone-slider.component.ts ***!
  \************************************************************************/
/*! exports provided: GooneSliderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GooneSliderComponent", function() { return GooneSliderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-slick-carousel */ "eSVu");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");



function GooneSliderComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h5");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Esther Howard");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "i", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "i", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "i", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "i", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "(77 Reviews)");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Fitness Trainer");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "$60 per hour");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Connect");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const slide_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("src", slide_r2.img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
class GooneSliderComponent {
    constructor() { }
    ngOnInit() {
    }
}
GooneSliderComponent.ɵfac = function GooneSliderComponent_Factory(t) { return new (t || GooneSliderComponent)(); };
GooneSliderComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: GooneSliderComponent, selectors: [["app-goone-slider"]], inputs: { title: "title", slideArray: "slideArray", slidesConfig: "slidesConfig" }, decls: 6, vars: 3, consts: [[1, "goonee-talents-sec", "py-5", "bg-light", "mt-md-4"], [1, "fw-bold", "top-experts"], [1, "carousel", 3, "config"], ["slickModal", "slick-carousel"], ["ngxSlickItem", "", "class", "slide", 4, "ngFor", "ngForOf"], ["ngxSlickItem", "", 1, "slide"], [1, "goonee-talents-card"], ["alt", "goonee-talents", 3, "src"], [1, "p-3", "p-md-4"], [1, "review-s", "d-flex", "align-self-center"], [1, "fas", "fa-star"], [1, "fas", "fa-star-half-alt"], [1, "far", "fa-star"], ["href", "#", 1, "btn", "btn-primary", "w-100"]], template: function GooneSliderComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h2", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "ngx-slick-carousel", 2, 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, GooneSliderComponent_div_5_Template, 20, 1, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("config", ctx.slidesConfig);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.slideArray);
    } }, directives: [ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_1__["SlickCarouselComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], ngx_slick_carousel__WEBPACK_IMPORTED_MODULE_1__["SlickItemDirective"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJnb29uZS1zbGlkZXIuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "nlBZ":
/*!********************************************************************************!*\
  !*** ./src/app/components/site/userauthentication/signup/custom-validators.ts ***!
  \********************************************************************************/
/*! exports provided: CustomValidators */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomValidators", function() { return CustomValidators; });
class CustomValidators {
    static patternValidator(regex, error) {
        return (control) => {
            if (!control.value) {
                // if control is empty return no error
                return null;
            }
            // test the value of the control against the regexp supplied
            const valid = regex.test(control.value);
            // if true, return no error (no error), else return error passed in the second parameter
            return valid ? null : error;
        };
    }
}


/***/ }),

/***/ "s4QN":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/site/userauthentication/changepassword/changepassword.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: ChangepasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangepasswordComponent", function() { return ChangepasswordComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _signup_custom_validators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../signup/custom-validators */ "nlBZ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/user.service */ "/W/p");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-intl-tel-input */ "t34c");









function ChangepasswordComponent_div_13_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ChangepasswordComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, ChangepasswordComponent_div_13_span_1_Template, 2, 0, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r0.f.password.errors.required);
} }
function ChangepasswordComponent_div_17_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "Invalid");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ChangepasswordComponent_div_17_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "i", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, " Password must be more than 8 characters long");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ChangepasswordComponent_div_17_ng_template_3_span_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "i", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, " Password must contains 1 uppercase, 1 lowercase, 1 numeric and 1 special character");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ChangepasswordComponent_div_17_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](0, ChangepasswordComponent_div_17_ng_template_3_span_0_Template, 3, 0, "span", 26);
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r8.f.password.hasError("hasNumber") || ctx_r8.f.password.hasError("hasCapitalCase") || ctx_r8.f.password.hasError("hasSmallCase") || ctx_r8.f.password.hasError("hasSpecialCharacters"));
} }
function ChangepasswordComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, ChangepasswordComponent_div_17_span_1_Template, 2, 0, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](2, ChangepasswordComponent_div_17_span_2_Template, 3, 0, "span", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](3, ChangepasswordComponent_div_17_ng_template_3_Template, 1, 1, "ng-template", null, 23, _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵreference"](4);
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r1.f.password.errors.minlength || ctx_r1.f.password.hasError("hasNumber") || ctx_r1.f.password.hasError("hasCapitalCase") || ctx_r1.f.password.hasError("hasSmallCase") || ctx_r1.f.password.hasError("hasSpecialCharacters"));
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r1.f.password.errors.minlength)("ngIfElse", _r7);
} }
function ChangepasswordComponent_div_19_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ChangepasswordComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, ChangepasswordComponent_div_19_span_1_Template, 2, 0, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r2.f.cpassword.errors.required);
} }
function ChangepasswordComponent_div_23_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "span", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](1, "i", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](2, " Confirm Password did not match");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} }
function ChangepasswordComponent_div_23_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](1, ChangepasswordComponent_div_23_span_1_Template, 3, 0, "span", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx_r3.f.cpassword.errors.mustMatch);
} }
const _c0 = function (a0) { return { "border-danger": a0 }; };
const _c1 = function (a0, a1) { return { "fa-eye-slash": a0, "fa-eye": a1 }; };
class ChangepasswordComponent {
    constructor(route, router, formbuilder, _service, toastr) {
        this.route = route;
        this.router = router;
        this.formbuilder = formbuilder;
        this._service = _service;
        this.toastr = toastr;
        this.password = "";
        this.cpassword = "";
        this.email = "";
        this.token = "";
        this.buttonText = 'Change Password';
        this.error = '';
        this.success = '';
        this.submitted = false;
        this.changepasswordForm = formbuilder.group({
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required,
                _signup_custom_validators__WEBPACK_IMPORTED_MODULE_1__["CustomValidators"].patternValidator(/\d/, {
                    hasNumber: true
                }),
                _signup_custom_validators__WEBPACK_IMPORTED_MODULE_1__["CustomValidators"].patternValidator(/[A-Z]/, {
                    hasCapitalCase: true
                }),
                _signup_custom_validators__WEBPACK_IMPORTED_MODULE_1__["CustomValidators"].patternValidator(/[a-z]/, {
                    hasSmallCase: true
                }),
                _signup_custom_validators__WEBPACK_IMPORTED_MODULE_1__["CustomValidators"].patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, {
                    hasSpecialCharacters: true
                }),
                _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(8)
            ])),
            cpassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]),
        }, { validators: this.mustMatch('password', 'cpassword') });
    }
    toggleFieldTextType() {
        this.fieldTextType = !this.fieldTextType;
    }
    toggleFieldTextTypeC() {
        this.fieldTextTypeC = !this.fieldTextTypeC;
    }
    get f() {
        return this.changepasswordForm.controls;
    }
    mustMatch(controlName, matchingControlName) {
        return (formGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];
            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                return;
            }
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            }
            else {
                matchingControl.setErrors(null);
            }
        };
    }
    postData(changepasswordForm) {
        this.error = '';
        this.success = '';
        this.submitted = true;
        if (this.changepasswordForm.invalid) {
            return;
        }
        this.buttonText = 'Updating Information...';
        const userData = {
            password: changepasswordForm.controls.password.value,
            email: this.email,
            resetToken: this.token
        };
        this._service.updatePassword(userData)
            .subscribe({
            next: data => {
                if (data.status == 'Failed') {
                    this.error = data.message;
                    this.toastr.error(data.message);
                    this.buttonText = 'Change Password';
                }
                else {
                    this.success = data.message;
                    this.submitted = false;
                    this.toastr.success(data.message);
                    this.buttonText = 'redirecting...';
                    setTimeout(() => {
                        this.router.navigate(['/login']);
                    }, 3000);
                }
            },
            error: error => {
                this.error = 'something went wrong. please try again later!';
                this.buttonText = 'Sign Up';
            }
        });
    }
    ngOnInit() {
        this.route.queryParams
            .subscribe(params => {
            this.token = params.token;
        });
        this._service.getDetail({ resetToken: this.token })
            .subscribe({
            next: data => {
                if (data.status == 'Failed') {
                    this.error = data.message;
                    this.toastr.error(data.message);
                    this.buttonText = 'Change Password';
                }
                else {
                    this.email = data.message;
                }
            },
            error: error => {
                // this.error='something went wrong. please try again later!';
                this.toastr.error('Something went wrong. Please try again later!');
                this.buttonText = 'Reset Password';
            }
        });
    }
}
ChangepasswordComponent.ɵfac = function ChangepasswordComponent_Factory(t) { return new (t || ChangepasswordComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](_services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_5__["ToastrService"])); };
ChangepasswordComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({ type: ChangepasswordComponent, selectors: [["app-changepassword"]], decls: 29, vars: 22, consts: [[1, "hero-banner", "login-signup", "mb-md-5", "mb-4"], [1, "hero-caption"], [1, "container"], [1, "col-lg-5"], [1, "d-flex", "justify-content-center", "align-items-center", "mb-4"], ["height", "45px", "viewBox", "0 0 22 47", "xmlns", "http://www.w3.org/2000/svg", 1, "me-3"], ["d", "M6.80666 23.6021C6.80207 27.0696 7.48229 30.5039 8.80827 33.7079C10.1343 36.9119 12.0799 39.8225 14.5336 42.2726L10.4192 46.387C4.37932 40.3384 0.988195 32.1391 0.991091 23.5912C0.993986 15.0433 4.39067 6.84633 10.4346 0.801758L14.5502 4.91737C12.0916 7.36767 10.1418 10.2799 8.81281 13.4866C7.48387 16.6932 6.80208 20.131 6.80666 23.6021Z"], ["d", "M16.2159 23.6023C16.213 25.8333 16.652 28.0428 17.5075 30.1033C18.363 32.1638 19.6181 34.0345 21.2004 35.6074L17.0937 39.714C14.9715 37.5998 13.2879 35.087 12.1396 32.3202C10.9913 29.5534 10.4009 26.587 10.4023 23.5914C10.4038 20.5957 10.997 17.6299 12.148 14.8641C13.2989 12.0984 14.9849 9.58734 17.1091 7.4751L21.2158 11.5818C19.629 13.1552 18.37 15.0278 17.5117 17.0912C16.6535 19.1545 16.2131 21.3676 16.2159 23.6023Z"], [1, "text-center", "fw-bold", "text-primary", "mb-4"], [3, "formGroup", "ngSubmit"], [1, "passwordgroup", "mb-3", "position-relative"], [4, "ngIf"], ["type", "password", "formControlName", "password", "placeholder", "New Password", 1, "form-control", 3, "type", "ngClass"], [1, "eye-icon"], [1, "far", "fa-eye", 3, "ngClass", "click"], ["formControlName", "cpassword", "placeholder", "Confirm Password", 1, "form-control", 3, "type", "ngClass"], [1, "btn", "btn-primary", "w-100", "mt-2"], [1, "col-lg-6", "offset-lg-6", "p-0"], ["src", "assets/images/goonee-login-image.jpg", "alt", "goonee_login_banner", 1, "w-100", "d-none", "d-lg-block"], ["src", "assets/images/goonee-login-mobile-image.jpg", "alt", "goonee_login_banner", 1, "w-100", "d-block", "d-lg-none"], ["class", "validate-pop text-danger", 4, "ngIf"], [1, "validate-pop", "text-danger"], ["class", "validate-msg text-danger d-block mt-2", 4, "ngIf", "ngIfElse"], ["elseBlock", ""], [1, "validate-msg", "text-danger", "d-block", "mt-2"], [1, "fas", "fa-info-circle"], ["class", "validate-msg text-danger d-block mt-2", 4, "ngIf"]], template: function ChangepasswordComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](4, "h1", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](5, "svg", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](6, "path", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](7, "path", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](8, " Reset Password");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](9, "h6", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](10, "Enter A New Password Below. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](11, "form", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("ngSubmit", function ChangepasswordComponent_Template_form_ngSubmit_11_listener() { return ctx.postData(ctx.changepasswordForm); });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](12, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](13, ChangepasswordComponent_div_13_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](14, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](15, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](16, "i", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ChangepasswordComponent_Template_i_click_16_listener() { return ctx.toggleFieldTextType(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](17, ChangepasswordComponent_div_17_Template, 5, 3, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](18, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](19, ChangepasswordComponent_div_19_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](20, "input", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](21, "span", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](22, "i", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵlistener"]("click", function ChangepasswordComponent_Template_i_click_22_listener() { return ctx.toggleFieldTextTypeC(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtemplate"](23, ChangepasswordComponent_div_23_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](24, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtext"](25);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementStart"](26, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](27, "img", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](28, "img", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("formGroup", ctx.changepasswordForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.password.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("type", ctx.fieldTextType ? "text" : "password")("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction1"](12, _c0, ctx.submitted && ctx.f.password.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction2"](14, _c1, !ctx.fieldTextType, ctx.fieldTextType));
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.password.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.cpassword.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("type", ctx.fieldTextTypeC ? "text" : "password")("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction1"](17, _c0, ctx.submitted && ctx.f.cpassword.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵpureFunction2"](19, _c1, !ctx.fieldTextTypeC, ctx.fieldTextTypeC));
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.cpassword.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵtextInterpolate"](ctx.buttonText);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_7__["NativeElementInjectorDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgClass"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjaGFuZ2VwYXNzd29yZC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "sn8Y":
/*!****************************************************************!*\
  !*** ./src/app/components/site/pages/learn/learn.component.ts ***!
  \****************************************************************/
/*! exports provided: LearnComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LearnComponent", function() { return LearnComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _shared_services_loading_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../shared/services/loading.service */ "gw8Y");


class LearnComponent {
    constructor(_loading) {
        this._loading = _loading;
    }
    ngOnInit() {
        this._loading.setLoading(true, 'Lazy Load');
    }
    ngAfterContentInit() {
        setTimeout(() => {
            this._loading.setLoading(false, 'Lazy Load');
        }, 500);
    }
}
LearnComponent.ɵfac = function LearnComponent_Factory(t) { return new (t || LearnComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shared_services_loading_service__WEBPACK_IMPORTED_MODULE_1__["LoadingService"])); };
LearnComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: LearnComponent, selectors: [["app-learn"]], decls: 190, vars: 0, consts: [[1, "hero-banner"], [1, "hero-caption"], [1, "container"], [1, "col-lg-5"], ["aria-label", "breadcrumb"], [1, "breadcrumb"], [1, "breadcrumb-item"], ["href", "index.html"], ["width", "20", "height", "17", "viewBox", "0 0 20 17", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["fill-rule", "evenodd", "clip-rule", "evenodd", "d", "M19.8639 8.38897C19.6825 8.53657 19.3877 8.53657 19.2063 8.38897L14.9497 4.93544C14.9192 4.91573 14.893 4.89448 14.8688 4.86936L13.045 3.38989C13.0145 3.37018 12.9883 3.34893 12.964 3.32381L9.99976 0.918552L0.793681 8.38897C0.612255 8.53657 0.317496 8.53657 0.13607 8.38897C-0.0453566 8.24175 -0.0453566 8.00258 0.13607 7.85537L9.61453 0.164323C9.62929 0.146163 9.63977 0.125298 9.65929 0.108683C9.7531 0.0329514 9.87691 -0.00220978 9.99976 0.000108542C10.1226 -0.00220978 10.2464 0.0325651 10.3402 0.108683C10.3607 0.124912 10.3698 0.145777 10.3859 0.164323L12.8569 2.17006V1.15811C12.8569 0.944826 13.0697 0.771724 13.3331 0.771724H15.2378C15.3697 0.771724 15.4887 0.815 15.5745 0.884936C15.6611 0.954872 15.714 1.05147 15.714 1.15811V4.48839L19.8639 7.85537C20.0454 8.00258 20.0454 8.24175 19.8639 8.38897ZM14.7616 1.5445H13.8092V2.94284L14.7616 3.71561V1.5445ZM3.33317 7.7267C3.59603 7.7267 3.80936 7.8998 3.80936 8.11309V15.4545C3.80936 15.8814 4.23602 16.2272 4.76173 16.2272H7.61884V11.2042C7.61884 10.9905 7.83169 10.8178 8.09502 10.8178H11.9045C12.1678 10.8178 12.3807 10.9905 12.3807 11.2042V16.2272H15.2378C15.764 16.2272 16.1902 15.8814 16.1902 15.4545V8.11309C16.1902 7.8998 16.403 7.7267 16.6663 7.7267C16.9297 7.7267 17.1425 7.8998 17.1425 8.11309V15.4545C17.1425 16.308 16.2897 17 15.2378 17H4.76173C3.70984 17 2.85699 16.308 2.85699 15.4545V8.11309C2.85699 7.89941 3.07032 7.7267 3.33317 7.7267ZM11.4283 16.2268V11.5902H8.57121V16.2268H11.4283Z"], ["aria-current", "page", 1, "breadcrumb-item", "active"], [1, "d-flex", "flex-wrap", "herobtns", "justify-content-lg-start", "justify-content-center", "mt-4"], ["href", "#", 1, "btn", "btn-primary", "me-3"], ["href", "#", 1, "btn", "btn-secondary", "text-white"], [1, "col-lg-6", "offset-lg-6", "p-0"], ["src", "assets/images/goonee-learn-banner.jpg", "alt", "goonee be a tutor", 1, "w-100", "d-none", "d-lg-block"], ["src", "assets/images/goonee-learn-banner-mobile.jpg", "alt", "goonee be a tutor", 1, "w-100", "d-block", "d-lg-none"], [1, "goonee-cards", "py-5"], [1, "container", "my-md-4"], [1, "fw-bold", "mb-5", "text-md-start", "text-center"], [1, "d-none", "d-sm-block"], [1, "row", "gx-md-5", "gx-3"], [1, "col"], ["href", "#", 1, "card-view", "px-4", "py-5", "d-block"], ["src", "assets/images/goonee-icon-3.png", "alt", "goonee anything"], ["src", "assets/images/goonee-icon-2.png", "alt", "goonee person"], ["src", "assets/images/goonee-icon-4.png", "alt", "goonee teaching"], ["id", "carouselGooneetwo", "data-bs-ride", "carousel", 1, "carousel", "slide", "d-block", "d-sm-none"], [1, "carousel-inner"], [1, "carousel-item", "active", "p-3"], ["src", "assets/images/goonee-icon-3.png", "alt", "goonee control"], [1, "carousel-item", "p-3"], ["src", "assets/images/goonee-icon-2.png", "alt", "goonee Simplicity"], ["src", "assets/images/goonee-icon-4.png", "alt", "goonee Earn money teaching"], [1, "carousel-indicators"], ["type", "button", "data-bs-target", "#carouselGooneetwo", "data-bs-slide-to", "0", "aria-current", "true", "aria-label", "Slide 1", 1, "active"], ["type", "button", "data-bs-target", "#carouselGooneetwo", "data-bs-slide-to", "1", "aria-label", "Slide 2"], ["type", "button", "data-bs-target", "#carouselGooneetwo", "data-bs-slide-to", "2", "aria-label", "Slide 3"], [1, "py-5", "bg-dark", "text-white", "text-center"], [1, "mb-3", "mb-md-5"], [1, "fw-bold", "mb-3"], [1, "fw-light"], [1, "row", "gx-md-5", "gx-3", "px-3", "px-sm-0"], [1, "col-md-4", "my-3", "px-lg-5"], ["src", "assets/images/goonee-search-hiw.png", "alt", "goonee Create", 1, "mw-100", "px-md-5", "px-3"], [1, "fw-bold", "mt-md-5", "mt-3", "mb-3"], ["src", "assets/images/goonee-connect-hiw.png", "alt", "goonee Connect", 1, "mw-100", "px-md-5", "px-3"], ["src", "assets/images/goonee-learn-hiw.png", "alt", "goonee Tutor", 1, "mw-100", "px-md-5", "px-3"], ["id", "carouselGooneeHIW", "data-bs-ride", "carousel", 1, "carousel", "slide", "d-block", "d-sm-none"], [1, "my-2"], ["src", "assets/images/goonee-search-hiw.png", "alt", "goonee Search", 1, "mw-100", "px-5"], [1, "fw-light", "mb-1"], ["src", "assets/images/goonee-connect-hiw.png", "alt", "goonee Connect", 1, "mw-100", "px-5"], ["src", "assets/images/goonee-learn-hiw.png", "alt", "goonee Tutor", 1, "mw-100", "px-5"], ["type", "button", "data-bs-target", "#carouselGooneeHIW", "data-bs-slide-to", "0", "aria-current", "true", "aria-label", "Slide 1", 1, "active"], ["type", "button", "data-bs-target", "#carouselGooneeHIW", "data-bs-slide-to", "1", "aria-label", "Slide 2"], ["type", "button", "data-bs-target", "#carouselGooneeHIW", "data-bs-slide-to", "2", "aria-label", "Slide 3"], [1, "py-5"], [1, "container", "py-md-4", "py-2"], [1, "fw-bold", "mb-md-5", "mb-4", "text-center"], [1, "col-lg-8", "offset-lg-2"], ["id", "accordionTH", 1, "accordion"], [1, "accordion-item"], ["id", "headingOne", 1, "accordion-header"], ["type", "button", "data-bs-toggle", "collapse", "data-bs-target", "#collapseOne", "aria-expanded", "true", "aria-controls", "collapseOne", 1, "accordion-button"], ["width", "32", "height", "29", "viewBox", "0 0 32 29", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["d", "M22.9775 2.15289H26.6301C29.4283 2.15289 31.6295 4.30335 31.6295 7.03709V23.9426C31.6295 26.6763 29.4283 28.8268 26.6301 28.8268H5.48079C2.68261 28.8268 0.481445 26.6763 0.481445 23.9426V7.03709C0.481445 4.30335 2.68261 2.15289 5.48079 2.15289H9.13342V1.40188C9.13342 0.734374 9.60426 0.274414 10.2875 0.274414C10.9707 0.274414 11.4415 0.734412 11.4415 1.40188V2.15289H20.6694V1.40188C20.6694 0.734339 21.1402 0.274414 21.8235 0.274414C22.5067 0.274414 22.9775 0.734412 22.9775 1.40188V2.15289ZM9.13342 4.40771H5.48079C4.04906 4.40771 2.78943 5.63833 2.78943 7.03709V9.66646H29.3216V7.03709C29.3216 5.63833 28.062 4.40771 26.6303 4.40771H22.9776V5.15872C22.9776 5.82626 22.5069 6.28619 21.8236 6.28619C21.1403 6.28619 20.6695 5.82619 20.6695 5.15872V4.40771H11.4415V5.15872C11.4415 5.82623 10.9707 6.28619 10.2875 6.28619C9.60422 6.28619 9.13342 5.82619 9.13342 5.15872V4.40771ZM2.78943 11.9213V23.9426C2.78943 25.3413 4.04906 26.572 5.48079 26.572H26.6301C28.0619 26.572 29.3215 25.3413 29.3215 23.9426V11.9213H2.78943ZM7.40344 23.9426C7.9802 23.9426 8.36482 23.7548 8.74932 23.3791C9.13383 23.0035 9.32609 22.4399 9.32609 22.0642C9.32609 21.6886 9.13383 21.125 8.74932 20.7493C7.98031 19.998 6.82668 19.998 6.05755 20.7493C5.67304 21.125 5.48079 21.5006 5.48079 22.0642C5.48079 22.4399 5.67304 23.0035 6.05755 23.3791C6.44206 23.7548 6.82668 23.9426 7.40344 23.9426ZM7.40344 18.3074C7.9802 18.3074 8.36482 18.1196 8.74932 17.7439C9.13383 17.3683 9.32609 16.8047 9.32609 16.429C9.32609 16.0534 9.13383 15.4898 8.74932 15.1141C7.98031 14.3628 6.82668 14.3628 6.05755 15.1141C5.67304 15.4898 5.48079 15.8654 5.48079 16.429C5.48079 16.9926 5.67304 17.3683 6.05755 17.7439C6.44206 18.1196 6.82668 18.3074 7.40344 18.3074ZM13.1715 23.9426C13.7483 23.9426 14.1329 23.7548 14.5174 23.3791C14.9019 23.0035 15.0941 22.4399 15.0941 22.0642C15.0941 21.6886 14.9019 21.125 14.5174 20.7493C13.7484 19.998 12.5947 19.998 11.8256 20.7493C11.4411 21.125 11.2488 21.6886 11.2488 22.0642C11.2488 22.4399 11.4411 23.0035 11.8256 23.3791C12.2101 23.7548 12.5947 23.9426 13.1715 23.9426ZM13.1715 18.3074C13.7483 18.3074 14.1329 18.1196 14.5174 17.7439C14.9019 17.3683 15.0941 16.8047 15.0941 16.429C15.0941 16.0534 14.9019 15.4898 14.5174 15.1141C13.7484 14.3628 12.5947 14.3628 11.8256 15.1141C11.4411 15.4898 11.2488 16.0534 11.2488 16.429C11.2488 16.8047 11.4411 17.3683 11.8256 17.7439C12.2101 18.1196 12.5947 18.3074 13.1715 18.3074ZM18.9394 23.9426C19.5162 23.9426 19.9008 23.7548 20.2853 23.3791C20.6698 23.0035 20.8621 22.6278 20.8621 22.0642C20.8621 21.5006 20.6698 21.125 20.2853 20.7493C19.5163 19.998 18.3627 19.998 17.5936 20.7493C17.209 21.125 17.0168 21.6886 17.0168 22.0642C17.0168 22.4399 17.209 23.0035 17.5936 23.3791C17.9781 23.7548 18.3627 23.9426 18.9394 23.9426ZM18.9394 18.3074C19.5162 18.3074 19.9008 18.1196 20.2853 17.7439C20.6698 17.3683 20.8621 16.9926 20.8621 16.429C20.8621 16.0534 20.6698 15.4898 20.2853 15.1141C19.5163 14.3628 18.3627 14.3628 17.5936 15.1141C17.209 15.4898 17.0168 16.0534 17.0168 16.429C17.0168 16.8047 17.209 17.3683 17.5936 17.7439C17.9781 18.1196 18.3627 18.3074 18.9394 18.3074ZM24.7075 23.9426C25.2843 23.9426 25.6689 23.7548 26.0534 23.3791C26.4379 23.0035 26.6301 22.4399 26.6301 22.0642C26.6301 21.6886 26.4379 21.125 26.0534 20.7493C25.2844 19.998 24.1307 19.998 23.3616 20.7493C22.9771 21.125 22.7848 21.5006 22.7848 22.0642C22.7848 22.4399 22.9771 23.0035 23.3616 23.3791C23.7461 23.7548 24.1307 23.9426 24.7075 23.9426ZM24.7075 18.3074C25.2843 18.3074 25.6689 18.1196 26.0534 17.7439C26.4379 17.3683 26.6301 16.9926 26.6301 16.429C26.6301 16.0534 26.4379 15.4898 26.0534 15.1141C25.2844 14.3628 24.1307 14.3628 23.3616 15.1141C22.9771 15.4898 22.7848 16.0534 22.7848 16.429C22.7848 16.8047 22.9771 17.3683 23.3616 17.7439C23.7461 18.1196 24.1307 18.3074 24.7075 18.3074Z"], ["id", "collapseOne", "aria-labelledby", "headingOne", "data-bs-parent", "#accordionTH", 1, "accordion-collapse", "collapse", "show"], [1, "accordion-body"], ["id", "headingTwo", 1, "accordion-header"], ["type", "button", "data-bs-toggle", "collapse", "data-bs-target", "#collapseTwo", "aria-expanded", "false", "aria-controls", "collapseTwo", 1, "accordion-button", "collapsed"], ["width", "26", "height", "35", "viewBox", "0 0 26 35", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["d", "M25.0517 22.7767C25.0517 19.6367 23.7897 16.6847 21.4977 14.4643C17.9938 11.07 16.8227 6.14094 18.4412 1.60086C18.5505 1.29424 18.5 0.955195 18.3058 0.690654C18.1112 0.426374 17.7969 0.269531 17.4617 0.269531H17.3565C13.1133 0.269531 9.66124 3.61368 9.66124 7.72426V14.1749C9.66124 14.807 9.1302 15.3215 8.47773 15.3215C7.82499 15.3215 7.29395 14.807 7.29395 14.1749C7.29395 13.7946 7.07215 13.4469 6.72109 13.277C6.36976 13.1071 5.94962 13.144 5.63553 13.3724C4.16195 14.4447 2.94093 15.8498 2.10443 17.4357C1.22719 19.0995 0.782227 20.8964 0.782227 22.777C0.782227 29.2031 6.13257 34.4403 12.7444 34.5302C12.7997 34.5315 12.8553 34.5323 12.9112 34.5323C12.9131 34.5323 12.9149 34.5323 12.9171 34.5323C12.9174 34.5323 12.9171 34.5323 12.9171 34.5323C12.9751 34.5323 13.0329 34.5315 13.0906 34.5302C19.7019 34.4398 25.0517 29.2026 25.0517 22.7767ZM16.2477 31.1884C15.3583 32.05 14.1756 32.5247 12.9174 32.5247H12.9171C11.6588 32.5247 10.4759 32.05 9.58623 31.1882C8.69657 30.3263 8.20654 29.1803 8.20654 27.9617C8.20654 26.7427 8.69657 25.597 9.58623 24.7352L12.9171 21.5084L16.2477 24.7352C18.0842 26.5143 18.0842 29.4091 16.2477 31.1884ZM19.2291 30.3621C20.1806 28.004 19.6763 25.2172 17.7132 23.3155L13.6497 19.379C13.4554 19.1908 13.1921 19.0852 12.9171 19.0852C12.6424 19.0852 12.3788 19.1908 12.1842 19.379L8.12101 23.3155C6.83982 24.5566 6.13419 26.2066 6.13419 27.9617C6.13419 28.7942 6.29339 29.6033 6.59723 30.3561C4.31602 28.5676 2.85458 25.8346 2.85458 22.7767C2.85458 20.1642 3.89993 17.7316 5.76451 15.9169C6.34844 16.7673 7.34657 17.329 8.47746 17.329C10.2727 17.329 11.7333 15.9141 11.7333 14.1749V7.72426C11.7333 5.15832 13.5742 3.00095 16.044 2.4269C15.5488 4.51396 15.5367 6.68832 16.0167 8.79602C16.6282 11.4804 18.0168 13.9313 20.0322 15.884C21.9327 17.7251 22.9794 20.1731 22.9794 22.777C22.9794 25.838 21.5147 28.5739 19.2291 30.3621Z"], ["id", "collapseTwo", "aria-labelledby", "headingTwo", "data-bs-parent", "#accordionTH", 1, "accordion-collapse", "collapse"], ["id", "headingThree", 1, "accordion-header"], ["type", "button", "data-bs-toggle", "collapse", "data-bs-target", "#collapseThree", "aria-expanded", "false", "aria-controls", "collapseThree", 1, "accordion-button", "collapsed"], ["width", "35", "height", "35", "viewBox", "0 0 35 35", "fill", "none", "xmlns", "http://www.w3.org/2000/svg"], ["d", "M29.7202 22.0376C29.8727 21.6818 30.2215 21.4509 30.6031 21.4494H30.731C33.0553 21.4494 34.9462 19.5587 34.9462 17.2347C34.9462 14.9108 33.0553 13.0201 30.731 13.0201H30.495C30.1674 13.0188 29.8674 12.8533 29.6904 12.5869C29.6708 12.4901 29.6409 12.3955 29.6008 12.3046C29.4404 11.9414 29.515 11.5235 29.7908 11.2383L29.8661 11.163C29.8683 11.161 29.8703 11.1589 29.8723 11.1569C30.668 10.3604 31.1059 9.30175 31.1052 8.17596C31.1046 7.0521 30.6669 5.99564 29.8729 5.20049C29.8708 5.19836 29.8687 5.1963 29.8667 5.19418C29.0706 4.3991 28.0128 3.96141 26.8877 3.96141C26.887 3.96141 26.8861 3.96141 26.8853 3.96141C25.7594 3.96201 24.7011 4.40102 23.906 5.19697L23.8273 5.27573C23.5421 5.55138 23.1241 5.62602 22.7608 5.46572C22.7562 5.46379 22.7517 5.4618 22.7472 5.45987C22.3914 5.3074 22.1604 4.9587 22.159 4.57713V4.44923C22.159 2.12534 20.268 0.234619 17.9437 0.234619C15.6195 0.234619 13.7285 2.12534 13.7285 4.44923V4.68531C13.7272 5.01282 13.5617 5.31278 13.2952 5.48969C13.1985 5.50928 13.1038 5.53929 13.013 5.57934C12.6498 5.73971 12.2317 5.66507 11.9465 5.38935L11.8651 5.30787C11.0689 4.51278 10.0113 4.0751 8.8862 4.0751C8.88541 4.0751 8.88448 4.0751 8.88375 4.0751C7.75781 4.0757 6.69948 4.51471 5.90157 5.31345C5.10584 6.10986 4.66797 7.16851 4.66863 8.2943C4.66923 9.42008 5.1083 10.4782 5.90436 11.2732L5.98306 11.3519C6.25881 11.6371 6.33346 12.055 6.17314 12.4183C6.16138 12.4448 6.15062 12.4716 6.14059 12.4987C6.00351 12.8713 5.66074 13.1194 5.26498 13.1337H5.15652C2.83238 13.1337 0.941406 15.0244 0.941406 17.3483C0.941406 19.6723 2.83238 21.563 5.15672 21.5628H5.39276C5.77996 21.5644 6.12857 21.7952 6.28106 22.151C6.28299 22.1556 6.28498 22.1601 6.28691 22.1645C6.4473 22.5278 6.37265 22.9457 6.09683 23.2309L6.01813 23.3096C6.01633 23.3114 6.01454 23.3133 6.01268 23.3152C5.21868 24.1112 4.7818 25.1688 4.7824 26.2933C4.783 27.4191 5.22207 28.4773 6.01866 29.2728C6.02058 29.2748 6.02257 29.2768 6.02457 29.2787C7.66928 30.9176 10.3406 30.9148 11.9815 29.2723L12.0603 29.1935C12.3454 28.9179 12.7634 28.8431 13.1267 29.0035C13.1531 29.0151 13.18 29.026 13.2071 29.036C13.5798 29.1731 13.8278 29.5158 13.8422 29.9116V30.0199C13.8422 32.3439 15.7332 34.2346 18.0574 34.2346C20.3817 34.2346 22.2726 32.3439 22.2727 30.0201V29.7841C22.2743 29.397 22.5051 29.0483 22.8609 28.8959C22.8655 28.8939 22.8699 28.892 22.8744 28.89C23.2378 28.7297 23.6557 28.8042 23.941 29.08L24.0224 29.1614C24.8185 29.9565 25.8762 30.3943 27.0013 30.3942C27.0021 30.3942 27.003 30.3942 27.0038 30.3942C28.1297 30.3935 29.188 29.9546 29.9859 29.1559C31.6285 27.5117 31.627 24.8378 29.9832 23.196L29.9045 23.1173C29.6288 22.8321 29.5541 22.4143 29.7144 22.0509C29.7164 22.0465 29.7184 22.0421 29.7202 22.0376ZM27.9445 25.0424C27.9481 25.046 27.9516 25.0496 27.9552 25.0532L28.0409 25.1389C28.6135 25.711 28.6141 26.6421 28.0398 27.2169C27.7627 27.4944 27.3941 27.6472 27.002 27.6474C27.0016 27.6474 27.0014 27.6474 27.0011 27.6474C26.6093 27.6474 26.2409 27.495 25.962 27.2164L25.8768 27.1312C25.8732 27.1276 25.8696 27.1241 25.8659 27.1205C24.7747 26.0534 23.1683 25.7608 21.7713 26.3743C20.4125 26.9599 19.5312 28.2932 19.5252 29.7786V30.0201C19.5252 30.8294 18.8667 31.4879 18.0572 31.4879C17.2478 31.4879 16.5893 30.8294 16.5893 30.0201V29.8922C16.5893 29.8815 16.5892 29.8709 16.5889 29.8601C16.5533 28.3385 15.6157 27.0138 14.1961 26.4734C12.807 25.879 11.2172 26.1755 10.1348 27.2341C10.1312 27.2376 10.1276 27.2412 10.1241 27.2447L10.0382 27.3305C9.46627 27.9031 8.53493 27.9037 7.96223 27.3317C7.96057 27.3301 7.95885 27.3284 7.95725 27.3268C7.68156 27.05 7.52974 26.6827 7.52947 26.292C7.52921 25.8999 7.68176 25.5313 7.95885 25.2539C7.96044 25.2524 7.96197 25.2507 7.96356 25.2492L8.04578 25.1669C8.04937 25.1633 8.05296 25.1597 8.05648 25.1561C9.1237 24.0652 9.41626 22.459 8.80292 21.0622C8.21727 19.7034 6.88372 18.8222 5.39801 18.8162H5.15645C4.34705 18.8162 3.68854 18.1577 3.68854 17.3484C3.68854 16.5391 4.34712 15.8807 5.15645 15.8806H5.28437C5.29506 15.8806 5.30582 15.8805 5.31651 15.8802C6.83842 15.8446 8.16327 14.9071 8.70376 13.4877C9.29804 12.0988 9.0017 10.5094 7.94291 9.42712C7.93939 9.42354 7.9358 9.41995 7.93221 9.41636L7.84647 9.33057C7.56906 9.05345 7.41617 8.68496 7.41597 8.2929C7.4157 7.9009 7.56826 7.53222 7.8476 7.25258C8.12475 6.9752 8.49329 6.82233 8.88541 6.82213C8.88574 6.82213 8.88594 6.82213 8.88627 6.82213C9.27812 6.82213 9.64652 6.9746 9.9254 7.25318L10.0107 7.33838C10.0142 7.34196 10.0179 7.34555 10.0214 7.34907C11.0454 8.35047 12.5231 8.66976 13.8559 8.19834C13.9818 8.1822 14.1052 8.14867 14.2225 8.0984C15.5854 7.51435 16.47 6.17899 16.4759 4.69095V4.44943C16.4759 3.64014 17.1344 2.98172 17.9438 2.98172C18.7532 2.98172 19.4118 3.6402 19.4118 4.4495V4.58284C19.4176 6.06284 20.2991 7.39621 21.658 7.98185C23.0551 8.59531 24.6614 8.3026 25.7525 7.23551C25.7561 7.23199 25.7597 7.22841 25.7633 7.22482L25.8491 7.13909C26.1262 6.86171 26.4948 6.70884 26.8869 6.70864C26.8872 6.70864 26.8875 6.70864 26.8877 6.70864C27.2783 6.70864 27.6458 6.86018 27.9227 7.1355C27.9242 7.1371 27.9258 7.13862 27.9273 7.14022C28.2048 7.41733 28.3577 7.78582 28.3579 8.17788C28.3582 8.56889 28.2065 8.93651 27.9309 9.21362C27.9296 9.21495 27.9282 9.21628 27.9268 9.21768L27.8416 9.30294C27.838 9.30646 27.8345 9.31005 27.8309 9.3137C26.8294 10.3375 26.51 11.8151 26.9815 13.1476C26.9977 13.2736 27.0313 13.397 27.0815 13.5143C27.6657 14.877 29.0012 15.7614 30.4894 15.7673H30.7309C31.5404 15.7673 32.1989 16.4258 32.1989 17.2351C32.1989 18.0443 31.5403 18.7028 30.7309 18.7028H30.5976C29.1174 18.7087 27.7839 19.59 27.1982 20.9487C26.5847 22.3452 26.8773 23.9515 27.9445 25.0424ZM17.9437 11.5995C14.8361 11.5995 12.3077 14.1274 12.3077 17.2347C12.3077 20.342 14.836 22.87 17.9437 22.87C21.0514 22.87 23.5797 20.342 23.5797 17.2347C23.5797 14.1274 21.0514 11.5995 17.9437 11.5995ZM17.9437 20.123C16.3509 20.123 15.055 18.8273 15.055 17.2347C15.055 15.642 16.3509 14.3463 17.9437 14.3463C19.5366 14.3463 20.8325 15.642 20.8325 17.2347C20.8325 18.8273 19.5366 20.123 17.9437 20.123Z"], ["id", "collapseThree", "aria-labelledby", "headingThree", "data-bs-parent", "#accordionTH", 1, "accordion-collapse", "collapse"], [1, "py-5", "bg-light"], [1, "container", "py-0", "py-md-5"], [1, "row", "gx-md-5", "gx-3", "align-items-center", "px-3", "px-sm-0"], [1, "col-md-5", "text-center"], ["src", "assets/images/goonee-become-tutor.png", "alt", "goonee-statistics-glance", 1, "mw-100"], [1, "col-md-1"], [1, "col-md-6", "text-center", "text-md-start", "mt-3", "mb-3"], [1, "fw-bold", "mb-4"], [1, "d-flex", "flex-wrap", "justify-content-lg-start", "justify-content-center", "mt-4", "px-3", "px-md-0"], ["href", "#", 1, "btn", "btn-primary", "mt-2", "me-sm-3"], ["href", "#", 1, "btn", "btn-secondary", "mt-2", "text-white"]], template: function LearnComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "nav", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "ol", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "li", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "svg", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "path", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "li", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Learn");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Learn Anything");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolorum impedit autem labore quam! Doloribus aperiam architecto harum vel in doloremque eaque labore, dolorem nihil odio nam mollitia vitae. Odio, eaque?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Download For iOS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Download For Android");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "img", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h1", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Overview");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Literally anything");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Find a tutor for anything. From pottery or chemistry, to travel guidance and physical fitness, our twelve categories sum up life.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "img", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Online or in person");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Schedule sessions in person or online for a specific subject, day/time, and price. Sessions occur directly in the app through meet-up or video call.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "img", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Teaching that fits you");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Find a tutor that fits your price, distance, and availability. No subscriptions or upfront payments. Only pay for the time you need.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "img", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Literally anything");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Find a tutor for anything. From pottery or chemistry, to travel guidance and physical fitness, our twelve categories sum up life.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "img", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "Online or in person");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "Schedule sessions in person or online for a specific subject, day/time, and price. Sessions occur directly in the app through meet-up or video call.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](69, "img", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Teaching that fits you");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Find a tutor that fits your price, distance, and availability. No subscriptions or upfront payments. Only pay for the time you need.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "button", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "button", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](77, "button", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "h1", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "How It works");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "p", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Available on the App Store & Playstore");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "img", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "h5", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "Search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "p", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "Search for any subject topic or idea and find a tutor that fits you.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](94, "img", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "h5", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "p", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "Connect, message, and schedule with just a few taps. You can also send photos, documents, and videos!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](100, "img", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "h5", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "Learn");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "p", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Schedule online or in-person sessions up to thirty days in advance or instantly call an expert!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "div", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "img", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "h5", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Search");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "p", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Search for any subject topic or idea and find a tutor that fits you.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](116, "img", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "h5", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "Connect");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "p", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](120, "Connect, message, and schedule with just a few taps. You can also send photos, documents, and videos!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "div", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](123, "img", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "h5", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](125, "Learn");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "p", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "Schedule online or in-person sessions up to thirty days in advance or instantly call an expert!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "button", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](130, "button", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](131, "button", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "div", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "div", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "h1", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "Learner Handbook");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "div", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "div", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "h2", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "button", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "svg", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](142, "path", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](143, " Learning made easy ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "div", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](146, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](147, "Learn anything, from anyone, anywhere, at any time. Connect to anyone in your area or all over the world, negotiate a price, and begin learning. You can learn in person or through instant video calling. You can schedule a session as soon as 15 minutes in advance or late as 30 days in advance.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "div", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "h2", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "button", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "svg", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](152, "path", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](153, " Thousands of Subjects ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "div", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](157, "QuickTutor has over 9,000 subjects divided into twelve categories with seventy-two subcategories. We offer tutoring in any academic subject, skill, hobby, language, or talent you can think of! From auto and health to tech and remedial education - we aim to educate all of world. We also have a submit queue in which anyone can submit a subject they believe should be available to learn or teach!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "div", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "h2", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](160, "button", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "svg", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](162, "path", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](163, " Easy profile setup ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](164, "div", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](166, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](167, "Profile Pictures");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](168, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](169, "Upload one-to-four pictures of yourself. Note: all user profile pictures are subject to moderation. No nudity.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](171, "Biography");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](173, "Make sure your bio clearly describes you, what you need, and what you are looking for in a tutor.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](174, "div", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "div", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "div", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "div", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](178, "img", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](179, "div", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](180, "div", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "h1", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](182, "Are you Ready to Learn with Goonee?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "p", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](184, "Download the Goonee iOS Platform in the Apple App Store today for free. Goonee will be launching on Android this Summer.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "div", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "a", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](187, "Download For iOS");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "a", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](189, "Download For Androind");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsZWFybi5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "t2kx":
/*!*******************************************************************************!*\
  !*** ./src/app/components/site/userauthentication/signup/signup.component.ts ***!
  \*******************************************************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _users__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users */ "iLbm");
/* harmony import */ var _custom_validators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./custom-validators */ "nlBZ");
/* harmony import */ var ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-intl-tel-input */ "t34c");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/user.service */ "/W/p");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-cookie-service */ "b6Qw");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _shared_services_token_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./../../../../shared/services/token.service */ "hrsj");
/* harmony import */ var _shared_services_loading_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../shared/services/loading.service */ "gw8Y");
/* harmony import */ var _unique_email_validator_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./unique-email-validator.service */ "FBdK");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ "ofXK");















function SignupComponent_div_12_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_12_span_1_Template, 2, 0, "span", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r0.f.fname.errors.required);
} }
function SignupComponent_div_14_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "i", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, " Atleast 3 characters required");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_14_span_1_Template, 3, 0, "span", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r1.f.fname.hasError("minlength"));
} }
function SignupComponent_div_16_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_16_span_1_Template, 2, 0, "span", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r2.f.lname.errors.required);
} }
function SignupComponent_div_18_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "i", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, " Atleast 3 characters required");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_18_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_18_span_1_Template, 3, 0, "span", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r3.f.lname.hasError("minlength"));
} }
function SignupComponent_div_20_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_20_span_1_Template, 2, 0, "span", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r4.f.email.errors.required);
} }
function SignupComponent_div_22_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "Invalid Email");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_22_span_1_Template, 2, 0, "span", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r5.f.email.errors.email);
} }
function SignupComponent_div_23_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "i", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, " This email is already taken.");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_23_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_23_span_1_Template, 3, 0, "span", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r6.f.email.hasError("emailTaken"));
} }
function SignupComponent_div_25_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_25_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_25_span_1_Template, 2, 0, "span", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r7.f.phone.errors.required);
} }
function SignupComponent_a_27_Template(rf, ctx) { if (rf & 1) {
    const _r24 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "a", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function SignupComponent_a_27_Template_a_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r24); const ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r23.sendOTP(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "Get Code");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_28_Template(rf, ctx) { if (rf & 1) {
    const _r30 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "input", 47, 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("keypress", function SignupComponent_div_28_Template_input_keypress_3_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r29.numberOnly($event); })("input", function SignupComponent_div_28_Template_input_input_3_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const _r26 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](7); const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r31.onInputEntry($event, _r26); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](6, "input", 49, 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("keypress", function SignupComponent_div_28_Template_input_keypress_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r32.numberOnly($event); })("input", function SignupComponent_div_28_Template_input_input_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const _r27 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](10); const ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r33.onInputEntry($event, _r27); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](8, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "input", 51, 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("keypress", function SignupComponent_div_28_Template_input_keypress_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const ctx_r34 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r34.numberOnly($event); })("input", function SignupComponent_div_28_Template_input_input_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const _r28 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](13); const ctx_r35 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r35.onInputEntry($event, _r28); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](11, "div", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](12, "input", 49, 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("keypress", function SignupComponent_div_28_Template_input_keypress_12_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r36.numberOnly($event); })("input", function SignupComponent_div_28_Template_input_input_12_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const _r28 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](13); const ctx_r37 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r37.onInputEntry($event, _r28); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](14, "p", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](15, "Didn\u2019t get Passcode? ");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](16, "a", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function SignupComponent_div_28_Template_a_click_16_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵrestoreView"](_r30); const ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](); return ctx_r38.reSendOTP(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](17, "Resend Passcode");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_30_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_30_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_30_span_1_Template, 2, 0, "span", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r10.f.password.errors.required);
} }
function SignupComponent_div_34_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "Invalid");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_34_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "i", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, " Password must be more than 8 characters long");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_34_ng_template_3_span_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "i", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, " Password must contains 1 uppercase, 1 lowercase, 1 numeric and 1 special character");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_34_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](0, SignupComponent_div_34_ng_template_3_span_0_Template, 3, 0, "span", 40);
} if (rf & 2) {
    const ctx_r43 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r43.f.password.hasError("hasNumber") || ctx_r43.f.password.hasError("hasCapitalCase") || ctx_r43.f.password.hasError("hasSmallCase") || ctx_r43.f.password.hasError("hasSpecialCharacters"));
} }
function SignupComponent_div_34_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_34_span_1_Template, 2, 0, "span", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](2, SignupComponent_div_34_span_2_Template, 3, 0, "span", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](3, SignupComponent_div_34_ng_template_3_Template, 1, 1, "ng-template", null, 57, _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const _r42 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵreference"](4);
    const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r11.f.password.errors.minlength || ctx_r11.f.password.hasError("hasNumber") || ctx_r11.f.password.hasError("hasCapitalCase") || ctx_r11.f.password.hasError("hasSmallCase") || ctx_r11.f.password.hasError("hasSpecialCharacters"));
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r11.f.password.errors.minlength)("ngIfElse", _r42);
} }
function SignupComponent_div_36_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 39);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_36_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_36_span_1_Template, 2, 0, "span", 38);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r12.f.cpassword.errors.required);
} }
function SignupComponent_div_40_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "i", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, " Confirm Password did not match");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_40_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_40_span_1_Template, 3, 0, "span", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r13.f.cpassword.errors.mustMatch);
} }
function SignupComponent_div_48_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "span", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](1, "i", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](2, " Please accept terms & conditions");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} }
function SignupComponent_div_48_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](1, SignupComponent_div_48_span_1_Template, 3, 0, "span", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx_r14.f.terms.errors);
} }
const _c0 = function (a0) { return { "border-danger": a0 }; };
const _c1 = function (a0, a1) { return [a0, a1]; };
const _c2 = function (a0, a1) { return { "fa-eye-slash": a0, "fa-eye": a1 }; };
class SignupComponent {
    constructor(formbuilder, _service, cookieService, router, token, _loading, customValidator, toastr) {
        this.formbuilder = formbuilder;
        this._service = _service;
        this.cookieService = cookieService;
        this.router = router;
        this.token = token;
        this._loading = _loading;
        this.customValidator = customValidator;
        this.toastr = toastr;
        this.title = 'Goonee Signup';
        this.separateDialCode = false;
        this.SearchCountryField = ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_3__["SearchCountryField"];
        this.CountryISO = ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_3__["CountryISO"];
        this.PhoneNumberFormat = ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_3__["PhoneNumberFormat"];
        this.preferredCountries = [ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_3__["CountryISO"].UnitedStates, ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_3__["CountryISO"].UnitedKingdom];
        this.firstname = "";
        this.lastname = "";
        this.email = "";
        this.phone = "";
        this.password = "";
        this.cpassword = "";
        this.terms = "";
        this.num = "";
        this.submitted = false;
        this.getCodeButton = false;
        this.buttonDisable = false;
        this.buttonText = 'Sign Up';
        this.opt = [];
        this.showOTPInput = false;
        this.passwordInputDisable = true;
        this.error = '';
        this.success = '';
        this.validphone = '';
        this.signupForm = formbuilder.group({
            fname: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(3)]),
            lname: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(3)]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].email], this.customValidator.validateEmailNotTaken.bind(this.customValidator)),
            num: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](''),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required,
                _custom_validators__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].patternValidator(/\d/, {
                    hasNumber: true
                }),
                _custom_validators__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].patternValidator(/[A-Z]/, {
                    hasCapitalCase: true
                }),
                _custom_validators__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].patternValidator(/[a-z]/, {
                    hasSmallCase: true
                }),
                _custom_validators__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, {
                    hasSpecialCharacters: true
                }),
                _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(8)
            ])),
            cpassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]),
            terms: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](false, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].requiredTrue]),
        }, { validators: this.mustMatch('password', 'cpassword') });
    }
    numberOnly(event) {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    toggleFieldTextType() {
        this.fieldTextType = !this.fieldTextType;
    }
    toggleFieldTextTypeC() {
        this.fieldTextTypeC = !this.fieldTextTypeC;
    }
    get f() {
        return this.signupForm.controls;
    }
    mustMatch(controlName, matchingControlName) {
        return (formGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];
            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                return;
            }
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            }
            else {
                matchingControl.setErrors(null);
            }
        };
    }
    postData(signupForm) {
        this.error = '';
        this.success = '';
        this.submitted = true;
        console.log(signupForm);
        if (this.signupForm.invalid) {
            return;
        }
        this.buttonText = 'Sending Information...';
        this.buttonDisable = true;
        this.firstname = signupForm.controls.fname.value;
        this.lastname = signupForm.controls.lname.value;
        this.email = signupForm.controls.email.value;
        this.password = signupForm.controls.password.value;
        this.terms = signupForm.controls.terms.value;
        const user = new _users__WEBPACK_IMPORTED_MODULE_1__["Users"]();
        user.firstname = this.firstname;
        user.lastname = this.lastname;
        user.email = this.email;
        user.password = this.password;
        user.phone = signupForm.controls.phone.value.number;
        user.dialCode = signupForm.controls.phone.value.dialCode;
        user.countryCode = signupForm.controls.phone.value.countryCode;
        user.role = 'student';
        user.term = signupForm.controls.terms.value;
        this._service.createUser(user)
            .subscribe({
            next: data => {
                if (data.status == 'Failed') {
                    this.error = data.message;
                    this.toastr.error(data.message);
                    if (data.isExist) {
                        this.passwordInputDisable = true;
                        this.signupForm.controls.password.setValue('');
                        this.signupForm.controls.cpassword.setValue('');
                    }
                    this.buttonText = 'Sign Up';
                    this.buttonDisable = false;
                }
                else {
                    this.success = data.message;
                    this.toastr.success(data.message);
                    this.submitted = false;
                    this.buttonText = 'redirecting...';
                    this.signupForm.reset();
                    this.buttonDisable = false;
                    setTimeout(() => {
                        this.router.navigate(['/login']);
                    }, 3000);
                }
            },
            error: error => {
                this.error = 'something went wrong. please try again later!';
                this.buttonDisable = false;
                this.buttonText = 'Sign Up';
            }
        });
    }
    ngOnInit() {
        this._loading.setLoading(true, 'Lazy Load');
        if (this.token.isValidToken()) {
            this.router.navigate(['/']);
        }
    }
    ngAfterContentInit() {
        setTimeout(() => {
            this._loading.setLoading(false, 'Lazy Load');
        }, 500);
    }
    onChange(UpdatedValue) {
        if (this.signupForm.controls.phone.valid) {
            this._service.isUniquePhone({ 'phone': UpdatedValue.number })
                .subscribe({
                next: data => {
                    if (data.status == 'Failed') {
                        this.toastr.error(data.message);
                    }
                    else {
                        this.getCodeButton = true;
                        this.validphone = UpdatedValue.e164Number;
                    }
                },
                error: error => {
                    this.error = 'something went wrong. please try again later!';
                    this.buttonDisable = false;
                    this.buttonText = 'Sign Up';
                }
            });
        }
        else {
            this.getCodeButton = false;
            this.showOTPInput = false;
            this.signupForm.controls.num.setValue('');
        }
    }
    sendOTP() {
        this.error = '';
        this.success = '';
        this._loading.setLoading(true, 'Lazy Load');
        this.opt.length = 0;
        if (this.signupForm.controls.phone.valid) {
            this._service.sendotpToPhone({ 'phoneNumber': this.validphone, 'verify': false })
                .subscribe({
                next: data => {
                    if (data.status == 'Failed') {
                        this.error = data.message;
                        this.toastr.error(data.message);
                        this._loading.setLoading(false, 'Lazy Load');
                    }
                    else {
                        this.success = data.message;
                        this.toastr.success(data.message);
                        this.getCodeButton = false;
                        this.showOTPInput = true;
                        this._loading.setLoading(false, 'Lazy Load');
                    }
                },
                error: error => {
                    //this.error='something went wrong. please try again later!';
                    // this.toastr.error('Something went wrong. Please try again later!');
                    this.toastr.error(error.error.message);
                    this._loading.setLoading(false, 'Lazy Load');
                }
            });
        }
    }
    reSendOTP() {
        this.error = '';
        this.success = '';
        this.getCodeButton = false;
        this.passwordInputDisable = true;
        this.signupForm.controls.num.setValue('');
        this.opt.length = 0;
        this._loading.setLoading(true, 'Lazy Load');
        if (this.signupForm.controls.phone.valid) {
            this._service.sendotpToPhone({ 'phoneNumber': this.validphone, 'verify': false })
                .subscribe({
                next: data => {
                    if (data.status == 'Failed') {
                        this.error = data.message;
                        this.toastr.error(data.message);
                        this._loading.setLoading(false, 'Lazy Load');
                    }
                    else {
                        this.success = data.message;
                        this.toastr.success(data.message);
                        this.showOTPInput = true;
                        this._loading.setLoading(false, 'Lazy Load');
                    }
                },
                error: error => {
                    //this.error='something went wrong. please try again later!';
                    //this.toastr.error('Something went wrong. Please try again later!');
                    this.toastr.error(error.error.message);
                    this._loading.setLoading(false, 'Lazy Load');
                }
            });
        }
    }
    onInputEntry(event, nextInput) {
        let input = event.target;
        let length = input.value.length;
        let maxLength = input.attributes.maxlength.value;
        if (length >= maxLength) {
            nextInput.focus();
            nextInput.removeAttribute("readonly");
            this.opt.push(input.value);
            if (this.opt.length == 4) {
                console.log(this.opt.join(''));
                this._loading.setLoading(true, 'Lazy Load');
                this._service.verifyOTP({ 'phoneNumber': this.validphone, 'otp': this.opt.join(''), 'verify': false })
                    .subscribe({
                    next: data => {
                        if (data.status == 'Failed') {
                            this.error = data.message;
                            this.toastr.error(data.message);
                            this.showOTPInput = true;
                            this.opt.length = 0;
                            this.signupForm.controls.num.setValue('');
                            this._loading.setLoading(false, 'Lazy Load');
                        }
                        else {
                            this.success = data.message;
                            this.toastr.success(data.message);
                            this.getCodeButton = false;
                            this.showOTPInput = false;
                            this.opt.length = 0;
                            this.signupForm.controls.num.setValue('');
                            this.passwordInputDisable = false;
                            this._loading.setLoading(false, 'Lazy Load');
                        }
                    },
                    error: error => {
                        // this.error='something went wrong. please try again later!';
                        //console.log(error.error);
                        this.signupForm.controls.num.setValue('');
                        this.toastr.error(error.error.message);
                        this._loading.setLoading(false, 'Lazy Load');
                    }
                });
            }
        }
    }
}
SignupComponent.ɵfac = function SignupComponent_Factory(t) { return new (t || SignupComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_cookie_service__WEBPACK_IMPORTED_MODULE_6__["CookieService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_shared_services_token_service__WEBPACK_IMPORTED_MODULE_8__["TokenService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_shared_services_loading_service__WEBPACK_IMPORTED_MODULE_9__["LoadingService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](_unique_email_validator_service__WEBPACK_IMPORTED_MODULE_10__["CustomValidationService"]), _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_11__["ToastrService"])); };
SignupComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵdefineComponent"]({ type: SignupComponent, selectors: [["app-signup"]], decls: 66, vars: 66, consts: [[1, "hero-banner", "login-signup", "mb-md-5", "mb-4"], [1, "hero-caption"], [1, "container"], [1, "col-lg-5"], [1, "d-flex", "justify-content-center", "align-items-center", "mb-4"], ["height", "45px", "viewBox", "0 0 22 47", "xmlns", "http://www.w3.org/2000/svg", 1, "me-3"], ["d", "M6.80666 23.6021C6.80207 27.0696 7.48229 30.5039 8.80827 33.7079C10.1343 36.9119 12.0799 39.8225 14.5336 42.2726L10.4192 46.387C4.37932 40.3384 0.988195 32.1391 0.991091 23.5912C0.993986 15.0433 4.39067 6.84633 10.4346 0.801758L14.5502 4.91737C12.0916 7.36767 10.1418 10.2799 8.81281 13.4866C7.48387 16.6932 6.80208 20.131 6.80666 23.6021Z"], ["d", "M16.2159 23.6023C16.213 25.8333 16.652 28.0428 17.5075 30.1033C18.363 32.1638 19.6181 34.0345 21.2004 35.6074L17.0937 39.714C14.9715 37.5998 13.2879 35.087 12.1396 32.3202C10.9913 29.5534 10.4009 26.587 10.4023 23.5914C10.4038 20.5957 10.997 17.6299 12.148 14.8641C13.2989 12.0984 14.9849 9.58734 17.1091 7.4751L21.2158 11.5818C19.629 13.1552 18.37 15.0278 17.5117 17.0912C16.6535 19.1545 16.2131 21.3676 16.2159 23.6023Z"], [3, "formGroup", "ngSubmit"], [1, "row"], [1, "col-sm-6", "mb-3", "position-relative"], [4, "ngIf"], ["onkeypress", "return (event.charCode > 64 &&\n                            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)", "type", "text", "formControlName", "fname", "placeholder", "First Name", 1, "form-control", 3, "ngClass"], ["onkeypress", "return (event.charCode > 64 &&\n                            event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)", "type", "text", "formControlName", "lname", "placeholder", "Last Name", 1, "form-control", 3, "ngClass"], ["type", "email", "formControlName", "email", "placeholder", "Email", 1, "form-control", 3, "ngClass"], [1, "col-sm-6", "mb-3", "position-relative", "country-code-flag"], ["name", "phone", "formControlName", "phone", 3, "cssClass", "preferredCountries", "enableAutoCountrySelect", "enablePlaceholder", "searchCountryFlag", "searchCountryField", "selectFirstCountry", "selectedCountryISO", "phoneValidation", "separateDialCode", "maxLength", "numberFormat", "ngClass", "ngModelChange"], ["class", "btn btn-primary get-code-btn px-1 position-absolute", 3, "click", 4, "ngIf"], ["class", "col-sm-12 mb-3 position-relative", 4, "ngIf"], [1, "col-sm-6", "passwordgroup", "mb-3", "position-relative"], ["formControlName", "password", "placeholder", "Set Password", 1, "form-control", 3, "type", "ngClass"], [1, "eye-icon"], [1, "far", "fa-eye", 3, "ngClass", "click"], ["formControlName", "cpassword", "placeholder", "Confirm Password", 1, "form-control", 3, "type", "ngClass"], [1, "form-check", "text-start", "mt-2"], ["formControlName", "terms", "type", "checkbox", "value", "1", "id", "flexCheckDefault", 1, "form-check-input", 3, "ngClass"], ["for", "flexCheckDefault", 1, "form-check-label"], ["href", "#"], ["type", "submit", 1, "btn", "btn-primary", "w-100", "mt-4", 3, "disabled"], [1, "divider", "text-center"], [1, "social-ls-icons", "text-center", "mt-4", "mb-4"], [1, "fab", "fa-apple"], [1, "fab", "fa-facebook-f"], [1, "text-center"], ["routerLink", "/login", 1, "text-decoration-none", "ms-2", "fw-bold"], [1, "col-lg-6", "offset-lg-6", "p-0"], ["src", "assets/images/goonee-login-image.jpg", "alt", "goonee_login_banner", 1, "w-100", "d-none", "d-lg-block"], ["src", "assets/images/goonee-login-mobile-image.jpg", "alt", "goonee_login_banner", 1, "w-100", "d-block", "d-lg-none"], ["class", "validate-pop text-danger", 4, "ngIf"], [1, "validate-pop", "text-danger"], ["class", "validate-msg text-danger d-block mt-2", 4, "ngIf"], [1, "validate-msg", "text-danger", "d-block", "mt-2"], [1, "fas", "fa-info-circle"], [1, "btn", "btn-primary", "get-code-btn", "px-1", "position-absolute", 3, "click"], [1, "col-sm-12", "mb-3", "position-relative"], [1, "row", "text-center", "gx-3"], [1, "col"], ["type", "text", "formControlName", "num", "placeholder", "_", "maxlength", "1", 1, "form-control", "text-center", 3, "keypress", "input"], ["input1", ""], ["type", "text", "formControlName", "num", "readonly", "", "placeholder", "_", "maxlength", "1", 1, "form-control", "text-center", 3, "keypress", "input"], ["input2", ""], ["type", "text", "formControlName", "num", "placeholder", "_", "maxlength", "1", "readonly", "", 1, "form-control", "text-center", 3, "keypress", "input"], ["input3", ""], ["input4", ""], [1, "text-center", "form-check", "p-0"], [1, "text-decoration-none", "fw-bold", 3, "click"], ["class", "validate-msg text-danger d-block mt-2", 4, "ngIf", "ngIfElse"], ["elseBlock", ""], ["class", "validate-msg text-danger d-block mt-2 ", 4, "ngIf"]], template: function SignupComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](4, "h1", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](5, "svg", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](6, "path", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](7, "path", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](8, " Sign Up");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](9, "form", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngSubmit", function SignupComponent_Template_form_ngSubmit_9_listener() { return ctx.postData(ctx.signupForm); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](10, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](11, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](12, SignupComponent_div_12_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](13, "input", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](14, SignupComponent_div_14_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](15, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](16, SignupComponent_div_16_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](17, "input", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](18, SignupComponent_div_18_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](19, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](20, SignupComponent_div_20_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](21, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](22, SignupComponent_div_22_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](23, SignupComponent_div_23_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](24, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](25, SignupComponent_div_25_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](26, "ngx-intl-tel-input", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("ngModelChange", function SignupComponent_Template_ngx_intl_tel_input_ngModelChange_26_listener($event) { return ctx.onChange($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](27, SignupComponent_a_27_Template, 2, 0, "a", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](28, SignupComponent_div_28_Template, 18, 0, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](29, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](30, SignupComponent_div_30_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](31, "input", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](32, "span", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](33, "i", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function SignupComponent_Template_i_click_33_listener() { return ctx.toggleFieldTextType(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](34, SignupComponent_div_34_Template, 5, 3, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](35, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](36, SignupComponent_div_36_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](37, "input", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](38, "span", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](39, "i", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵlistener"]("click", function SignupComponent_Template_i_click_39_listener() { return ctx.toggleFieldTextTypeC(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](40, SignupComponent_div_40_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](41, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](42, "input", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](43, "label", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](44, " I Agree with all ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](45, "a", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](46, "Terms & Conditions");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](47, " of the Website. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtemplate"](48, SignupComponent_div_48_Template, 2, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](49, "button", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](50);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](51, "p", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](52, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](53, "Or Sign Up with");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](54, "div", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](55, "a", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](56, "i", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](57, "a", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](58, "i", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](59, "p", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](60, "Already have an account? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](61, "a", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtext"](62, "Login Here");
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementStart"](63, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](64, "img", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelement"](65, "img", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("formGroup", ctx.signupForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.fname.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction1"](43, _c0, ctx.submitted && ctx.f.fname.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.fname.hasError("minlength"));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.lname.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction1"](45, _c0, ctx.submitted && ctx.f.lname.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.lname.hasError("minlength"));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.email.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction1"](47, _c0, ctx.submitted && ctx.f.email.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.email.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.f.email.hasError("emailTaken"));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.phone.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("cssClass", "form-control ")("preferredCountries", ctx.preferredCountries)("enableAutoCountrySelect", true)("enablePlaceholder", true)("searchCountryFlag", true)("searchCountryField", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction2"](49, _c1, ctx.SearchCountryField.Iso2, ctx.SearchCountryField.Name))("selectFirstCountry", false)("selectedCountryISO", ctx.CountryISO.India)("phoneValidation", true)("separateDialCode", ctx.separateDialCode)("maxLength", 12)("numberFormat", ctx.PhoneNumberFormat.National)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction1"](52, _c0, ctx.submitted && ctx.f.phone.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.getCodeButton);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.showOTPInput);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.password.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("type", ctx.fieldTextType ? "text" : "password")("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction1"](54, _c0, ctx.submitted && ctx.f.password.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵattribute"]("readonly", ctx.passwordInputDisable ? true : null);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction2"](56, _c2, !ctx.fieldTextType, ctx.fieldTextType));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.password.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.cpassword.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("type", ctx.fieldTextTypeC ? "text" : "password")("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction1"](59, _c0, ctx.submitted && ctx.f.cpassword.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵattribute"]("readonly", ctx.passwordInputDisable ? true : null);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction2"](61, _c2, !ctx.fieldTextTypeC, ctx.fieldTextTypeC));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.cpassword.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵpureFunction1"](64, _c0, ctx.submitted && ctx.f.terms.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.terms.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵproperty"]("disabled", ctx.buttonDisable ? true : null);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_4__["ɵɵtextInterpolate"](ctx.buttonText);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_3__["NativeElementInjectorDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_12__["NgClass"], ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_3__["NgxIntlTelInputComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["CheckboxControlValueAccessor"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["MaxLengthValidator"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzaWdudXAuY29tcG9uZW50LnNjc3MifQ== */"] });


/***/ }),

/***/ "tMgt":
/*!*****************************************************************************!*\
  !*** ./src/app/components/site/userauthentication/login/login.component.ts ***!
  \*****************************************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _signup_users__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../signup/users */ "iLbm");
/* harmony import */ var angularx_social_login__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularx-social-login */ "ahC7");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/user.service */ "/W/p");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _shared_services_token_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../../../shared/services/token.service */ "hrsj");
/* harmony import */ var _shared_services_auth_state_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../../../../shared/services/auth-state.service */ "bVZ7");
/* harmony import */ var _shared_services_loading_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../shared/services/loading.service */ "gw8Y");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-intl-tel-input */ "t34c");














function LoginComponent_div_11_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function LoginComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, LoginComponent_div_11_span_1_Template, 2, 0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.f.username.errors.required);
} }
function LoginComponent_div_14_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function LoginComponent_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, LoginComponent_div_14_span_1_Template, 2, 0, "span", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r1.f.password.errors.required);
} }
const _c0 = function (a0) { return { "border-danger": a0 }; };
const _c1 = function (a0, a1) { return { "fa-eye-slash": a0, "fa-eye": a1 }; };
class LoginComponent {
    constructor(formbuilder, _service, router, token, authState, _loading, authService, toastr) {
        this.formbuilder = formbuilder;
        this._service = _service;
        this.router = router;
        this.token = token;
        this.authState = authState;
        this._loading = _loading;
        this.authService = authService;
        this.toastr = toastr;
        this.error = '';
        this.success = '';
        this.loggedId = false;
        this.buttonDisable = false;
        this.title = 'Goonee Signup';
        this.username = "";
        this.password = "";
        this.submitted = false;
        this.buttonText = 'Login';
        this.loginForm = formbuilder.group({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required])
        });
    }
    toggleFieldTextType() {
        this.fieldTextType = !this.fieldTextType;
    }
    get f() {
        return this.loginForm.controls;
    }
    postData(loginForm) {
        this.error = '';
        this.success = '';
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }
        this._loading.setLoading(true, 'Lazy Load');
        this.buttonDisable = true;
        this.buttonText = 'Sending Information...';
        this._service.userLogin({ username: loginForm.controls.username.value, password: loginForm.controls.password.value, social: false })
            .subscribe({
            next: data => {
                if (data.status == 'Failed') {
                    this.error = data.message;
                    this._loading.setLoading(false, 'Lazy Load');
                    this.toastr.error(data.message);
                    this.buttonText = 'Login';
                    this.buttonDisable = false;
                }
                else {
                    this.responseHandler(data);
                    this.success = data.message;
                    this._loading.setLoading(false, 'Lazy Load');
                    this.toastr.success("Logged In Successfully.");
                    this.submitted = false;
                    this.authState.setAuthState(true);
                    this.loginForm.reset();
                    this.buttonDisable = true;
                    this.router.navigate(['/']);
                }
            },
            error: error => {
                this.buttonDisable = false;
                this._loading.setLoading(false, 'Lazy Load');
                console.log(error);
                //this.error='something went wrong. please try again later!';
                this.toastr.error(error.error.message);
                this.buttonText = 'Login';
            }
        });
    }
    // social login with facebook
    signInWithFB() {
        this._loading.setLoading(true, 'Lazy Load');
        let socialPlatformProvider = angularx_social_login__WEBPACK_IMPORTED_MODULE_2__["FacebookLoginProvider"].PROVIDER_ID;
        this.authService.signIn(socialPlatformProvider).then((userData) => {
            this.user = userData.facebook;
            const user = new _signup_users__WEBPACK_IMPORTED_MODULE_1__["Users"]();
            user.firstname = userData.facebook.first_name;
            user.lastname = userData.facebook.last_name;
            user.email = userData.facebook.email;
            user.social = true;
            this._service.userLogin(user)
                .subscribe({
                next: data => {
                    if (data.status == 'Failed') {
                        this.toastr.error(data.message);
                        this._loading.setLoading(false, 'Lazy Load');
                        this.error = data.message;
                    }
                    else {
                        this._loading.setLoading(false, 'Lazy Load');
                        this.responseHandler(data);
                        this.toastr.success("Logged In Successfully.");
                        this.authState.setAuthState(true);
                        this.router.navigate(['/']);
                    }
                },
                error: error => {
                    this.error = 'something went wrong. please try again later!';
                }
            });
        });
    }
    // Handle response
    responseHandler(data) {
        this.token.handleData(data.token);
    }
    ngOnInit() {
        this._loading.setLoading(true, 'Lazy Load');
        if (this.token.isValidToken()) {
            this.router.navigate(['/']);
        }
    }
    ngAfterContentInit() {
        setTimeout(() => {
            this._loading.setLoading(false, 'Lazy Load');
        }, 500);
    }
}
LoginComponent.ɵfac = function LoginComponent_Factory(t) { return new (t || LoginComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_services_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_shared_services_token_service__WEBPACK_IMPORTED_MODULE_6__["TokenService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_shared_services_auth_state_service__WEBPACK_IMPORTED_MODULE_7__["AuthStateService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_shared_services_loading_service__WEBPACK_IMPORTED_MODULE_8__["LoadingService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](angularx_social_login__WEBPACK_IMPORTED_MODULE_2__["SocialAuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_9__["ToastrService"])); };
LoginComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: LoginComponent, selectors: [["app-login"]], decls: 38, vars: 16, consts: [[1, "hero-banner", "login-signup", "mb-md-5", "mb-4"], [1, "hero-caption"], [1, "container"], [1, "col-lg-5"], [1, "d-flex", "justify-content-center", "align-items-center", "mb-4"], ["height", "45px", "viewBox", "0 0 22 47", "xmlns", "http://www.w3.org/2000/svg", 1, "me-3"], ["d", "M6.80666 23.6021C6.80207 27.0696 7.48229 30.5039 8.80827 33.7079C10.1343 36.9119 12.0799 39.8225 14.5336 42.2726L10.4192 46.387C4.37932 40.3384 0.988195 32.1391 0.991091 23.5912C0.993986 15.0433 4.39067 6.84633 10.4346 0.801758L14.5502 4.91737C12.0916 7.36767 10.1418 10.2799 8.81281 13.4866C7.48387 16.6932 6.80208 20.131 6.80666 23.6021Z"], ["d", "M16.2159 23.6023C16.213 25.8333 16.652 28.0428 17.5075 30.1033C18.363 32.1638 19.6181 34.0345 21.2004 35.6074L17.0937 39.714C14.9715 37.5998 13.2879 35.087 12.1396 32.3202C10.9913 29.5534 10.4009 26.587 10.4023 23.5914C10.4038 20.5957 10.997 17.6299 12.148 14.8641C13.2989 12.0984 14.9849 9.58734 17.1091 7.4751L21.2158 11.5818C19.629 13.1552 18.37 15.0278 17.5117 17.0912C16.6535 19.1545 16.2131 21.3676 16.2159 23.6023Z"], [3, "formGroup", "ngSubmit"], [1, "mb-3", "position-relative"], [4, "ngIf"], ["type", "text", "formControlName", "username", "placeholder", "Email or phone number", 1, "form-control", 3, "ngClass"], [1, "passwordgroup", "mb-3", "position-relative"], ["type", "password", "formControlName", "password", "placeholder", "Enter your password", 1, "form-control", 3, "type", "ngClass"], [1, "eye-icon"], [1, "far", "fa-eye", 3, "ngClass", "click"], [1, "pt-2", "pb-2"], ["routerLink", "/forgot-password", 1, "text-decoration-none", "ms-3", "fw-bold"], ["type", "submit", 1, "btn", "btn-primary", "w-100", 3, "disabled"], [1, "divider", "text-center"], [1, "social-ls-icons", "text-center", "mt-4", "mb-4"], ["href", "#"], [1, "fab", "fa-apple"], [3, "click"], [1, "fab", "fa-facebook-f"], [1, "text-center"], ["routerLink", "/signup", 1, "text-decoration-none", "ms-2", "fw-bold"], [1, "col-lg-6", "offset-lg-6", "p-0"], ["src", "assets/images/goonee-login-image.jpg", "alt", "goonee_login_banner", 1, "w-100", "d-none", "d-lg-block"], ["src", "assets/images/goonee-login-mobile-image.jpg", "alt", "goonee_login_banner", 1, "w-100", "d-block", "d-lg-none"], ["class", "validate-pop text-danger", 4, "ngIf"], [1, "validate-pop", "text-danger"]], template: function LoginComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "h1", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "svg", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](6, "path", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](7, "path", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](8, " Login");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnamespaceHTML"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "form", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngSubmit", function LoginComponent_Template_form_ngSubmit_9_listener() { return ctx.postData(ctx.loginForm); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](10, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](11, LoginComponent_div_11_Template, 2, 1, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](12, "input", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](14, LoginComponent_div_14_Template, 2, 1, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](15, "input", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](16, "span", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](17, "i", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function LoginComponent_Template_i_click_17_listener() { return ctx.toggleFieldTextType(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](18, "p", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](19, "a", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](20, "Forgot Password?");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](21, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](22);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](23, "p", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](24, "span");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](25, "Or Login with");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](26, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](27, "a", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](28, "i", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](29, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function LoginComponent_Template_a_click_29_listener() { return ctx.signInWithFB(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](30, "i", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](31, "p", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](32, "Don\u2019t have an account? ");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](33, "a", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](34, "Create new account");
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](35, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](36, "img", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](37, "img", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("formGroup", ctx.loginForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.username.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction1"](9, _c0, ctx.submitted && ctx.f.username.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.submitted && ctx.f.password.errors);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("type", ctx.fieldTextType ? "text" : "password")("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction1"](11, _c0, ctx.submitted && ctx.f.password.errors));
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction2"](13, _c1, !ctx.fieldTextType, ctx.fieldTextType));
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("disabled", ctx.buttonDisable ? true : null);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](ctx.buttonText);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_11__["NativeElementInjectorDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgClass"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterLinkWithHref"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsb2dpbi5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "u8BJ":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/site/userauthentication/forgetpassword/forgetpassword.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: ForgetpasswordComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForgetpasswordComponent", function() { return ForgetpasswordComponent; });
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _signup_custom_validators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../signup/custom-validators */ "nlBZ");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/user.service */ "/W/p");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "5eHb");
/* harmony import */ var _shared_services_loading_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../shared/services/loading.service */ "gw8Y");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-intl-tel-input */ "t34c");











function ForgetpasswordComponent_div_3_div_12_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ForgetpasswordComponent_div_3_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ForgetpasswordComponent_div_3_div_12_span_1_Template, 2, 0, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r4.f.username.errors.required);
} }
const _c0 = function (a0) { return { "border-danger": a0 }; };
function ForgetpasswordComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r7 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "h1", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "svg", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](3, "path", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](4, "path", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, " Forgot Password");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnamespaceHTML"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "p", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](7, "Enter your Email or phone number to receive a link to ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](8, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](9, "create a new password.");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](10, "form", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngSubmit", function ForgetpasswordComponent_div_3_Template_form_ngSubmit_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r7); const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r6.postData(ctx_r6.forgetpasswordForm); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](11, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](12, ForgetpasswordComponent_div_3_div_12_Template, 2, 1, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](13, "input", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](14, "button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("formGroup", ctx_r0.forgetpasswordForm);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r0.submitted && ctx_r0.f.username.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction1"](4, _c0, ctx_r0.submitted && ctx_r0.f.username.errors));
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](ctx_r0.buttonText);
} }
function ForgetpasswordComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "h1", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "svg", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](3, "path", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](4, "path", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, " Forgot Password");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnamespaceHTML"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "h6", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](7, "Password Reset Email Has Been Sent. . ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](8, "p", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](9, "A password reset email has been sent to ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](10, "b", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](12, " for your account, It may take several minutes to show up in your inbox. Please wait for 10 minutes before attempting another reset.");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "p", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](14, "Didn\u2019t get Mail? ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](15, "a", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ForgetpasswordComponent_div_4_Template_a_click_15_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r9); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r8.refresh(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](16, "Resend Mail");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](ctx_r1.success);
} }
function ForgetpasswordComponent_div_5_div_14_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ForgetpasswordComponent_div_5_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ForgetpasswordComponent_div_5_div_14_span_1_Template, 2, 0, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r10.ot.otp.errors.required);
} }
function ForgetpasswordComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "h1", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "svg", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](3, "path", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](4, "path", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, " Passcode Verification");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnamespaceHTML"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "p", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](7, "A password reset OTP has been sent to your ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](8, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](9, "mobile number ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](10, "b", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](12, "form", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngSubmit", function ForgetpasswordComponent_div_5_Template_form_ngSubmit_12_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r13); const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r12.postOTPFormData(ctx_r12.verifyOTPForm); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](14, ForgetpasswordComponent_div_5_div_14_Template, 2, 1, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](15, "input", 23);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("keypress", function ForgetpasswordComponent_div_5_Template_input_keypress_15_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r13); const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r14.numberOnly($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](16, "button", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](17, "Submit");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](18, "p", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](19, "Didn\u2019t get OTP? ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](20, "a", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ForgetpasswordComponent_div_5_Template_a_click_20_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r13); const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r15.reSendOTP(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](21, "Resend OTP");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtextInterpolate"](ctx_r2.success);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("formGroup", ctx_r2.verifyOTPForm);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r2.submittedOtpForm && ctx_r2.ot.otp.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction1"](4, _c0, ctx_r2.submittedOtpForm && ctx_r2.ot.otp.errors));
} }
function ForgetpasswordComponent_div_6_div_10_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ForgetpasswordComponent_div_6_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ForgetpasswordComponent_div_6_div_10_span_1_Template, 2, 0, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r16.cf.password.errors.required);
} }
function ForgetpasswordComponent_div_6_div_14_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "Invalid");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ForgetpasswordComponent_div_6_div_14_span_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "i", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, " Password must be more than 8 characters long");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ForgetpasswordComponent_div_6_div_14_ng_template_3_span_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "i", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, " Password must contains 1 uppercase, 1 lowercase, 1 numeric and 1 special character");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ForgetpasswordComponent_div_6_div_14_ng_template_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](0, ForgetpasswordComponent_div_6_div_14_ng_template_3_span_0_Template, 3, 0, "span", 35);
} if (rf & 2) {
    const ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r24.cf.password.hasError("hasNumber") || ctx_r24.cf.password.hasError("hasCapitalCase") || ctx_r24.cf.password.hasError("hasSmallCase") || ctx_r24.cf.password.hasError("hasSpecialCharacters"));
} }
function ForgetpasswordComponent_div_6_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ForgetpasswordComponent_div_6_div_14_span_1_Template, 2, 0, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](2, ForgetpasswordComponent_div_6_div_14_span_2_Template, 3, 0, "span", 31);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ForgetpasswordComponent_div_6_div_14_ng_template_3_Template, 1, 1, "ng-template", null, 32, _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵreference"](4);
    const ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r17.cf.password.errors.minlength || ctx_r17.cf.password.hasError("hasNumber") || ctx_r17.cf.password.hasError("hasCapitalCase") || ctx_r17.cf.password.hasError("hasSmallCase") || ctx_r17.cf.password.hasError("hasSpecialCharacters"));
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r17.cf.password.errors.minlength)("ngIfElse", _r23);
} }
function ForgetpasswordComponent_div_6_div_16_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](1, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ForgetpasswordComponent_div_6_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ForgetpasswordComponent_div_6_div_16_span_1_Template, 2, 0, "span", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r18.cf.cpassword.errors.required);
} }
function ForgetpasswordComponent_div_6_div_20_span_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "span", 33);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "i", 34);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](2, " Confirm Password did not match");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} }
function ForgetpasswordComponent_div_6_div_20_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](1, ForgetpasswordComponent_div_6_div_20_span_1_Template, 3, 0, "span", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r19 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r19.cf.cpassword.errors.mustMatch);
} }
const _c1 = function (a0, a1) { return { "fa-eye-slash": a0, "fa-eye": a1 }; };
function ForgetpasswordComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "h1", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "svg", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](3, "path", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](4, "path", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](5, " Reset Password");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnamespaceHTML"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](6, "h6", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](7, "Enter A New Password Below. ");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](8, "form", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("ngSubmit", function ForgetpasswordComponent_div_6_Template_form_ngSubmit_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r29); const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r28.postChangePassData(ctx_r28.changePasswordForm); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](9, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](10, ForgetpasswordComponent_div_6_div_10_Template, 2, 1, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](11, "input", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](12, "span", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](13, "i", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ForgetpasswordComponent_div_6_Template_i_click_13_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r29); const ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r30.toggleFieldTextType(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](14, ForgetpasswordComponent_div_6_div_14_Template, 5, 3, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](15, "div", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](16, ForgetpasswordComponent_div_6_div_16_Template, 2, 1, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](17, "input", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](18, "span", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](19, "i", 28);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵlistener"]("click", function ForgetpasswordComponent_div_6_Template_i_click_19_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵrestoreView"](_r29); const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"](); return ctx_r31.toggleFieldTextTypeC(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](20, ForgetpasswordComponent_div_6_div_20_Template, 2, 1, "div", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](21, "button", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtext"](22, "Save");
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("formGroup", ctx_r3.changePasswordForm);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r3.submittedChangePasswordForm && ctx_r3.cf.password.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("type", ctx_r3.fieldTextType ? "text" : "password")("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction1"](11, _c0, ctx_r3.submittedChangePasswordForm && ctx_r3.cf.password.errors));
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction2"](13, _c1, !ctx_r3.fieldTextType, ctx_r3.fieldTextType));
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r3.submittedChangePasswordForm && ctx_r3.cf.password.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r3.submittedChangePasswordForm && ctx_r3.cf.cpassword.errors);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("type", ctx_r3.fieldTextTypeC ? "text" : "password")("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction1"](16, _c0, ctx_r3.submittedChangePasswordForm && ctx_r3.cf.cpassword.errors));
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵpureFunction2"](18, _c1, !ctx_r3.fieldTextTypeC, ctx_r3.fieldTextTypeC));
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx_r3.submittedChangePasswordForm && ctx_r3.cf.cpassword.errors);
} }
class ForgetpasswordComponent {
    constructor(router, formbuilder, _service, toastr, _loading) {
        this.router = router;
        this.formbuilder = formbuilder;
        this._service = _service;
        this.toastr = toastr;
        this._loading = _loading;
        this.showEmailLinkMessage = false;
        this.showOTPForm = false;
        this.showChangePassForm = false;
        this.username = "";
        this.otp = "";
        this.password = "";
        this.cpassword = "";
        this.error = '';
        this.success = '';
        this.submitted = false;
        this.buttonText = 'Reset Password';
        this.showForm = true;
        this.phoneNumber = '';
        this.submittedOtpForm = false;
        this.token = '';
        this.submittedChangePasswordForm = false;
        this.forgetpasswordForm = formbuilder.group({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required])
        });
        this.verifyOTPForm = formbuilder.group({
            otp: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required])
        });
        this.changePasswordForm = formbuilder.group({
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required,
                _signup_custom_validators__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].patternValidator(/\d/, {
                    hasNumber: true
                }),
                _signup_custom_validators__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].patternValidator(/[A-Z]/, {
                    hasCapitalCase: true
                }),
                _signup_custom_validators__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].patternValidator(/[a-z]/, {
                    hasSmallCase: true
                }),
                _signup_custom_validators__WEBPACK_IMPORTED_MODULE_2__["CustomValidators"].patternValidator(/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, {
                    hasSpecialCharacters: true
                }),
                _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(8)
            ])),
            cpassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControl"](null, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]),
        }, { validators: this.mustMatch('password', 'cpassword') });
    }
    toggleFieldTextType() {
        this.fieldTextType = !this.fieldTextType;
    }
    toggleFieldTextTypeC() {
        this.fieldTextTypeC = !this.fieldTextTypeC;
    }
    mustMatch(controlName, matchingControlName) {
        return (formGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];
            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                return;
            }
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            }
            else {
                matchingControl.setErrors(null);
            }
        };
    }
    get f() {
        return this.forgetpasswordForm.controls;
    }
    postData(forgetpasswordForm) {
        this.success = '';
        this.submitted = true;
        if (this.forgetpasswordForm.invalid) {
            return;
        }
        this._loading.setLoading(true, 'Lazy Load');
        this.buttonText = 'Validating Information...';
        let userinput = forgetpasswordForm.controls.username.value;
        const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const pattern = /^\d+$/;
        if (pattern.test(userinput)) {
            this.phoneNumber = userinput;
            this._service.sendotpToPhone({ 'phoneNumber': userinput, 'verify': true }).subscribe(result => {
                console.log(result.status);
                this._loading.setLoading(false, 'Lazy Load');
                this.toastr.success(result.message);
                this.success = userinput.substring(0, 6) + "****";
                this.showForm = false;
                this.showOTPForm = true;
            }, error => {
                this.buttonText = 'Reset Password';
                this._loading.setLoading(false, 'Lazy Load');
                this.toastr.error(error.error.message);
                this.handleError(error);
            });
        }
        else if (regularExpression.test(String(userinput).toLowerCase())) {
            this._service.forgetPassword({ email: forgetpasswordForm.controls.username.value })
                .subscribe({
                next: data => {
                    if (data.status == 'Failed') {
                        this._loading.setLoading(false, 'Lazy Load');
                        this.toastr.error(data.message);
                        this.buttonText = 'Reset Password';
                    }
                    else {
                        this.success = JSON.parse(JSON.stringify(data.message));
                        //this.toastr.success("You have successfully logged in");
                        this._loading.setLoading(false, 'Lazy Load');
                        this.toastr.success('Generate password link sucessfully sent to your email address.');
                        this.submitted = false;
                        this.showForm = false;
                        this.showEmailLinkMessage = true;
                        //this.router.navigate(['/']);
                    }
                },
                error: error => {
                    this._loading.setLoading(false, 'Lazy Load');
                    this.toastr.error('Something went wrong. Please try again later!');
                    this.buttonText = 'Reset Password';
                }
            });
        }
        else {
            this._loading.setLoading(false, 'Lazy Load');
            this.toastr.error('Please enter a valid email address.');
            this.buttonText = 'Reset Password';
        }
    }
    //Verify OTP form post data
    get ot() {
        return this.verifyOTPForm.controls;
    }
    postOTPFormData(verifyOTPForm) {
        console.log(verifyOTPForm);
        this.submittedOtpForm = true;
        if (this.verifyOTPForm.invalid) {
            return;
        }
        this._loading.setLoading(true, 'Lazy Load');
        this._service.verifyOTP({ 'phoneNumber': this.phoneNumber, 'otp': verifyOTPForm.controls.otp.value, 'verify': true }).subscribe(result => {
            this._loading.setLoading(false, 'Lazy Load');
            this.toastr.success(result.message);
            this.showForm = false;
            this.showOTPForm = false;
            this.showChangePassForm = true;
            this.token = result.token;
        }, error => {
            this.showForm = false;
            this._loading.setLoading(false, 'Lazy Load');
            this.toastr.error(error.error.message);
            this.handleError(error);
        });
    }
    //Resend OTP Function
    reSendOTP() {
        this.verifyOTPForm.controls.otp.setValue('');
        this.submittedOtpForm = false;
        this._loading.setLoading(true, 'Lazy Load');
        this._service.sendotpToPhone({ 'phoneNumber': this.forgetpasswordForm.controls.username.value, 'verify': true }).subscribe(result => {
            this.toastr.success(result.message);
            this.showForm = false;
            this._loading.setLoading(false, 'Lazy Load');
            this.showOTPForm = true;
        }, error => {
            this.buttonText = 'Reset Password';
            this._loading.setLoading(false, 'Lazy Load');
            this.toastr.error(error.error.message);
            this.handleError(error);
        });
    }
    //Change Password Form Submission
    get cf() {
        return this.changePasswordForm.controls;
    }
    postChangePassData(changePasswordForm) {
        this.submittedChangePasswordForm = true;
        if (this.changePasswordForm.invalid) {
            return;
        }
        const userData = {
            password: changePasswordForm.controls.password.value,
            phone: this.phoneNumber,
            token: this.token
        };
        this._loading.setLoading(true, 'Lazy Load');
        this._service.updatePasswordWithPhone(userData).subscribe(result => {
            this._loading.setLoading(false, 'Lazy Load');
            this.toastr.success(result.message);
            this.showForm = false;
            this.showOTPForm = false;
            setTimeout(() => {
                this.router.navigate(['/login']);
            }, 3000);
        }, error => {
            this.showForm = false;
            this.showOTPForm = false;
            this._loading.setLoading(false, 'Lazy Load');
            this.toastr.error(error.error.message);
            this.handleError(error);
        });
    }
    ngOnInit() {
        //console.log(this.router.snapshot.queryParamMap.get('id'));
    }
    refresh() {
        window.location.reload();
    }
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = `Error: ${error.error.message}`;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["throwError"])(errorMessage);
        }
        else {
            // server-side error
            console.log(error.message);
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["throwError"])(errorMessage);
        }
    }
    //Allow only number function
    numberOnly(event) {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
}
ForgetpasswordComponent.ɵfac = function ForgetpasswordComponent_Factory(t) { return new (t || ForgetpasswordComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"]), _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdirectiveInject"](_shared_services_loading_service__WEBPACK_IMPORTED_MODULE_7__["LoadingService"])); };
ForgetpasswordComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: ForgetpasswordComponent, selectors: [["app-forgetpassword"]], decls: 10, vars: 4, consts: [[1, "hero-banner", "login-signup", "mb-md-5", "mb-4"], [1, "hero-caption"], [1, "container"], ["class", "col-lg-5", 4, "ngIf"], [1, "col-lg-6", "offset-lg-6", "p-0"], ["src", "assets/images/goonee-login-image.jpg", "alt", "goonee_login_banner", 1, "w-100", "d-none", "d-lg-block"], ["src", "assets/images/goonee-login-mobile-image.jpg", "alt", "goonee_login_banner", 1, "w-100", "d-block", "d-lg-none"], [1, "col-lg-5"], [1, "d-flex", "justify-content-center", "align-items-center", "mb-4"], ["height", "45px", "viewBox", "0 0 22 47", "xmlns", "http://www.w3.org/2000/svg", 1, "me-3"], ["d", "M6.80666 23.6021C6.80207 27.0696 7.48229 30.5039 8.80827 33.7079C10.1343 36.9119 12.0799 39.8225 14.5336 42.2726L10.4192 46.387C4.37932 40.3384 0.988195 32.1391 0.991091 23.5912C0.993986 15.0433 4.39067 6.84633 10.4346 0.801758L14.5502 4.91737C12.0916 7.36767 10.1418 10.2799 8.81281 13.4866C7.48387 16.6932 6.80208 20.131 6.80666 23.6021Z"], ["d", "M16.2159 23.6023C16.213 25.8333 16.652 28.0428 17.5075 30.1033C18.363 32.1638 19.6181 34.0345 21.2004 35.6074L17.0937 39.714C14.9715 37.5998 13.2879 35.087 12.1396 32.3202C10.9913 29.5534 10.4009 26.587 10.4023 23.5914C10.4038 20.5957 10.997 17.6299 12.148 14.8641C13.2989 12.0984 14.9849 9.58734 17.1091 7.4751L21.2158 11.5818C19.629 13.1552 18.37 15.0278 17.5117 17.0912C16.6535 19.1545 16.2131 21.3676 16.2159 23.6023Z"], [1, "text-center"], [3, "formGroup", "ngSubmit"], [1, "mb-3", "position-relative"], [4, "ngIf"], ["type", "text", "formControlName", "username", "placeholder", "Email or phone number", 1, "form-control", 3, "ngClass"], [1, "btn", "btn-primary", "w-100", "mt-2", "mb-2"], ["class", "validate-pop text-danger", 4, "ngIf"], [1, "validate-pop", "text-danger"], [1, "text-center", "fw-bold", "text-primary"], [1, "fw-bold"], [1, "text-decoration-none", "ms-2", "fw-bold", 3, "click"], ["type", "text", "maxlength", "4", "formControlName", "otp", "placeholder", "Enter OTP", 1, "form-control", 3, "ngClass", "keypress"], [1, "text-center", "fw-bold", "text-primary", "mb-4"], [1, "passwordgroup", "mb-3", "position-relative"], ["type", "password", "formControlName", "password", "placeholder", "New Password", 1, "form-control", 3, "type", "ngClass"], [1, "eye-icon"], [1, "far", "fa-eye", 3, "ngClass", "click"], ["formControlName", "cpassword", "placeholder", "Confirm Password", 1, "form-control", 3, "type", "ngClass"], [1, "btn", "btn-primary", "w-100", "mt-2"], ["class", "validate-msg text-danger d-block mt-2", 4, "ngIf", "ngIfElse"], ["elseBlock", ""], [1, "validate-msg", "text-danger", "d-block", "mt-2"], [1, "fas", "fa-info-circle"], ["class", "validate-msg text-danger d-block mt-2", 4, "ngIf"]], template: function ForgetpasswordComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](3, ForgetpasswordComponent_div_3_Template, 16, 6, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](4, ForgetpasswordComponent_div_4_Template, 17, 1, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](5, ForgetpasswordComponent_div_5_Template, 22, 6, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵtemplate"](6, ForgetpasswordComponent_div_6_Template, 23, 21, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](7, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](8, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](9, "img", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.showForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.showEmailLinkMessage);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.showOTPForm);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵproperty"]("ngIf", ctx.showChangePassForm);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_8__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["FormControlName"], ngx_intl_tel_input__WEBPACK_IMPORTED_MODULE_9__["NativeElementInjectorDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_8__["NgClass"], _angular_forms__WEBPACK_IMPORTED_MODULE_0__["MaxLengthValidator"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmb3JnZXRwYXNzd29yZC5jb21wb25lbnQuc2NzcyJ9 */"] });


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _components_site_pages_home_home_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/site/pages/home/home.component */ "Qfl8");
/* harmony import */ var _components_site_pages_about_about_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/site/pages/about/about.component */ "YZvJ");
/* harmony import */ var _components_site_userauthentication_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/site/userauthentication/login/login.component */ "tMgt");
/* harmony import */ var _components_site_userauthentication_signup_signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/site/userauthentication/signup/signup.component */ "t2kx");
/* harmony import */ var _components_site_userauthentication_forgetpassword_forgetpassword_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/site/userauthentication/forgetpassword/forgetpassword.component */ "u8BJ");
/* harmony import */ var _components_site_userauthentication_changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/site/userauthentication/changepassword/changepassword.component */ "s4QN");
/* harmony import */ var _components_site_pages_category_category_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/site/pages/category/category.component */ "PfYf");
/* harmony import */ var _components_site_pages_be_a_tutor_be_a_tutor_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/site/pages/be-a-tutor/be-a-tutor.component */ "ggxV");
/* harmony import */ var _components_site_pages_learn_learn_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/site/pages/learn/learn.component */ "sn8Y");
/* harmony import */ var _components_site_pages_main_category_detail_main_category_detail_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/site/pages/main-category-detail/main-category-detail.component */ "3oJF");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ "fXoL");













const routes = [
    { path: '', component: _components_site_pages_home_home_component__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"] },
    { path: 'login', component: _components_site_userauthentication_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'signup', component: _components_site_userauthentication_signup_signup_component__WEBPACK_IMPORTED_MODULE_4__["SignupComponent"] },
    { path: 'about-us', component: _components_site_pages_about_about_component__WEBPACK_IMPORTED_MODULE_2__["AboutComponent"] },
    { path: 'forgot-password', component: _components_site_userauthentication_forgetpassword_forgetpassword_component__WEBPACK_IMPORTED_MODULE_5__["ForgetpasswordComponent"] },
    { path: 'response-password-reset', component: _components_site_userauthentication_changepassword_changepassword_component__WEBPACK_IMPORTED_MODULE_6__["ChangepasswordComponent"] },
    { path: 'category', component: _components_site_pages_category_category_component__WEBPACK_IMPORTED_MODULE_7__["CategoryComponent"] },
    { path: 'be-a-tutor', component: _components_site_pages_be_a_tutor_be_a_tutor_component__WEBPACK_IMPORTED_MODULE_8__["BeATutorComponent"] },
    { path: 'learn', component: _components_site_pages_learn_learn_component__WEBPACK_IMPORTED_MODULE_9__["LearnComponent"] },
    { path: 'category/:data', component: _components_site_pages_main_category_detail_main_category_detail_component__WEBPACK_IMPORTED_MODULE_10__["MainCategoryDetailComponent"] }
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_11__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "yZLc":
/*!*****************************************************!*\
  !*** ./src/app/shared/services/auth.interceptor.ts ***!
  \*****************************************************/
/*! exports provided: AuthInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthInterceptor", function() { return AuthInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _token_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./token.service */ "hrsj");


class AuthInterceptor {
    constructor(tokenService) {
        this.tokenService = tokenService;
    }
    intercept(req, next) {
        const accessToken = this.tokenService.getToken();
        req = req.clone({
            setHeaders: {
                Authorization: "Bearer " + accessToken
            }
        });
        return next.handle(req);
    }
}
AuthInterceptor.ɵfac = function AuthInterceptor_Factory(t) { return new (t || AuthInterceptor)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_token_service__WEBPACK_IMPORTED_MODULE_1__["TokenService"])); };
AuthInterceptor.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: AuthInterceptor, factory: AuthInterceptor.ɵfac });


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map