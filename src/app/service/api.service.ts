import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Configuration, configuration } from "../configration";
import { config, Observable, throwError } from "rxjs";
import { Router } from "@angular/router";
import { AuthenticationService } from "./authentication.service";
import { catchError, map, retry, tap } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { SpinnerService } from "./spinner.service";
const AUTH = configuration.AUTH_KEY;

@Injectable({
  providedIn: "root",
})
export class ApiService {
  url: string;
  constructor(
    public http: HttpClient,
    public spinner: SpinnerService,
    public router: Router,
    private authService: AuthenticationService,
    private toastr: ToastrService
  ) { }

  get(request: Request) {
    this.spinner.show();
    return this.http.get(configuration.BASE_URL + request.path, {
        headers: this.getHeader(request),
      })
      .pipe(
        tap((result) => {
          this.spinner.hide()
          if (result["token"]) {
            let data = this.authService.getAuthDetail();
            data = {
              ...data,
              token: result["token"],
            };
            this.authService.setAuth(data);
          }
        }),
        retry(1),
        catchError(this.handleError.bind(this))
      );
  }
  logoutApi(request: Request) {
    this.spinner.show();
    return this.http
      .get(configuration.BASE_URL + request.path, {
        headers: this.getHeader2(request),
      });
  }

  getPDF(request: Request): Observable<Blob> {
    this.url = configuration.BASE_URL + request.path;
    var authorization = 'Bearer ' + sessionStorage.getItem("access_token");

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      "Authorization": authorization, responseType: 'blob'
    });

    return this.http.get<Blob>(this.url, {
      headers: headers, responseType:
        'blob' as 'json'
    });
  }

  getExcel(request: Request): Observable<Blob> {
    this.url = configuration.BASE_URL + request.path;
    const token = this.authService.getToken();

    const headers = new HttpHeaders({
      'Content-Type': 'application/octet-stream',
      "TOKEN": token, responseType: 'blob'
    });
    return this.http.get<Blob>(this.url, { headers: headers, responseType: "blob" as 'json' });
  }

  post(request: Request): Observable<any[]> {
    return this.http
      .post<any[]>(configuration.BASE_URL + request["path"], request["data"], {
        headers: this.getHeader(request),
      })
      .pipe(
        map((result) => {
          this.spinner.hide()
          if (result["token"]) {
            let data = this.authService.getAuthDetail();
            data = {
              ...data,
              token: result["token"],
            };
            this.authService.setAuth(data);
          }
          return result;
        }),
        retry(1),
        catchError(this.handleError.bind(this))
      );
  }

  getWithCode(request): Observable<any[]> {
    let header = this.getHeader(request);

    header = header.append("TOKEN", [request["code"]]);

    return this.http
      .get<any[]>(configuration.BASE_URL + request["path"], {
        headers: header,
      })
      .pipe(
        map((result) => {
          if (result["token"]) {
            let data = this.authService.getAuthDetail();
            data = {
              ...data,
              token: result["token"],
            };
            this.authService.setAuth(data);
          }
          return result;
        }),
        retry(1),
        catchError(this.handleError.bind(this))
      );
  }
  postImage(request: Request) {
    let headers = new HttpHeaders();
    const token = this.authService.getToken();
    if (request.isAuth) {
      headers = new HttpHeaders({ TOKEN: token });
    }
    return this.http
      .post(configuration.BASE_URL + request.path, request.data, {
        headers: headers,
      })
      .pipe(retry(1), catchError(this.handleError.bind(this)));
  }

  getUrl(request: Request) {
    return this.http.get(request.path, {});
  }
  delete(request: Request) {
    return this.http.delete(configuration.BASE_URL + request.path, {
      headers: this.getHeader(request),
    });
  }
  postWithoutToken(request): Observable<any[]> {
    return this.http
      .post<any[]>(configuration.BASE_URL + request["path"], request["data"], {
        headers: this.getHeaderWithoutHeader(request),
      })
      .pipe(
        map((result) => {
          if (result["token"]) {
            let data = this.authService.getAuthDetail();
            data = {
              token: result["token"],
              ...data,
            };
            this.authService.setAuth(data);
          }
          return result;
        }),
        catchError(this.handleError.bind(this))
      );
  }

  getHeaderWithoutHeader(request: Request) {
    let token = JSON.parse(localStorage.getItem("token"));
    let header: HttpHeaders = new HttpHeaders({
      "Content-Type": "application/json",
      Accept: "application/json",
      "Access-Control-Allow-Origin": "*",
    });
    return header;
  }

  handleError(error) {
    if (error["error"] != undefined) {
      if (error['error']['status']['code'] && (error['error']['status']['code'] == "SESSION_EXPIRED" || error['error']['status']['code'] == 'UNAUTHORIZED')) {
        localStorage.removeItem(AUTH);
        window.location.href = '/';
      }
    }
    if (error["error"] != undefined) {
      this.spinner.hide();
      // this.toastrService.error(error['error']['status']['description']); TODO Implemnet This
    }
    let errorMessage = "";
    if (error.error instanceof ErrorEvent) {
      // this.toastrService.error(error['error']['error']);
      // client-side error
      errorMessage = error; //`Error: ${error.error.message}`;
    } else {
      // this.toastrService.error(error['error']['error']);
      // server-side error
      errorMessage = error; //`Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }

  login(request): Observable<any[]> {
    return this.http.post<any[]>(
      configuration.BASE_URL + request.path,
      request.data,
      {}
    );
  }

  put(request: Request) {
    return this.http.put(configuration.BASE_URL + request.path, request.data, {
      headers: this.getHeader(request),
    });
  }

  logout() {
    let request = {
      path: "auth/users/logout",
      isAuth: true
    };
    this.spinner.show()
    this.logoutApi(request).subscribe(response => { });
    setTimeout(() => {
      this.authService.logout();
    }, 1000)

  }

  // postImage(request:Request){
  //   let headers = new HttpHeaders();
  //   if(request.isAuth){
  //     headers = new HttpHeaders({"Authorization":"Bearer "+this.auth.details.accessToken});
  //   }
  //   return this.http.post(configuration.BASE_URL+request.path,request.data,{
  //     headers: headers
  //   });
  // }

  private getHeader(request: Request) {
    const token = this.authService.getToken();
    let header: HttpHeaders = new HttpHeaders({
    });
    if (!!this.authService.getToken()) {
      header = header.append("Access-Control-Allow-Origin", "*");
      header = header.append("TOKEN", [token]);
    }
    return header;
  }

  private getHeader2(request: Request) {
    const token = this.authService.getVerifyToken();
    let header: HttpHeaders = new HttpHeaders({
    });
    if (!!this.authService.getVerifyToken()) {
      header = header.append("TOKEN", [token]);
    }
    return header;
  }
}

export interface Request {
  path: string;
  data?: any;
  isAuth?: boolean;
}
