import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {VideoCallModule} from './pages/video-call/video-call.module';
import { CommonLayoutComponent } from './layouts/common-layout/common-layout.component';
import {HeaderModule} from './layouts/header/header.module';
import {FooterModule} from './layouts/footer/footer.module';
import { SideBarComponent } from './layouts/side-bar/side-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    CommonLayoutComponent,
    SideBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    VideoCallModule,
    HeaderModule,
    FooterModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    CommonLayoutComponent
  ]
})
export class AppModule { }
