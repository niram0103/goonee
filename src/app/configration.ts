export class Configuration {
  BASE_URL = '';
  AUTH_KEY: string = "authDetail";

  constructor(public state: projectState) {
    if (this.state == "live") {
      this.BASE_URL = 'http://localhost:6110/';
    }else if (this.state == "local") {
      this.BASE_URL = 'http://localhost:6110/';
    }
  }
}

type projectState =  'live' | "local" ;
export const configuration = new Configuration('live');
