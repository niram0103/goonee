import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {CalendarComponent} from '../calendar/calendar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
const routes: Routes = [
  {path:'',component : CalendarComponent}
];


@NgModule({
    declarations: [CalendarComponent],
    exports: [
        CalendarComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory,
        }),
        HttpClientModule,
        FormsModule
    ]
})
export class CalendarsModule { }
