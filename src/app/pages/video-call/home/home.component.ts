import {Component, ElementRef, OnInit, QueryList, ViewChildren} from '@angular/core';
import {
  connect,
  createLocalVideoTrack,
  RemoteAudioTrack,
  RemoteDataTrack,
  RemoteParticipant,
  RemoteTrack,
  RemoteVideoTrack
} from 'twilio-video';
import * as $ from 'jquery';
export type Nullable<T> = T | null;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChildren('join-button') joinButton: ElementRef

  token = '00eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTSzE3YjU5ZDliMTRiZjViODNkMDI4NTMwZmFkZGY4MWYzLTE2NDAwMDMwNjEiLCJpc3MiOiJTSzE3YjU5ZDliMTRiZjViODNkMDI4NTMwZmFkZGY4MWYzIiwic3ViIjoiQUNiYjE2YjhlNjE3NzExMDlmYzhjZjExMjVmNjg3YjczZSIsImV4cCI6MTY0MDAwNjY2MSwiZ3JhbnRzIjp7ImlkZW50aXR5IjoidXN3ZSIsInZpZGVvIjp7fX19.Y2aGydOYNB-zPcrfN9zVNMYs94hhyzyanMBVG2K7uV4';
  constructor() {
  }

  async ngOnInit() {
    const localVideoTrack = await createLocalVideoTrack({width: 640});
    $('#local-media-container').append(localVideoTrack.attach());
  }

  async onJoinClick() {
    console.log('ddd');
    $('#join-button').attr('disabled',true);
    const room = await connect(this.token, {
      name: 'room-name',
      audio: true,
      video: {width: 640}
    });
    console.log('pp',room)

    // Attach the remote tracks of participants already in the room.
    room.participants.forEach(
      participant => {
        this.manageTracksForRemoteParticipant(participant);
      }
    );

    // Wire-up event handlers.
    room.on('participantConnected', this.onParticipantConnected);
    room.on('participantDisconnected', this.onParticipantDisconnected);
    window.onbeforeunload = () => room.disconnect();
  }

  attachAttachableTracksForRemoteParticipant(participant: RemoteParticipant) {
    participant.tracks.forEach(publication => {
      console.log(publication.track)
      if (!publication.isSubscribed) {
        return;
      }

      if (!this.trackExistsAndIsAttachable(publication.track)) {
        return;
      }

      this.attachTrack(publication.track);
    });
  }

  trackExistsAndIsAttachable(track?: Nullable<RemoteTrack>): track is RemoteAudioTrack | RemoteVideoTrack {
    return !!track && (
      (track as RemoteAudioTrack).attach !== undefined ||
      (track as RemoteVideoTrack).attach !== undefined
    );
  }

  attachTrack(track: RemoteAudioTrack | RemoteVideoTrack) {
    console.log(track);
   $('#remote-media-container').append(track.attach());
  }

  /**
   * Triggers when a remote participant connects to the room.
   *
   * @param participant
   * The remote participant
   */
  onParticipantConnected(participant: RemoteParticipant) {
    this.manageTracksForRemoteParticipant(participant);
  }

  /**
   * Triggers when a remote participant disconnects from the room.
   *
   * @param participant
   * The remote participant
   */
  onParticipantDisconnected(participant: RemoteParticipant) {
    document.getElementById(participant.sid)?.remove();
  }

  /**
   * Triggers when a remote track is subscribed to.
   *
   * @param track
   * The remote track
   */
  onTrackSubscribed(track: RemoteTrack) {
    if (!this.trackExistsAndIsAttachable(track)) {
      return;
    }

    this.attachTrack(track);
  }

  /**
   * Triggers when a remote track is unsubscribed from.
   *
   * @param track
   * The remote track
   */
  onTrackUnsubscribed(track: RemoteTrack) {
    if (this.trackExistsAndIsAttachable(track)) {
      if (!(track instanceof RemoteDataTrack)) {
        track.detach().forEach(element => element.remove());
      }
    }
  }

  /**
   * Manages track attachment and subscription for a remote participant.
   *
   * @param participant
   * The remote participant
   */
  manageTracksForRemoteParticipant(participant: RemoteParticipant) {
    // Handle tracks that this participant has already published.
    this.attachAttachableTracksForRemoteParticipant(participant);

    // Handles tracks that this participant eventually publishes.
    participant.on('trackSubscribed', this.onTrackSubscribed);
    participant.on('trackUnsubscribed', this.onTrackUnsubscribed);
  }

}
